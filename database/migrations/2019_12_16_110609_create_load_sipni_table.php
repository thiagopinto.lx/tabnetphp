<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoadSipniTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('load_sipni', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ano');
            $table->string('color');
            $table->unsignedInteger('imuno_sipnis_id');
            $table->foreign('imuno_sipnis_id')->references('id')->on('imuno_sipnis');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('load_sipni');
    }
}
