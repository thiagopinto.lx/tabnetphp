<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexToCicloBairrosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ciclo_bairros', function (Blueprint $table) {
            $table->index('ciclo_id');
            $table->index('bairro_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ciclo_bairros', function (Blueprint $table) {
            $table->dropIndex(['ciclo_id']);
            $table->dropIndex(['bairro_id']);
        });
    }
}
