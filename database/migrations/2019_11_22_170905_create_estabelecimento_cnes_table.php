<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstabelecimentoCnesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estabelecimento_cnes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('co_cnes');
            $table->string('nu_cnpj_mantenedora')->nullable();
            $table->string('no_razao_social')->nullable();
            $table->string('no_fantasia')->nullable();
            $table->string('no_logradouro')->nullable();
            $table->string('nu_endereco')->nullable();
            $table->string('no_complemento')->nullable();
            $table->string('no_bairro')->nullable();
            $table->integer('co_cep')->nullable();
            $table->string('nu_telefone')->nullable();
            $table->string('no_email')->nullable();
            $table->string('nu_cnpj')->nullable();
            $table->integer('co_estado_gestor')->nullable();
            $table->integer('co_municipio_gestor')->nullable();
            $table->double('nu_latitude')->nullable();
            $table->double('nu_longitude')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estabelecimento_cnes');
    }
}
