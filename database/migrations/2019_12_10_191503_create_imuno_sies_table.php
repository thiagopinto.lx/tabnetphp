<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImunoSiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('imuno_sies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->integer('perda');
            $table->unsignedInteger('imuno_sipni_id')->nullable();
            $table->foreign('imuno_sipni_id')->references('id')->on('imuno_sipni');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('imuno_sies');
    }
}
