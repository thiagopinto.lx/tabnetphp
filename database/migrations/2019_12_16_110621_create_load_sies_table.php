<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoadSiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('load_sies', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ano');
            $table->unsignedInteger('imuno_sies_id');
            $table->foreign('imuno_sies_id')->references('id')->on('imuno_sies');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('load_sies');
    }
}
