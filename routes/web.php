<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

$resources = [
    ['notificacao' => 'dengue', 'controller' => 'DengueController'],
    ['notificacao' => 'chik', 'controller' => 'ChikController'],
    ['notificacao' => 'zika', 'controller' => 'ZikaController'],
    ['notificacao' => 'leishvisc', 'controller' => 'LeishViscController'],
    ['notificacao' => 'leishteg', 'controller' => 'LeishTegController'],
    ['notificacao' => 'meningite', 'controller' => 'MeningiteController'],
    ['notificacao' => 'obito', 'controller' => 'ObitoController']
];

Auth::routes();

Route::get('/', 'HomeController@index')->name('index');
Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function() use ($resources){

    Route::get('logout', 'Auth\LoginController@logout')->name('logout');

    Route::get('arbovirose', 'ArboviroseController@index')->name('arbovirose');
    Route::get('arbovirose/getgeocodes', 'ArboviroseController@getGeoCodes')->name('arbovirose.getgeocodes');
    Route::get('meningite/getdatacontatoagravo', 'MeningiteController@getDataContatoAgravo')->name('meningite.getdatacontatoagravo');
    Route::get('meningite/getdatasintomashemorragico', 'MeningiteController@getDataSintomasHemorragico')->name('meningite.getdatasintomashemorragico');
    Route::get('meningite/getdatatipo', 'MeningiteController@getDataTipo')->name('meningite.getdatatipo');
    Route::get('meningite/getdatacriterioconfirmacao', 'MeningiteController@getDataCriterioConfirmacao')->name('meningite.getdatacriterioconfirmacao');
    Route::get('meningite/getdatanotificacaounidade', 'MeningiteController@getDataNotifiacaoUnidade')->name('meningite.getdatanotificacaounidade');

    Route::get('obitoinfantil', 'ObitoInfantilController@index')->name('obitoinfantil.index');
    Route::get('obitoinfantil/detalhamentos', 'ObitoInfantilController@detalhamento')->name('obitoinfantil.detalhamentos');
    Route::get('obitoinfantil/heatmap/{tabela}', 'ObitoInfantilController@heatMap')->name('obitoinfantil.heatmap');
    Route::get('obitoinfantil/clustersmaps', 'ObitoInfantilController@clustersMaps')->name('obitoinfantil.clustersmaps');
    Route::get('obitoinfantil/getgeocodes', 'ObitoInfantilController@getGeoCodes')->name('obitoinfantil.getgeocodes');
    Route::get('obitoinfantil/getdatabairro', 'ObitoInfantilController@getDataBairro')->name('obitoinfantil.getdatabairro');
    Route::get('obitoinfantil/getdataobitocid', 'ObitoInfantilController@getDataObitoCid')->name('obitoinfantil.getdataobitocid');
    Route::get('obitoinfantil/getdatanotificacaounidade', 'ObitoInfantilController@getDataNotifiacaoUnidade')->name('obitoinfantil.getdatanotificacaounidade');

    Route::get('obitoperinatal', 'ObitoPerinatalController@index')->name('obitoperinatal.index');
    Route::get('obitoperinatalobitoperinatalobitoperinatal/detalhamentos', 'ObitoPerinatalController@detalhamento')->name('obitoperinatal.detalhamentos');
    Route::get('obitoperinatalobitoperinatal/heatmap/{tabela}', 'ObitoPerinatalController@heatMap')->name('obitoperinatal.heatmap');
    Route::get('obitoperinatal/clustersmaps', 'ObitoPerinatalController@clustersMaps')->name('obitoperinatal.clustersmaps');
    Route::get('obitoperinatal/getgeocodes', 'ObitoPerinatalController@getGeoCodes')->name('obitoperinatal.getgeocodes');
    Route::get('obitoperinatal/getdatabairro', 'ObitoPerinatalController@getDataBairro')->name('obitoperinatal.getdatabairro');
    Route::get('obitoperinatal/getdataobitocid', 'ObitoPerinatalController@getDataObitoCid')->name('obitoperinatal.getdataobitocid');
    Route::get('obitoperinatal/getdatanotificacaounidade', 'ObitoPerinatalController@getDataNotifiacaoUnidade')->name('obitoperinatal.getdatanotificacaounidade');

    Route::get('ciclos/getdataciclo/{ano}', 'CicloController@getDataCiclo')->name('ciclos.getdataciclo');

    Route::get('imunizacao', 'ImunizacaoController@index')->name('imunizacao.index');
    Route::get('imunizacao/imuno/{id}', 'ImunizacaoController@imuno')->name('imunizacao.imuno');
    Route::get('imunizacao/estabelecimentos/{idImuno?}/{ano?}', 'ImunizacaoController@estabelecimentos')->name('imunizacao.estabelecimentos');
    Route::get('imunizacao/estabelecimento/search/{id}', 'ImunizacaoController@estabelecimentoSearch')->name('imunizacao.estabelecimento.search');
    Route::get('imunizacao/estabelecimento/autocomplete/{id}', 'ImunizacaoController@estabelecimentoAutoComplete')->name('imunizacao.estabelecimento.autocomplete');
    Route::get('imunizacao/estabelecimento/{idImuno?}/{ano?}/{idEstabelecimento?}', 'ImunizacaoController@estabelecimento')->name('imunizacao.estabelecimento');

    foreach ($resources as $resource) {

        Route::get($resource['notificacao'].'/detalhamentos', $resource['controller'].'@detalhamento')->name($resource['notificacao'].'.detalhamentos');
        Route::get($resource['notificacao'].'/getdatabar', $resource['controller'].'@getDataBar')->name($resource['notificacao'].'.getdatabar');
        Route::get($resource['notificacao'].'/getdataline', $resource['controller'].'@getDataLine')->name($resource['notificacao'].'.getdataline');
        Route::get($resource['notificacao'].'/getdatalinebysem', $resource['controller'].'@getDataLineBySem')->name($resource['notificacao'].'.getdatalinebysem');
        Route::get($resource['notificacao'].'/getdataclassificacao', $resource['controller'].'@getDataClassificacao')->name($resource['notificacao'].'.getdataclassificacao');
        Route::get($resource['notificacao'].'/getdataevolucao', $resource['controller'].'@getDataEvolucao')->name($resource['notificacao'].'.getdataevolucao');
        Route::get($resource['notificacao'].'/getdataripsa', $resource['controller'].'@getDataRipsa')->name($resource['notificacao'].'.getdataripsa');
        Route::get($resource['notificacao'].'/getdatasexo', $resource['controller'].'@getDataSexo')->name($resource['notificacao'].'.getdatasexo');
        Route::get($resource['notificacao'].'/getdatabairro', $resource['controller'].'@getDataBairro')->name($resource['notificacao'].'.getdatabairro');
        Route::get($resource['notificacao'].'/getsemepdem', $resource['controller'].'@getSemEpdem')->name($resource['notificacao'].'.getsemepdem');
        Route::get($resource['notificacao'].'/getdatasemepdem', $resource['controller'].'@getDataSemEpdem')->name($resource['notificacao'].'.getdatasemepdem');
        Route::get($resource['notificacao'].'/heatmap/{tabela}', $resource['controller'].'@heatMap')->name($resource['notificacao'].'.heatmap');
        Route::get($resource['notificacao'].'/getdataheatmap/{tabela}', $resource['controller'].'@getDataHeatMap')->name($resource['notificacao'].'.getdataheatmap');
        Route::get($resource['notificacao'].'/clustersmaps', $resource['controller'].'@clustersMaps')->name($resource['notificacao'].'.clustersmaps');
        Route::get($resource['notificacao'].'/getgeocodes', $resource['controller'].'@getGeoCodes')->name($resource['notificacao'].'.getgeocodes');
        Route::resource('/'.$resource['notificacao'], $resource['controller']);
    }

    /*
    | Panel admin
    */
    Route::group(['namespace' => 'Painel', 'prefix' => 'painel', 'as' => 'painel.'], function() use ($resources){

        Route::get('/', 'HomeController@index')->name('home');

        Route::get('users', 'UsersController@index')->name('users');
        Route::post('users/getindex', 'UsersController@getIndex')->name('users.getindex');
        Route::post('users/destroy', 'UsersController@destroy')->name('users.destroy');
        

        foreach ($resources as $resource) {
            Route::get($resource['notificacao'], $resource['controller'].'@index')->name($resource['notificacao']);
            Route::post($resource['notificacao'].'/store', $resource['controller'].'@store')->name($resource['notificacao'].'.store');
            Route::post($resource['notificacao'].'/getindex', $resource['controller'].'@getIndex')->name($resource['notificacao'].'.getindex');
            Route::post($resource['notificacao'].'/storeColor', $resource['controller'].'@storeColor')->name($resource['notificacao'].'.storecolor');
        }

        Route::post('notificacao/getgps/{tabela}', 'NotificacaoController@getGps')->name('notificacao.getgps');
        Route::post('notificacao/getindex/{tabela}', 'NotificacaoController@getIndex')->name('notificacao.getindex');
        Route::get('notificacao/{tabela}', 'NotificacaoController@index')->name('notificacao.index');
        Route::get('notificacao/editgeocode/{tabela}/{agravo}', 'NotificacaoController@editGeocode')->name('notificacao.editgeocode');
        Route::match(['put', 'patch'],'notificacao/updategeocode/{tabela}', 'NotificacaoController@updateGeocode')->name('notificacao.updategeocode');
        Route::match(['put', 'patch'],'notificacao/updategeocodes/{tabela}', 'NotificacaoController@updateGeocodes')->name('notificacao.updategeocodes');
        Route::get('notificacao/editgeocodes/{tabela}', 'NotificacaoController@editGeocodes')->name('notificacao.editgeocodes');
        Route::get('notificacao/getdatageocodes/{tabela}', 'NotificacaoController@getDataGeocodes')->name('notificacao.getdatageocodes');

        Route::post('notificacao-obito/getgps/{tabela}', 'NotificacaoObitoController@getGps')->name('notificacao-obito.getgps');
        Route::post('notificacao-obito/getindex/{tabela}', 'NotificacaoObitoController@getIndex')->name('notificacao-obito.getindex');
        Route::get('notificacao-obito/{tabela}', 'NotificacaoObitoController@index')->name('notificacao-obito.index');
        Route::get('notificacao-obito/editgeocode/{tabela}/{agravo}', 'NotificacaoObitoController@editGeocode')->name('notificacao-obito.editgeocode');
        Route::match(['put', 'patch'],'notificacao-obito/updategeocode/{tabela}', 'NotificacaoObitoController@updateGeocode')->name('notificacao-obito.updategeocode');
        Route::match(['put', 'patch'],'notificacao-obito/updategeocodes/{tabela}', 'NotificacaoObitoController@updateGeocodes')->name('notificacao-obito.updategeocodes');
        Route::get('notificacao-obito/editgeocodes/{tabela}', 'NotificacaoObitoController@editGeocodes')->name('notificacao-obito.editgeocodes');
        Route::get('notificacao-obito/getdatageocodes/{tabela}', 'NotificacaoObitoController@getDataGeocodes')->name('notificacao-obito.getdatageocodes');

        Route::post('unidadesnotificadoras/getindex', 'UnidadeNotificadoraController@getIndex')->name('unidadesnotificadoras.getindex');
        Route::get('unidadesnotificadoras', 'UnidadeNotificadoraController@index')->name('unidadesnotificadoras');
        Route::post('unidadesnotificadoras/store', 'UnidadeNotificadoraController@store')->name('unidadesnotificadoras.store');

        Route::post('cids/getindex', 'CidController@getIndex')->name('cids.getindex');
        Route::get('cids', 'CidController@index')->name('cids');
        Route::post('cids/store', 'CidController@store')->name('cids.store');

        Route::post('bairros/getindex', 'BairroController@getIndex')->name('bairros.getindex');
        Route::get('bairros', 'BairroController@index')->name('bairros');
        Route::post('bairros/store', 'BairroController@store')->name('bairros.store');
        Route::get('bairros/map/{id}', 'BairroController@map')->name('bairros.map');
        Route::get('bairros/autocomplete', 'BairroController@autocomplete')->name('bairros.autocomplete');

        Route::post('regionais/getindex', 'RegionalController@getIndex')->name('regionais.getindex');
        Route::get('regionais', 'RegionalController@index')->name('regionais');
        Route::post('regionais/store', 'RegionalController@store')->name('regionais.store');
        Route::get('regionais/map/{id}', 'RegionalController@map')->name('regionais.map');

        Route::get('ciclos', 'CicloController@index')->name('ciclos');
        Route::post('ciclos/getindex', 'CicloController@getIndex')->name('ciclos.getindex');
        Route::post('ciclos/store', 'CicloController@store')->name('ciclos.store');
        Route::get('ciclos/bairros/{id}', 'CicloController@bairros')->name('ciclos.bairros');
        Route::post('ciclos/getindexbairros/{id}', 'CicloController@getIndexBairros')->name('ciclos.getindexbairros');
        Route::post('ciclos/storebairrociclo/{id}', 'CicloController@storeBairroCiclo')->name('ciclos.storebairrociclo');
        Route::delete('ciclos/bairro/destroy/{id}', 'CicloController@destroyBairros')->name('ciclos.bairro.destroy');

        Route::get('estabelecimentos-cnes', 'EstabelecimentoCnesController@index')
        ->name('estabelecimentos-cnes');
        Route::post('estabelecimentos-cnes/getindex', 'EstabelecimentoCnesController@getIndex')
        ->name('estabelecimentos-cnes.getindex');
        Route::post('estabelecimentos-cnes/store', 'EstabelecimentoCnesController@store')
        ->name('estabelecimentos-cnes.store');
        Route::get('estabelecimentos-cnes/editgeocodes', 'EstabelecimentoCnesController@editGeocodes')
        ->name('estabelecimentos-cnes.editgeocodes');
        Route::get('estabelecimentos-cnes/getdatageocodes', 'EstabelecimentoCnesController@getDataGeocodes')
        ->name('estabelecimentos-cnes.getdatageocodes');
        Route::match(['put', 'patch'],'notificacao-obito/updategeocodes', 'EstabelecimentoCnesController@updateGeocodes')
        ->name('estabelecimentos-cnes.updategeocodes');
        Route::get('estabelecimentos-cnes/editgeocode/{co_cnes}', 'EstabelecimentoCnesController@editGeocode')
        ->name('estabelecimentos-cnes.editgeocode');
        Route::match(['put', 'patch'],'notificacao-obito/updategeocode', 'EstabelecimentoCnesController@updateGeocode')
        ->name('estabelecimentos-cnes.updategeocode');
        Route::post('estabelecimentos-cnes/getgps', 'EstabelecimentoCnesController@getGps')
        ->name('estabelecimentos-cnes.getgps');
        Route::get(
            'estabelecimentos-cnes/autocomplete-fantasia',
            'EstabelecimentoCnesController@autocompleteFantasia'
        )->name('estabelecimentos-cnes.autocomplete-fantasia');
        Route::get(
            'estabelecimentos-cnes/autocomplete-razaosocial', 
            'EstabelecimentoCnesController@autocompleteRazaoSocial'
        )->name('estabelecimentos-cnes.autocomplete-razaosocial');

        Route::get('load-sies', 'LoadSiesController@index')
        ->name('load-sies');
        Route::post('load-sies/getindex', 'LoadSiesController@getIndex')
        ->name('load-sies.getindex');
        Route::post('load-sies/store', 'LoadSiesController@store')
        ->name('load-sies.store');

        Route::get('movimento-sies/{load_sies_id}', 'MovimentoSiesController@index')
        ->name('movimento-sies');
        Route::post('movimento-sies/getindex/{load_sies_id}', 'MovimentoSiesController@getIndex')
        ->name('movimento-sies.getindex');

        Route::get('estabelecimentos-sies', 'EstabelecimentoSiesController@index')
        ->name('estabelecimentos-sies');
        Route::post('estabelecimentos-sies/getindex', 'EstabelecimentoSiesController@getIndex')
        ->name('estabelecimentos-sies.getindex');
        Route::post('estabelecimentos-sies/store', 'EstabelecimentoSiesController@store')
        ->name('estabelecimentos-sies.store');
        Route::get('estabelecimentos-sies/relationship/{idSies}/{idCnes}', 'EstabelecimentoSiesController@relationship')
        ->name('estabelecimentos-sies.relationship');

        Route::get('load-sipni', 'LoadSipniController@index')
        ->name('load-sipni');
        Route::post('load-sipni/getindex', 'LoadSipniController@getIndex')
        ->name('load-sipni.getindex');
        Route::post('load-sipni/store', 'LoadSipniController@store')
        ->name('load-sipni.store');
        Route::post('load-sipni/storeColor', 'LoadSipniController@storeColor')->name('load-sipni.storecolor');


        Route::get('doses-aplicadas-sipni/{load_sipni_id}', 'DosesAplicadasSipniController@index')
        ->name('doses-aplicadas-sipni');
        Route::post('doses-aplicadas-sipni/getindex/{load_sipni_id}', 'DosesAplicadasSipniController@getIndex')
        ->name('doses-aplicadas-sipni.getindex');

        Route::get('imuno-sies', 'ImunoSiesController@index')
        ->name('imuno-sies');
        Route::post('imuno-sies/getindex', 'ImunoSiesController@getIndex')
        ->name('imuno-sies.getindex');
        Route::get('imuno-sies/relationship/{idSies}/{idSipni}', 'ImunoSiesController@relationship')
        ->name('imuno-sies.relationship');
        Route::get('imuno-sies/percatecnica/{idImunoSies}/{perdaImuno}', 'ImunoSiesController@percatecnica')
        ->name('imuno-sies.percatecnica');

        Route::get('imuno-sipni', 'ImunoSipniController@index')
        ->name('imuno-sipni');
        Route::post('imuno-sipni/getindex', 'ImunoSipniController@getIndex')
        ->name('imuno-sipni.getindex');
        Route::get(
            'imuno-sipni/autocomplete-nome',
            'ImunoSipniController@autocompleteNome'
        )->name('imuno-sipni.autocomplete-nome');
        Route::get(
            'imuno-sipni/autocomplete-sigla', 
            'ImunoSipniController@autocompleteSigla'
        )->name('imuno-sipni.autocomplete-sigla');

        Route::get('/imunizacao', 'ImunizacaoController@index')->name('imunizacao');
    });
});
