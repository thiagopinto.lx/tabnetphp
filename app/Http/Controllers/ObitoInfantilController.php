<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\LoadData;
use App\Models\Dengue;
use App\Utilities\Helper;

class ObitoInfantilController extends Controller
{
    public static $resouce = 'obito';
    public static $resouceFullName = 'Obito Infantil';
    public static $resouceShortName = 'Obito Inf';
    public static $routeBefore = ['detalhamentos', 'clustersmaps'];
    public static $routeAfter = [];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $loadDatas = LoadData::where('table_name', self::$resouce)
        ->orderBy('ano', 'desc')
        ->limit(5)
        ->get();
        $newLoadDatas = array();
        $soma = 0;
        $countDatas = count($loadDatas);
        foreach($loadDatas as $loadData){
            $count = DB::table($loadData->table_name.$loadData->ano)
                ->where(function($q){
                    $q->where('IDADE', '<', '400');
                })->count();
            $loadData->count = $count;
            $loadData->tabela = $loadData->table_name.$loadData->ano;
            $loadData->updated = date_format($loadData->updated_at, 'd-m-Y');
            $newLoadDatas[] = $loadData;
            $soma = $soma+$count;
        }
        if($soma > 0 && $countDatas >0){
            $total = new \stdClass;
            $media = $soma/$countDatas;
            $total->titulo = "Média".self::$resouceShortName;
            $total->media = $media;
            $total->color = '#FF0000';
        }

        $options['titulo'] = self::$resouceFullName;
        $fullName = self::$resouceFullName;
        $routeBefore = ['getgeocodes', 'getdatabairro', 'getdataobitocid', 'getdatanotificacaounidade'];
        $routeBefore = array_merge(self::$routeBefore, $routeBefore);

        $urls = Controller::createUrl('obitoinfantil', $routeBefore, self::$routeAfter);

        return view('clustersmaps-obito', compact('newLoadDatas', 'total', 'urls', 'fullName', 'options'));
    }

    public function detalhamento(Request $request)
    {
      $loadDatas = LoadData::where('table_name', self::$resouce)
      ->orderBy('ano', 'desc')
      ->limit(5)
      ->get();
      $newLoadDatas = array();
      $soma = 0;
      $countDatas = count($loadDatas);
      foreach($loadDatas as $loadData){
          $count = DB::table($loadData->table_name.$loadData->ano)
              ->where(function($q){
                  $q->where('IDADE', '<', '400');
              })->count();
          $loadData->count = $count;
          $loadData->tabela = $loadData->table_name.$loadData->ano;
          $loadData->updated = date_format($loadData->updated_at, 'd-m-Y');
          $newLoadDatas[] = $loadData;
          $soma = $soma+$count;
      }
      if($soma > 0 && $countDatas >0){
          $total = new \stdClass;
          $media = $soma/$countDatas;
          $total->titulo = "Média".self::$resouceShortName;
          $total->media = $media;
          $total->color = '#FF0000';
      }

      $options['titulo'] = self::$resouceFullName;
      $fullName = self::$resouceFullName;
      $routeBefore = ['getdataobitocid', 'getdatanotificacaounidade'];
      $urls = Controller::createUrl('obitoinfantil', $routeBefore, self::$routeAfter);

      return view('detalhamento-obito', compact('newLoadDatas', 'total', 'urls', 'fullName', 'options'));
    }

    public function heatMap($tabela)
    {
      $ano = $rest = substr($tabela, -4);
      $options['inicio'] = $ano."-01-01";
      $options['fim'] = $ano."-12-31";
      $options['titulo'] = self::$resouceFullName;
      $routeAfter = array_merge(self::$routeAfter, ['getdataheatmap']);
      $urls = Controller::createUrl(self::$resouce, self::$routeBefore, $routeAfter);
      $urls['getdataheatmap'] =  route($urls['getdataheatmap'], ['tabela' => $tabela]);
      $fullName = self::$resouceFullName;
      return view('heatmap', compact('fullName', 'urls', 'options', 'ano'));
    }

    public function getDataHeatMap(Request $request, $tabela)
    {
      $inicio = $request->get('inicio');
      $fim = $request->get('fim');

      $notificacoes = DB::table($tabela)->select('ID_GEO1', 'ID_GEO2')->where(function($q) use ($inicio, $fim){
          $q->where('DTOBITO', '>=', $inicio);
          $q->where('DTOBITO', '<=', $fim);
          $q->whereNotNull('ID_GEO1');
          $q->whereNotNull('ID_GEO2');
          $q->where('IDADE', '<' ,'400');
      })
      ->get();

        return json_encode($notificacoes);
    }

    public function getGeoCodes(Request $request)
    {
        $ano = $request->get('ano');
        $datas = new \stdClass;
        $loadData = LoadData::where([
            ['table_name', '=', self::$resouce],
            ['ano', '=', $ano]
        ])
        ->first();
        $geocodes = DB::table(self::$resouce.$ano)->select('NUMERODO', 'IDADE', 'NU_ANO','ID_GEO1', 'ID_GEO2')->where(function($q){
            $q->whereNotNull('ID_GEO1');
            $q->whereNotNull('ID_GEO2');
            $q->where('IDADE', '<' ,'400');
        })
        ->get();
        $datas->ano = $ano;
        $datas->geocodes = $geocodes;
        $datas->color = $loadData->color;


        return json_encode($datas);
    }

    public function getDataBairro(Request $request)
    {
        $anos = $request->get('anos');
        $countAnos = count($anos);
        $data = array();
        $datas = new \stdClass;

        if ($countAnos > 0) {

            for ($i=0; $i < $countAnos; $i++) {
                $bairros[] = DB::table(self::$resouce.$anos[$i])
                        ->select(DB::raw('"BAIRES", count(*) AS "COUNTB"'))
                        ->whereNotNull('BAIRES')
                        ->where(function($q){
                            $q->where('IDADE', '<', '400');
                        })
                        ->groupBy('BAIRES')
                        ->orderBy('COUNTB', 'desc')
                        ->limit(10)
                        ->get();
            }

            //dd($bairros);

            for ($i=0; $i < 10; $i++) {

                for ($j=0; $j < $countAnos; $j++) {
                    if (isset($bairros[$j][$i])) {
                        $bairro[] = "{$bairros[$j][$i]->BAIRES}";
                        $bairro[] = "{$bairros[$j][$i]->COUNTB}";
                    }else {
                        $bairro[] = null;
                        $bairro[] = null;
                    }
                }
                $data[] = $bairro;
                unset($bairro);
            }
            $datas->data = $data;
        }else {
            $datas->data = [];
        }


        return json_encode($datas);

    }


    public function getDataObitoCid(Request $request)
    {

        $anos = $request->get('anos');
        $countAnos = count($anos);
        $data = array();
        $datas = new \stdClass;
        $datasets[] = new \stdClass;
        $labels = array();

        /*data = {
            datasets: [{
                data: [10, 20, 30]
            }],

            // These labels appear in the legend and in the tooltips when hovering different arcs
            labels: [
                'Red',
                'Yellow',
                'Blue'
            ]
        };
        */

        if ($countAnos > 0) {

            for ($i=0; $i < $countAnos; $i++) {
                $cids[] = DB::table(self::$resouce.$anos[$i])
                        ->join('cids', self::$resouce.$anos[$i].".CAUSABAS", '=', 'cids.CID10')
                        ->select('cids.DESCR', DB::raw('count('.self::$resouce.$anos[$i].'."CAUSABAS") AS "COUNTCID"'))
                        ->where(function($q){
                            $q->where('IDADE', '<', '400');
                        })
                        ->groupBy('cids.DESCR', self::$resouce.$anos[$i].'.CAUSABAS')
                        ->orderBy('COUNTCID', 'desc')
                        ->limit(10)
                        ->get();
            }


            for ($i=0; $i < 10; $i++) {

                for ($j=0; $j < $countAnos; $j++) {
                    if (isset($cids[$j][$i])) {
                        $cid[] = "{$cids[$j][$i]->DESCR}";
                        $cid[] = "{$cids[$j][$i]->COUNTCID}";
                        $datasets[0]->data[] = "{$cids[$j][$i]->COUNTCID}";
                        $labels[] = "{$cids[$j][$i]->DESCR}";
                    }else {
                        $cid[] = null;
                        $cid[] = null;
                    }
                }
                $data[] = $cid;
                unset($cid);
            }
            $datas->data = $data;
            $datas->datasets = $datasets;
            $datas->labels = $labels;
        }else {
            $datas->data = [];
        }

        return json_encode($datas);
    }


    public function getDataNotifiacaoUnidade(Request $request)
    {
        $mydata = new \stdClass;
        $mydata->periodos = $request->get('anos');
        $numero_parametros = count($mydata->periodos);
        $querys = '';
        foreach ($mydata->periodos as $key => $value) {
            $querys .= "(
                SELECT COUNT(m{$value}.\"CODESTAB\")
                FROM public.obito{$value} AS m{$value}
                WHERE
                unidades_notificadoras.\"ID_UNIDADE\" = m{$value}.\"CODESTAB\"
                AND
                (
                    m{$value}.\"IDADE\" < '400'
                )
            ) AS c{$value}";
            if( $key < ($numero_parametros-1) ){
                $querys .= ' , ';
            }

        }

        $listUnidades = DB::table('unidades_notificadoras')
        ->select(
            'NM_UNIDADE',
            DB::raw($querys)
            )
            ->orderBy('ID_UNIDADE')
            ->get();
        $data = Array();
        $clearListe = Array();
        $j = 0;
        foreach ($listUnidades as $key => $unidade) {
            $unidadeArray = (array) $unidade;
            $sum = 0;
            foreach ($mydata->periodos as $ano) {
                $sum += $unidadeArray["c{$ano}"];
            }
            if($sum != 0){
                $unidade = array_values($unidadeArray);
                for ($i=0; $i < count($unidadeArray); $i++) {
                    $data[$j][$i] = $unidade[$i];
                }
                $j++;
            }
        }
        $datas = new \stdClass;
        $datas->data = $data;
        return json_encode($datas);
    }

}
