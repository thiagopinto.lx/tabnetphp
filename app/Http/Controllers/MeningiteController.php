<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\LoadData;
use App\Models\Meningite;
use App\Utilities\Helper;

class MeningiteController extends Controller
{
    public static $resouce = 'meningite';
    public static $resouceFullName = 'Meningite';
    public static $resouceShortName = 'Meningite';
    public static $routeBefore = ['detalhamentos', 'clustersmaps', 'getsemepdem'];
    public static $routeAfter = ['heatmap'];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $loadDatas = LoadData::where('table_name', self::$resouce)
               ->orderBy('ano', 'desc')
               ->limit(5)
               ->get();
        $newLoadDatas = array();
        $soma = 0;
        $countDatas = count($loadDatas);
        foreach($loadDatas as $loadData){
            $count = DB::table($loadData->table_name.$loadData->ano)->count();
            $loadData->count = $count;
            $loadData->tabela = $loadData->table_name.$loadData->ano;
            $loadData->updated = date_format($loadData->updated_at, 'd-m-Y');
            $newLoadDatas[] = $loadData;
            $soma = $soma+$count;
        }
        if($soma > 0 && $countDatas >0){
            $total = new \stdClass;
            $media = $soma/$countDatas;
            $total->titulo = "Média".self::$resouceShortName;
            $total->media = $media;
            $total->color = '#FF0000';
        }

        $options['titulo'] = self::$resouceFullName;
        $routeBefore = array_merge(self::$routeBefore, ['getdatabar', 'getdataline', 'getdatalinebysem']);
        $urls = Controller::createUrl(self::$resouce, $routeBefore, self::$routeAfter);
        return view('dashboard', compact('newLoadDatas', 'total', 'urls', 'options'));
    }

    public function getDataBar(Request $request)
    {
        $data = new \stdClass;
        $data->periodos =  $request->get('anos');

        $countAnos = count($data->periodos);

        if ($countAnos > 0) {
            $data->table_name = self::$resouce;
            $data->labels[] = "Casos notificados";
            $data->labels[] = "Casos confirmados";
            $data->querys[] = function($table){
                return DB::table($table)->count();
            };
            $data->querys[] = function($table){
                return DB::table($table)->where([
                    ['CLASSI_FIN', '!=', '8'],
                    ['CLASSI_FIN', '!=', '5']
                ])->count();
            };

            $dataSet = Meningite::createDataSetBar($data);
        }
        return json_encode($dataSet) ;
    }

    public function getDataLine(Request $request)
    {

        $data = new \stdClass;
        $data->table_name = self::$resouce;
        $data->periodos = $request->get('anos');
        $data->labels = self::$meses;
        $data->querys[] = function($table){
            return DB::table($table)
            ->select(DB::raw('count(*)'))
            ->groupBy(DB::raw('DATE_TRUNC(\'month\', "DT_SIN_PRI")'))
            ->get();
        };
        $data->quantMedia = 12;
        $dataSet = Meningite::createDataSetLine($data);

        return json_encode($dataSet);
    }

    public function getDataLineBySem(Request $request)
    {
        $data = new \stdClass;
        $data->table_name = self::$resouce;
        $data->periodos = $request->get('anos');
        for ($i=0; $i <52 ; $i++) {
            $data->labels[] = $i+1;
        }
        $data->querys[] = function($table){
            return DB::table($table)
            ->select(DB::raw('count(*)'))
            ->groupBy('SEM_PRI')
            ->get();
        };
        $data->quantMedia = 52;

        $data->optionLine = new \stdClass;
        $data->optionLine->steppedLine = true;
        $data->optionLine->pointRadius = 5;
        $data->optionLine->pointHoverRadius = 15;
        $data->optionLine->pointStyles = [
            'circle',
            'rectRot',
            'rect',
            'star',
            'triangle'
        ];

        $dataSet = Meningite::createDataSetLine($data);
        return json_encode($dataSet) ;
    }

    public function heatMap($tabela)
    {
      $ano = $rest = substr($tabela, -4);
      $options['inicio'] = $ano."-01-01";
      $options['fim'] = $ano."-12-31";
      $options['titulo'] = self::$resouceFullName;
      $routeAfter = array_merge(self::$routeAfter, ['getdataheatmap']);
      $urls = Controller::createUrl(self::$resouce, self::$routeBefore, $routeAfter);
      $urls['getdataheatmap'] =  route($urls['getdataheatmap'], ['tabela' => $tabela]);
      $fullName = self::$resouceFullName;
      return view('heatmap', compact('fullName', 'urls', 'options', 'ano'));
    }

    public function clustersMaps()
    {
        $loadDatas = LoadData::where('table_name', self::$resouce)
        ->orderBy('ano', 'desc')
        ->limit(5)
        ->get();
        $newLoadDatas = array();
        $soma = 0;
        $countDatas = count($loadDatas);
        foreach($loadDatas as $loadData){
            $count = DB::table($loadData->table_name.$loadData->ano)->count();
            $loadData->count = $count;
            $loadData->tabela = $loadData->table_name.$loadData->ano;
            $loadData->updated = date_format($loadData->updated_at, 'd-m-Y');
            $newLoadDatas[] = $loadData;
            $soma = $soma+$count;
        }
        if($soma > 0 && $countDatas >0){
            $total = new \stdClass;
            $media = $soma/$countDatas;
            $total->titulo = "Média".self::$resouceShortName;
            $total->media = $media;
            $total->color = '#FF0000';
        }

        $options['titulo'] = self::$resouceFullName;
        $fullName = self::$resouceFullName;
        $routeBefore = array_merge(self::$routeBefore, ['getgeocodes', 'getdatabairro', 'getdatalinebysem']);
        $urls = Controller::createUrl(self::$resouce, $routeBefore, self::$routeAfter);
        return view('clustersmaps', compact('newLoadDatas', 'total', 'urls', 'fullName', 'options'));
    }

    public function getDataClassificacao(Request $request)
    {
        $mydata = new \stdClass;
        $mydata->table_name = self::$resouce;

        $mydata->periodos = $request->get('anos');

        $mydata->criterios[] = ["Confirmados", function($tabela){
            return DB::table($tabela)->where([
                ['CLASSI_FIN', '=', '1']
            ])->count();
        }];
        $mydata->criterios[] = ["Descartados", function($tabela){
            return DB::table($tabela)->where([
                ['CLASSI_FIN', '=', '2']
            ])->count();
        }];
        $mydata->criterios[] = ["Inconclusivo", function($tabela){
            return DB::table($tabela)->where(
                DB::raw('"CLASSI_FIN" = \'8\' OR "CLASSI_FIN" IS NULL OR "CLASSI_FIN" = \' \'')
                )->count();
        }];

        $datas = new \stdClass;
        $datas->data = Meningite::createDataDetalhamento($mydata);
        return json_encode($datas);

    }

    public function getDataEvolucao(Request $request)
    {
        $mydata = new \stdClass;
        $mydata->table_name = self::$resouce;

        $mydata->periodos = $request->get('anos');

        $mydata->criterios[] = ["Alta", function($tabela){
            return DB::table($tabela)->where([
                ['EVOLUCAO', '=', '1']
            ])->count();
        }];
        $mydata->criterios[] = ["Obito pelo Agravo", function($tabela){
            return DB::table($tabela)->where([
                ['EVOLUCAO', '=', '2']
            ])->count();
        }];
        $mydata->criterios[] = ["Obito por Outros", function($tabela){
            return DB::table($tabela)->where([
                ['EVOLUCAO', '=', '3']
            ])->count();
        }];
        $mydata->criterios[] = ["Inconclusivo", function($tabela){
            return DB::table($tabela)->where(
                DB::raw('"CLASSI_FIN" = \'8\' OR "CLASSI_FIN" IS NULL OR "CLASSI_FIN" = \' \'')
                )->count();
        }];

        $datas = new \stdClass;
        $datas->data = Meningite::createDataDetalhamento($mydata);
        return json_encode($datas);
    }

    public function getDataRipsa(Request $request)
    {
        $mydata = new \stdClass;
        $mydata->table_name = self::$resouce;

        $mydata->periodos = $request->get('anos');

        $mydata->criterios[] = ["<1 Ano", function($tabela){
            return DB::table($tabela)->where([
                ['NU_IDADE_N', '<', '4001']
            ])->count();
        }];
        $mydata->criterios[] = ["1-4", function($tabela){
            return DB::table($tabela)->where([
                ['NU_IDADE_N', '>=', '4001'],
                ['NU_IDADE_N', '<=', '4004']
            ])->count();
        }];
        $mydata->criterios[] = ["5-9", function($tabela){
            return DB::table($tabela)->where([
                ['NU_IDADE_N', '>=', '4005'],
                ['NU_IDADE_N', '<=', '4009']
            ])->count();
        }];
        $mydata->criterios[] = ["10-19", function($tabela){
            return DB::table($tabela)->where([
                ['NU_IDADE_N', '>=', '4010'],
                ['NU_IDADE_N', '<=', '4019']
            ])->count();
        }];
        $mydata->criterios[] = ["20-39", function($tabela){
            return DB::table($tabela)->where([
                ['NU_IDADE_N', '>=', '4020'],
                ['NU_IDADE_N', '<=', '4039']
            ])->count();
        }];
        $mydata->criterios[] = ["40-59", function($tabela){
            return DB::table($tabela)->where([
                ['NU_IDADE_N', '>=', '4040'],
                ['NU_IDADE_N', '<=', '4059']
            ])->count();
        }];
        $mydata->criterios[] = ["60-+", function($tabela){
            return DB::table($tabela)->where([
                ['NU_IDADE_N', '>=', '4060'],
                ['NU_IDADE_N', '<=', '4999']
            ])->count();
        }];
        $datas = new \stdClass;
        $datas->data = Meningite::createDataDetalhamento($mydata);
        return json_encode($datas);
    }

    public function getDataSexo(Request $request)
    {
        $anos = $request->get('anos');
        $countAnos = count($anos);
        $data = array();
        $datas = new \stdClass;


        if ($countAnos > 0) {
            $datas->data = Meningite::getBySexo(self::$resouce, $anos);
        }else{
            $datas->data = array();
        }

        return json_encode($datas);
    }

    public function detalhamento(Request $request)
    {
        $loadDatas = LoadData::where('table_name', self::$resouce)
        ->orderBy('ano', 'desc')
        ->limit(5)
        ->get();
        $newLoadDatas = array();
        $soma = 0;
        $countDatas = count($loadDatas);
        foreach($loadDatas as $loadData){
            $count = DB::table($loadData->table_name.$loadData->ano)->count();
            $loadData->count = $count;
            $loadData->tabela = $loadData->table_name.$loadData->ano;
            $loadData->updated = date_format($loadData->updated_at, 'd-m-Y');
            $newLoadDatas[] = $loadData;
            $soma = $soma+$count;
        }
        if($soma > 0 && $countDatas >0){
            $total = new \stdClass;
            $media = $soma/$countDatas;
            $total->titulo = "Média".self::$resouceShortName;
            $total->media = $media;
            $total->color = '#FF0000';
        }

        $fullName = self::$resouceFullName;
        $routeBefore = array_merge(self::$routeBefore,
            ['getdataclassificacao',
             'getdataevolucao',
             'getdataripsa',
             'getdatasexo',
             'getdatacontatoagravo',
             'getdatasintomashemorragico',
             'getdatatipo',
             'getdatacriterioconfirmacao',
             'getdatanotificacaounidade',
             'getdatabairro'
            ]);

        $options['titulo'] = self::$resouceFullName;
        $urls = Controller::createUrl(self::$resouce, $routeBefore, self::$routeAfter);
        return view('detalhamento-meningite', compact('newLoadDatas', 'total', 'urls', 'fullName', 'options'));
    }

    public function getGeoCodes(Request $request)
    {
        $ano = $request->get('ano');
        $datas = new \stdClass;
        $loadData = LoadData::where([
            ['table_name', '=', self::$resouce],
            ['ano', '=', $ano]
        ])
        ->first();
        $geocodes = DB::table(self::$resouce.$ano)->select('NU_NOTIFIC', 'NU_ANO','ID_GEO1', 'ID_GEO2')->where(function($q){
            $q->whereNotNull('ID_GEO1');
            $q->whereNotNull('ID_GEO2');
        })
        ->get();
        $datas->ano = $ano;
        $datas->geocodes = $geocodes;
        $datas->color = $loadData->color;


        return json_encode($datas);
    }

    public function getSemEpdem(Request $request)
    {
        $loadDatas = LoadData::where('table_name', self::$resouce)
        ->orderBy('ano', 'desc')
        ->limit(5)
        ->get();
        $newLoadDatas = array();
        $soma = 0;
        $countDatas = count($loadDatas);
        foreach($loadDatas as $loadData){
            $count = DB::table($loadData->table_name.$loadData->ano)->count();
            $loadData->count = $count;
            $loadData->tabela = $loadData->table_name.$loadData->ano;
            $loadData->updated = date_format($loadData->updated_at, 'd-m-Y');
            $newLoadDatas[] = $loadData;
            $soma = $soma+$count;
        }
        if($soma > 0 && $countDatas >0){
            $total = new \stdClass;
            $media = $soma/$countDatas;
            $total->titulo = "Média Meningite";
            $total->media = $media;
            $total->color = '#FF0000';
        }

        $options['titulo'] = self::$resouceFullName;
        $fullName = self::$resouceFullName;
        $routeBefore = array_merge(self::$routeBefore, ['getdatasemepdem']);
        $urls = Controller::createUrl(self::$resouce, $routeBefore, self::$routeAfter);
        return view('semanas-ep', compact('newLoadDatas', 'total', 'urls', 'fullName', 'options'));
    }

    public function getDataSemEpdem(Request $request)
    {
        $anos = $request->get('anos');
        $countAnos = count($anos);
        $data = array();
        $datas = new \stdClass;

        if ($countAnos > 0) {

            for ($i=0; $i < $countAnos; $i++) {
                $semanas[] = DB::table(self::$resouce.$anos[$i])
                        ->select(DB::raw('count(*)'))
                        ->groupBy('SEM_PRI')
                        ->get();
            }
            //dd($semanas);

            for ($i=0; $i < 52; $i++) {
                $semana[] = $i+1;
                for ($j=0; $j < $countAnos; $j++) {
                    if (isset($semanas[$j][$i])) {
                        $semana[] = $semanas[$j][$i]->count;
                    }else {
                        $semana[] = null;
                    }
                }
                $data[] = $semana;
                unset($semana);
            }
            $datas->data = $data;
        }else {
            $datas->data = [];
        }


        return json_encode($datas);

    }

    public function show($id)
    {
    }

    public function getDataBairro(Request $request)
    {
        $anos = $request->get('anos');
        $countAnos = count($anos);
        $data = array();
        $datas = new \stdClass;

        if ($countAnos > 0) {

            for ($i=0; $i < $countAnos; $i++) {
                $bairros[] = DB::table(self::$resouce.$anos[$i])
                        ->select(DB::raw('"NM_BAIRRO", count(*) AS "COUNTB"'))
                        ->whereNotNull('NM_BAIRRO')
                        ->groupBy('NM_BAIRRO')
                        ->orderBy('COUNTB', 'desc')
                        ->limit(10)
                        ->get();
            }

            //dd($semanas);

            for ($i=0; $i < 10; $i++) {

                for ($j=0; $j < $countAnos; $j++) {
                    if (isset($bairros[$j][$i])) {
                        $bairro[] = "{$bairros[$j][$i]->NM_BAIRRO}";
                        $bairro[] = "{$bairros[$j][$i]->COUNTB}";
                    }else {
                        $bairro[] = null;
                        $bairro[] = null;
                    }
                }
                $data[] = $bairro;
                unset($bairro);
            }
            $datas->data = $data;
        }else {
            $datas->data = [];
        }

        return json_encode($datas);
    }

    public function getDataContatoAgravo(Request $request)
    {
        $mydata = new \stdClass;
        $mydata->table_name = self::$resouce;

        $mydata->periodos = $request->get('anos');

        $mydata->criterios[] = ["Sem Histório de Contato", function($tabela){
            return DB::table($tabela)->where([
                ['ANT_CONT_N', '=', '7']
            ])->count();
        }];
        $mydata->criterios[] = ["Com Histório de Contato", function($tabela){
            return DB::table($tabela)->where([
                ['ANT_CONT_N', '!=', '7']
            ])->count();
        }];

        $datas = new \stdClass;
        $datas->data = Meningite::createDataDetalhamento($mydata);
        return json_encode($datas);
    }

    public function getDataSintomasHemorragico(Request $request)
    {
        $mydata = new \stdClass;
        $mydata->table_name = self::$resouce;

        $mydata->periodos = $request->get('anos');

        $mydata->criterios[] = ["Com Sintomas", function($tabela){
            return DB::table($tabela)->where([
                ['CLI_PETEQU', '=', '1']
            ])->count();
        }];
        $mydata->criterios[] = ["Sem Sintomas", function($tabela){
            return DB::table($tabela)->where([
                ['CLI_PETEQU', '=', '2']
            ])->count();
        }];
        $mydata->criterios[] = ["Ignorados", function($tabela){
            return DB::table($tabela)->where([
                ['CLI_PETEQU', '=', '9']
            ])->count();
        }];

        $datas = new \stdClass;
        $datas->data = Meningite::createDataDetalhamento($mydata);
        return json_encode($datas);
    }

    public function getDataTipo(Request $request)
    {
        $mydata = new \stdClass;
        $mydata->table_name = self::$resouce;

        $mydata->periodos = $request->get('anos');

        $mydata->criterios[] = ["Meningococcemia", function($tabela){
            return DB::table($tabela)->where([
                ['CON_DIAGES', '=', '01']
            ])->count();
        }];
        $mydata->criterios[] = ["Meningite Meningocócica", function($tabela){
            return DB::table($tabela)->where([
                ['CON_DIAGES', '=', '02']
            ])->count();
        }];
        $mydata->criterios[] = ["Meningite Meningocócica com Meningococemia", function($tabela){
            return DB::table($tabela)->where([
                ['CON_DIAGES', '=', '03']
            ])->count();
        }];
        $mydata->criterios[] = ["Meningite Tuberculosa", function($tabela){
            return DB::table($tabela)->where([
                ['CON_DIAGES', '=', '04']
            ])->count();
        }];
        $mydata->criterios[] = ["Meningite por outras bactérias", function($tabela){
            return DB::table($tabela)->where([
                ['CON_DIAGES', '=', '05']
            ])->count();
        }];
        $mydata->criterios[] = ["Meningite não especificada", function($tabela){
            return DB::table($tabela)->where([
                ['CON_DIAGES', '=', '06']
            ])->count();
        }];
        $mydata->criterios[] = ["Meningite Asséptica", function($tabela){
            return DB::table($tabela)->where([
                ['CON_DIAGES', '=', '07']
            ])->count();
        }];
        $mydata->criterios[] = ["Meningite de outra etiologia", function($tabela){
            return DB::table($tabela)->where([
                ['CON_DIAGES', '=', '08']
            ])->count();
        }];
        $mydata->criterios[] = ["Meningite por Hemófilo", function($tabela){
            return DB::table($tabela)->where([
                ['CON_DIAGES', '=', '09']
            ])->count();
        }];
        $mydata->criterios[] = ["Meningite por Pneumococos", function($tabela){
            return DB::table($tabela)->where([
                ['CON_DIAGES', '=', '10']
            ])->count();
        }];

        $datas = new \stdClass;
        $datas->data = Meningite::createDataDetalhamento($mydata);
        return json_encode($datas);
    }

    public function getDataCriterioConfirmacao(Request $request)
    {
        $mydata = new \stdClass;
        $mydata->table_name = self::$resouce;

        $mydata->periodos = $request->get('anos');

        $mydata->criterios[] = [" Cultura", function($tabela){
            return DB::table($tabela)->where([
                ['CRITERIO', '=', '1']
            ])->count();
        }];
        $mydata->criterios[] = ["CIE", function($tabela){
            return DB::table($tabela)->where([
                ['CRITERIO', '=', '2']
            ])->count();
        }];
        $mydata->criterios[] = ["Ag. Látex", function($tabela){
            return DB::table($tabela)->where([
                ['CRITERIO', '=', '3']
            ])->count();
        }];
        $mydata->criterios[] = ["Clínico", function($tabela){
            return DB::table($tabela)->where([
                ['CRITERIO', '=', '4']
            ])->count();
        }];
        $mydata->criterios[] = ["Bacterioscopia", function($tabela){
            return DB::table($tabela)->where([
                ['CRITERIO', '=', '5']
            ])->count();
        }];
        $mydata->criterios[] = ["Quimiocitológico", function($tabela){
            return DB::table($tabela)->where([
                ['CRITERIO', '=', '6']
            ])->count();
        }];
        $mydata->criterios[] = ["Clínico Epidemiológico", function($tabela){
            return DB::table($tabela)->where([
                ['CRITERIO', '=', '7']
            ])->count();
        }];
        $mydata->criterios[] = ["Isolamento Viral", function($tabela){
            return DB::table($tabela)->where([
                ['CRITERIO', '=', '8']
            ])->count();
        }];
        $mydata->criterios[] = ["PCR", function($tabela){
            return DB::table($tabela)->where([
                ['CRITERIO', '=', '9']
            ])->count();
        }];
        $mydata->criterios[] = ["Outros", function($tabela){
            return DB::table($tabela)->where([
                ['CRITERIO', '=', '10']
            ])->count();
        }];

        $datas = new \stdClass;
        $datas->data = Meningite::createDataDetalhamento($mydata);
        return json_encode($datas);
    }

    public function getDataNotifiacaoUnidade(Request $request)
    {
        $mydata = new \stdClass;
        $mydata->periodos = $request->get('anos');
        $numero_parametros = count($mydata->periodos);
        $querys = '';
        foreach ($mydata->periodos as $key => $value) {
            $querys .= "(
                SELECT COUNT(m{$value}.\"ID_UNIDADE\")
                FROM public.meningite{$value} AS m{$value}
                WHERE
                unidades_notificadoras.\"ID_UNIDADE\" = m{$value}.\"ID_UNIDADE\"
            ) AS c{$value}";
            if( $key < ($numero_parametros-1) ){
                $querys .= ' , ';
            }

        }

        $listUnidades = DB::table('unidades_notificadoras')
        ->select(
            'NM_UNIDADE',
            DB::raw($querys)
            )
            ->orderBy('ID_UNIDADE')
            ->get();
        $data = Array();
        $clearListe = Array();
        $j = 0;
        foreach ($listUnidades as $key => $unidade) {
            $unidadeArray = (array) $unidade;
            $sum = 0;
            foreach ($mydata->periodos as $ano) {
                $sum += $unidadeArray["c{$ano}"];
            }
            if($sum != 0){
                $unidade = array_values($unidadeArray);
                for ($i=0; $i < count($unidadeArray); $i++) {
                    $data[$j][$i] = $unidade[$i];
                }
                $j++;
            }
        }
        $datas = new \stdClass;
        $datas->data = $data;
        return json_encode($datas);
    }

}
