<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Ciclo;
use App\Models\Bairro;

class CicloController extends Controller
{
  public function getDataCiclo(Request $request, $ano)
  {
    $ciclo = Ciclo::find($request->get('ciclo'));
    $ciclo->load('bairros');
    //$ciclo->bairros = $ciclo->bairros()->select('name')->get();

    foreach ($ciclo->bairros as $bairro) {
      $cicloBairro = $ciclo->cicloBairros()->select('ipla')->where('bairro_id', $bairro->id)->first();
      $bairro->ipla = $cicloBairro->ipla;
      //$bairro->load('geocodes');
      $bairro->geocodes = $bairro->geocodes()->select('lng', 'lat')->get();
    }

      return json_encode($ciclo);
  }
}
