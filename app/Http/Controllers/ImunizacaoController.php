<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\LoadSipni;
use App\Models\LoadSies;
use App\Models\ImunoSipni;
use App\Models\ImunoSies;
use App\Models\EstabelecimentoCnes;
use App\Utilities\Helper;

class ImunizacaoController extends Controller
{
    public static $resouce = 'imunizacao';
    public static $resouceFullName = 'Dados Imunização';
    public static $resouceShortName = 'Dados Imunização';
    public static $routeBefore = [];
    public static $routeAfter = [];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fullName = self::$resouceFullName;
        $imunosSipni = ImunoSipni::all();
        $imunos = array();
        foreach ($imunosSipni as $imuno) {
            if(ImunoSies::where('imuno_sipni_id', $imuno->id)->exists()){
                $imunos[] = $imuno;
            }
        }
        $imunosSipni = $imunos;
        
        return view('dashboard-imunizacao', compact('fullName', 'imunosSipni'));
    }

    public function imuno($idImuno)
    {
        $fullName = self::$resouceFullName;
        $imunoSipni = ImunoSipni::find($idImuno);
        $nomeImuno = strtolower(str_replace(" ", "_", $imunoSipni->nome));
        $tabelaSipni = "da_sipni_{$nomeImuno}_";
        $tabelaSies = "movimento_sies";
        $loadSipnis = LoadSipni::where('imuno_sipnis_id', $idImuno)->limit(6)->orderBy('ano')->get();
        $idImunosSies = array();
        $newLoadSipnis = array();
        foreach ($loadSipnis as $load) {
            $load->dosesDistribuidas = 0;

            $load->dosesAplicadas = DB::table($tabelaSipni.$load->ano)->where('imuno_sipnis_id', $idImuno)->count();

            $imunosSies = ImunoSies::where('imuno_sipni_id', $load->imuno_sipnis_id)->get();

            foreach ($imunosSies as $imuno) {
                $idImunosSies[] = $imuno->id; 
            }
            if(LoadSies::where('ano', $load->ano)->whereIn('imuno_sies_id', $idImunosSies)->exists()){
                $load->updated = date_format($load->updated_at, 'd-m-Y');
                $loadsSies = LoadSies::where('ano', $load->ano)->whereIn('imuno_sies_id', $idImunosSies)->get();
                $newLoadSies = array();
                foreach ($loadsSies as $loadSies) {
                    $loadSies->dosesDistribuidas = DB::table($tabelaSies.$loadSies->ano)
                        ->where(
                            [
                                ['imuno_sies_id', $loadSies->imuno_sies_id], 
                                ['load_sies_id', $loadSies->id]
                            ]
                            )->sum('quantidade');
                    $load->dosesDistribuidas += $loadSies->dosesDistribuidas;
                    $loadSies->updated = date_format($loadSies->updated_at, 'd-m-Y');
                    $newLoadSies[] = $loadSies;
                }
                $load->loadSies = $newLoadSies;
                $newLoadSipnis[] = $load;
            }
        }

        $routeBefore = array_merge(self::$routeBefore, ['estabelecimentos', 'estabelecimento']);
        $urls = Controller::createUrl(self::$resouce, $routeBefore, self::$routeAfter);
        

        return view('map-imunizacao', compact('fullName', 'newLoadSipnis', 'urls', 'idImuno'));
    }

    public function estabelecimentos($idImuno, $ano){
        
        $data = new \stdClass;
        $tabelaEstabCnes = "estabelecimento_cnes";
        $tabelaEstabSies = "estabelecimento_sies";
        $imunoSipni = ImunoSipni::find($idImuno);
        $nomeImuno = strtolower(str_replace(" ", "_", $imunoSipni->nome));
        $tabelaSipni = "da_sipni_{$nomeImuno}_";
        $tabelaSies = "movimento_sies";


        $loadSipni = LoadSipni::where([['imuno_sipnis_id', $idImuno], ['ano', $ano]])->first();
        $data->color = $loadSipni->color;
        
        $imunosSies = ImunoSies::where('imuno_sipni_id', (int)$idImuno)->get();
        //dd($imunosSies);
        $idImunoSies = array();
        
        foreach ($imunosSies as $value) {
            $idImunoSies[] = $value->id;
        }

        $loadSies = LoadSies::where('ano', $ano)->whereIn('imuno_sies_id', $idImunoSies)->get();

        $subQuerys = '';

        $whereImunoSies = '';

        /*
        if (count($idImunoSies)==1) {
          $whereImunoSies .= "{$tabelaSies}{$ano}.imuno_sies_id = {$idImunoSies[0]}";
        }else{
          $whereImunoSies .= "(";

          for ($is=0; $is < count($idImunoSies); $is++) { 
            $whereImunoSies .= "{$tabelaSies}{$ano}.imuno_sies_id = {$idImunoSies[$is]}";
            if($is < (count($idImunoSies)-1)){
              $whereImunoSies .= " OR ";
            }
          }

          $whereImunoSies .= ")";
        }

        $subQuerys .= "(
                          SELECT 
                              SUM({$tabelaSies}{$ano}.quantidade) AS quantidade
                          FROM
                              {$tabelaEstabSies}
                          INNER JOIN
                              {$tabelaSies}{$ano}
                          ON
                              {$tabelaEstabSies}.id = {$tabelaSies}{$ano}.estabelecimento_sies_id
                          WHERE
                              {$whereImunoSies}
                              AND
                              {$tabelaEstabSies}.estabelecimento_cnes_id = {$tabelaEstabCnes}.id 
                        ) AS doses_distribuidas";
        */
        $estabelecimentos = DB::table($tabelaEstabCnes)
        ->select(
            "{$tabelaEstabCnes}.id",
            "{$tabelaEstabCnes}.nu_latitude AS latitude",
            "{$tabelaEstabCnes}.nu_longitude AS longitude",
            "{$tabelaEstabCnes}.no_fantasia AS nome",
            DB::raw("COUNT({$tabelaSipni}{$ano}.imuno_sipnis_id) AS doses_aplicadas"),
            //DB::raw($subQuerys)
        )
        ->join("{$tabelaSipni}{$ano}", "{$tabelaSipni}{$ano}.estabelecimento_cnes_id", "=", "{$tabelaEstabCnes}.id")
        ->where("{$tabelaSipni}{$ano}.imuno_sipnis_id", "=", $idImuno)
        ->groupBy("{$tabelaEstabCnes}.id")
        ->orderBy("doses_aplicadas", "desc")
        ->get();

        $countEstabelecimentos = count($estabelecimentos);

        for ($x=0; $x < $countEstabelecimentos ; $x++) { 
            $estabelecimentos[$x]->perda_tecnica_aceitavel = 0;
            $estabelecimentos[$x]->doses_distribuidas = 0;
            
            for ($is=0; $is < count($idImunoSies); $is++) { 
                $doses_distribuidas = DB::table($tabelaEstabSies)
                ->select(DB::raw("SUM({$tabelaSies}{$ano}.quantidade) AS quantidade"))
                ->join("{$tabelaSies}{$ano}", "{$tabelaEstabSies}.id", "=", "{$tabelaSies}{$ano}.estabelecimento_sies_id")
                ->where([
                        ["{$tabelaEstabSies}.estabelecimento_cnes_id", "=", "{$estabelecimentos[$x]->id}"],
                        ["{$tabelaSies}{$ano}.imuno_sies_id", "=", "{$imunosSies[$is]->id}"]
                    ])->get();

                if($imunosSies[$is]->perda > 1){
                    $estabelecimentos[$x]->perda_tecnica_aceitavel = 
                    (($doses_distribuidas[0]->quantidade/100)*$imunosSies[$is]->perda) + $estabelecimentos[$x]->perda_tecnica_aceitavel;
                }
                $estabelecimentos[$x]->perda_tecnica_aceitavel = intval($estabelecimentos[$x]->perda_tecnica_aceitavel);
                $estabelecimentos[$x]->doses_distribuidas = $doses_distribuidas[0]->quantidade + $estabelecimentos[$x]->doses_distribuidas;
            }

            $estabelecimentos[$x]->perda_total =  $estabelecimentos[$x]->doses_distribuidas - $estabelecimentos[$x]->doses_aplicadas;
        }
        
        $data->estabelecimentos = $estabelecimentos;
        return json_encode($data);
    }

    public function estabelecimento($idImuno, $ano, $idEstabelecimento){
        
        $data = new \stdClass;
        $tabelaEstabCnes = "estabelecimento_cnes";
        $tabelaEstabSies = "estabelecimento_sies";
        $imunoSipni = ImunoSipni::find($idImuno);
        $nomeImuno = strtolower(str_replace(" ", "_", $imunoSipni->nome));
        $tabelaSipni = "da_sipni_{$nomeImuno}_";
        $tabelaSies = "movimento_sies";


        $loadSipni = LoadSipni::where([['imuno_sipnis_id', $idImuno], ['ano', $ano]])->first();
        $data->color = $loadSipni->color;
        $imunosSies = ImunoSies::where('imuno_sipni_id', $idImuno)->get();
        $estabelecimento = EstabelecimentoCnes::find($idEstabelecimento);
        $idImunoSies = array();
        
        foreach ($imunosSies as $value) {
            $idImunoSies[] = $value->id;
        }

        $loadSies = LoadSies::where('ano', $ano)->whereIn('imuno_sies_id', $idImunoSies)->get();

        $subQuerys = '';

        $whereImunoSies = '';

        if (count($idImunoSies)==1) {
          $whereImunoSies .= "{$tabelaSies}{$ano}.imuno_sies_id = {$idImunoSies[0]}";
        }else{
          $whereImunoSies .= "(";

          for ($is=0; $is < count($idImunoSies); $is++) { 
            $whereImunoSies .= "{$tabelaSies}{$ano}.imuno_sies_id = {$idImunoSies[$is]}";
            if($is < (count($idImunoSies)-1)){
              $whereImunoSies .= " OR ";
            }
          }

          $whereImunoSies .= ")";
        }

        $subQuerys .= "(
                          SELECT 
                              SUM({$tabelaSies}{$ano}.quantidade) AS quantidade
                          FROM
                              {$tabelaEstabSies}
                          INNER JOIN
                              {$tabelaSies}{$ano}
                          ON
                              {$tabelaEstabSies}.id = {$tabelaSies}{$ano}.estabelecimento_sies_id
                          WHERE
                              {$whereImunoSies}
                              AND
                              {$tabelaEstabSies}.estabelecimento_cnes_id = {$idEstabelecimento}
                              AND
                                EXTRACT('month' FROM {$tabelaSipni}{$ano}.data_de_aplicacao)
                               =
                                EXTRACT('month' FROM {$tabelaSies}{$ano}.data) 
                        ) AS doses_distribuidas_mes";

        $dosesAplicadasPerDistribuidas = DB::table("{$tabelaSipni}{$ano}")
        ->select(
            DB::raw("EXTRACT('month' FROM {$tabelaSipni}{$ano}.data_de_aplicacao) AS mes"),
            DB::raw("COUNT(DATE_TRUNC('month', {$tabelaSipni}{$ano}.data_de_aplicacao)) AS doses_aplicadas_mes"),
            DB::raw($subQuerys)
        )
        ->where("{$tabelaSipni}{$ano}.imuno_sipnis_id", "=", $idImuno)
        ->where("{$tabelaSipni}{$ano}.estabelecimento_cnes_id", "=", $idEstabelecimento)
        ->groupBy(DB::raw("DATE_TRUNC('month', {$tabelaSipni}{$ano}.data_de_aplicacao), mes, doses_distribuidas_mes"))
        ->orderBy(DB::raw("DATE_TRUNC('month', {$tabelaSipni}{$ano}.data_de_aplicacao)"))
        ->get();

        $fullName = self::$resouceFullName;
        $fullName = "{$fullName} {$estabelecimento->no_fantasia} {$ano}";

        $data->estabelecimentos = json_encode($dosesAplicadasPerDistribuidas);
        //return json_encode($data);
        return view('chart-imunizacao', compact('fullName', 'data'));
    }
    

}
