<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;
use App\Models\LoadSipni;
use App\Models\DosesAplicadasSipni;
use App\Utilities\GoogleMaps;
use Predis\Configuration\Options;
use PhpParser\Node\Scalar\MagicConst\Class_;


class DosesAplicadasSipniController extends Controller
{
    public static $resouce = 'doses-aplicadas-sipni';
    public static $tableName = 'doses_aplicadas_sipni';

    public function index($load_sipni_id)
    {
      $options['titulo'] = "Tabelas de Doses Aplicadas Sipni";
      $options['descricao'] = "Lista Doses Aplicadas Sipni";
      $options['getindex'] = route('painel.'.self::$resouce.'.getindex', compact('load_sipni_id'));
      return view('painel.doses-aplicadas-sipni', compact('options'));
    }

    public function getIndex(Request $request, $load_sipnis_id)
    {
      $tabela = self::$tableName;
      $loadSies = LoadSipni::find($load_sipnis_id);
      $tabela = $tabela.$loadSies->ano;
      $data = DosesAplicadasSipni::toDataTables($request, $tabela, $load_sipnis_id);

      $data = DosesAplicadasSipni::addColumn(
              $data,
              'edit',
                  function($item) use ($tabela){
                      return '<a class="btn btn-primary" href="" role="button"><i class="fas fa-list"></i></a>';
                  }
              );

      return $data;
    }

}
