<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;
use App\Models\LoadData;
use App\Models\Bairro;
use App\Models\BairroGeocode;
use PHPUnit\Framework\Constraint\Exception;

class BairroController extends Controller
{
    public static $route = 'bairros';
    public static $tableName = 'bairros';

    public function index()
    {
        $cores = self::$cores;
        $options['titulo'] = "Bairros";
        $options['descricao'] = "Geocodes para renderização dos limite dos bairos";
        $options['store'] = route('painel.'.self::$route.'.store');
        $options['getindex'] = route('painel.'.self::$route.'.getindex');
        return view('painel.bairros', compact('cores', 'options'));
    }

    public function getIndex(Request $request)
    {
        $tabela = self::$tableName;
        $data = Bairro::toDataTables($request, $tabela);
        $data = Bairro::addColumn(
            $data,
            'view',
            function($item){
                $id = $item->id;
                return '<a class="btn btn-primary" href="'.route('painel.bairros.map', compact('id')).'" role="button">Ver no mapa <i class="far fa-eye"></i></a>';
            }
        );
        return $data;
    }

    public function store(Request $request)
    {
        $fileKml = $request->file('fileKml');
        $tableName = self::$tableName;

        // Define o valor default para a variável que contém o nome da imagem
        $nameFile = null;

        // Verifica se informou o arquivo e se é válido
        if ($request->hasFile('fileKml') && $request->file('fileKml')->isValid()) {

            // Define um aleatório para o arquivo baseado no timestamps atual
            $name = uniqid(date('HisYmd'));

            // Recupera a extensão do arquivo
            $extension = $fileKml->getClientOriginalExtension();

            // Define finalmente o nome
            $nameFile = "{$name}.{$extension}";

            // Faz o upload:
            $upload = $fileKml->storeAs('fileskml', $nameFile);
            // Se tiver funcionado o arquivo foi armazenado em storage/app/public/categories/nomedinamicoarquivo.extensao

            // Verifica se NÃO deu certo o upload (Redireciona de volta)
            if ( !$upload )
                return redirect()
                            ->back()
                            ->with('error', 'Falha ao fazer upload')
                            ->withInput();
        }

        try{
            $xmlBairros = simplexml_load_file(storage_path('app/'.$upload));
            $bairros = $xmlBairros->Document->Folder->children();

            foreach ($bairros->Placemark as $placemark) {
                $dataBairro = $placemark->ExtendedData->SchemaData->SimpleData;
                $coordinates = explode(" ", $placemark->MultiGeometry->Polygon->outerBoundaryIs->LinearRing->coordinates);

                $bairro = new Bairro;
                $bairro->name = $dataBairro[1];
                $bairro->description = $dataBairro[1];
                $bairro->gid = $dataBairro[0];
                $bairro->cod_bairro = $dataBairro[2];

                $bairro->save();

                foreach ($coordinates as $coordinate) {
                    $latLng = explode(",", $coordinate);
                    $bairroGeocode = new BairroGeocode;
                    $bairroGeocode->lat = $latLng[1];
                    $bairroGeocode->lng = $latLng[0];
                    $bairro->geocodes()->save($bairroGeocode);
                }
            }

        }catch(Exception $e){
            dd("Arquivo mau formatado");
        }
        return "ok;";
    }

  public function map($id)
  {
    $options['titulo'] = "Mapa view";
    $bairro = Bairro::find($id);
    $bairro->load('geocodes');

    return view('painel.bairro-map', compact('bairro', 'options'));
  }

  public function autocomplete(Request $request)
  {
    $q= trim($request->input('q'));
    $options = 'Any-Latin; NFKD; [:Punctuation:] Remove; [^\\u0000-\\u007E] Remove';
    $q = transliterator_transliterate($options, $q);
    $data = Bairro::select('id','name')->whereRaw("unaccent(name) ILIKE unaccent('%{$q}%')")->get();

    return response()->json($data);

  }
}
