<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;
use App\Models\LoadSies;
use App\Models\LoadSipni;
use App\Models\EstabelecimentoCnes;
use App\Utilities\GoogleMaps;
use Predis\Configuration\Options;
use PhpParser\Node\Scalar\MagicConst\Class_;


class LoadSipniController extends Controller
{
    public static $resouce = 'load-sipni';
    public static $tableName = 'load_sipnis';

    public function index()
    {
      $cores = self::$cores;
      $options['titulo'] = "Tabelas de Carga do Sipni";
      $options['descricao'] = "Lista de Carga do Sipni";
      $options['store'] = route('painel.'.self::$resouce.'.store');
      $options['getindex'] = route('painel.'.self::$resouce.'.getindex');
      return view('painel.load-sipni', compact('options', 'cores'));
    }

    public function getIndex(Request $request)
    {
      $tabela = self::$tableName;
      $data = LoadSipni::toDataTables($request, $tabela);
      $cores = self::$cores;
      
      $data = LoadSipni::addColumn(
        $data,
        'color',
        function($item) use ($cores){
            $componente =
            '<select class="colorselector-table" name="color">';
            foreach ($cores as $key => $value){
                if($value == $item->color){
                    $componente .='<option value="'.$value.'" data-color="'.$value.'" selected>'.$key.'</option>';
                }else{
                    $componente .='<option value="'.$value.'" data-color="'.$value.'">'.$key.'</option>';
                }
            }
            $componente .='</select>';

            return '<form class="colorForm" action="'.route('painel.'.self::$resouce.'.storecolor').'" method="post" enctype="multipart/form-data">
                      <input type="hidden" name="inputId" value="'.$item->id.'">
                      '.$componente.'
                    </form>';
        }
     );

      $data = LoadSipni::addColumn(
              $data,
              'edit',
                  function($item) use ($tabela){
                    $item_id = $item->id;
                    $url = route('painel.doses-aplicadas-sipni', compact('item_id'));
                    //$url = "http://localhost";
                    return '<a class="btn btn-primary" href="'.$url.'" role="button"><i class="fas fa-list"></i></a>';
                  }
              );

      return $data;
    }

    public function store(Request $request)
    {
      $fileCsv = $request->file('fileCsv');
      $color = "";     

      // Define o valor default para a variável que contém o nome da imagem
      $nameFile = null;

      // Verifica se informou o arquivo e se é válido
      if ($request->hasFile('fileCsv') && $request->file('fileCsv')->isValid()) {

        // Define um aleatório para o arquivo baseado no timestamps atual
        $name = 'data-sipni';

        // Recupera a extensão do arquivo
        $extension = $fileCsv->getClientOriginalExtension();

        // Define finalmente o nome
        $nameFile = "{$name}.{$extension}";

        // Deletar aquivos antigo
        Storage::delete('basecsv/'.$nameFile);

        // Faz o upload:
        $upload = $fileCsv->storeAs('basecsv/', $nameFile);
        // Se tiver funcionado o arquivo foi armazenado em storage/app/public/categories/nomedinamicoarquivo.extensao

        // Verifica se NÃO deu certo o upload (Redireciona de volta)
        if ( !$upload )
            return redirect()
                        ->back()
                        ->with('error', 'Falha ao fazer upload')
                        ->withInput();
      }
      
      if(LoadSipni::csvLoad($upload, $request)){
        
        //return $upload;
        return "ok;";
      };

    }

    public function storeColor(Request $request)
    {
      //dd($request);
        $id = $request->inputId;
        $color = $request->color;
        $loadData = LoadSipni::find($id);
        $loadData->color = $request->color;
        $loadData->update();
    }

}
