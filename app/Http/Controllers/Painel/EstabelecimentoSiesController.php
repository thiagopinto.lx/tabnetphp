<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;
use App\Models\EstabelecimentoSies;
use App\Models\EstabelecimentoCnes;
use App\Utilities\GoogleMaps;
use Predis\Configuration\Options;
use PhpParser\Node\Scalar\MagicConst\Class_;


class EstabelecimentoSiesController extends Controller
{
    public static $resouce = 'estabelecimentos-sies';
    public static $tableName = 'estabelecimento_sies';

    public function index()
    {
        $options['titulo'] = "Tabelas de Estabelecimentos de Saúde do Sies";
        $options['descricao'] = "Lista Estabelecimentos de Saúde do Sies";
        $options['store'] = route('painel.'.self::$resouce.'.store');
        $options['getindex'] = route('painel.'.self::$resouce.'.getindex');
        return view('painel.estabelecimentos-sies', compact('options'));
    }

    public function getIndex(Request $request)
    {
      $tabela = self::$tableName;
      $data = EstabelecimentoSies::toDataTables($request, $tabela);

    $data = EstabelecimentoSies::addColumn(
        $data,
        'cnes',
            function($item) use ($tabela){
                $estabelecimentoSies = EstabelecimentoSies::find($item->id);
                if($estabelecimentoSies->estabelecimento_cnes_id == null){
                  return '<div class="form-group multiple-datasets">
                        <input type="text" class="form-control typeahead" placeholder="Nome CNES" name="cnes-0">
                        <input type="hidden" class="siesId" name="siesId" value="'.$item->id.'">
                      </div>';
                } else {
                  $estabelecimentoCnes = EstabelecimentoCnes::find($estabelecimentoSies->estabelecimento_cnes_id);
                  return '<div class="form-group multiple-datasets">
                        <input type="text" class="form-control typeahead" placeholder="Nome CNES" value="'.$estabelecimentoCnes->no_fantasia.'">
                        <input type="hidden" class="siesId" name="siesId" value="'.$item->id.'">
                      </div>';
                }
                
            }
        );

      $data = EstabelecimentoSies::addColumn(
              $data,
              'edit',
                  function($item) use ($tabela){
                      return '<a class="btn btn-primary" href="" role="button"><i class="far fa-edit"></i></a>';
                  }
              );

      return $data;
    }

    public function store(Request $request)
    {
        $fileHtml = $request->file('fileHtml');
        $tableName = "estabelecimento_sies";        

        // Define o valor default para a variável que contém o nome da imagem
        $nameFile = null;

        // Verifica se informou o arquivo e se é válido
        if ($request->hasFile('fileHtml') && $request->file('fileHtml')->isValid()) {

          // Define um aleatório para o arquivo baseado no timestamps atual
          $name = 'tbEstabelecimento';

          // Recupera a extensão do arquivo
          $extension = $fileHtml->getClientOriginalExtension();

          // Define finalmente o nome
          $nameFile = "{$name}.{$extension}";

          // Deletar aquivos antigo
          Storage::delete('basehtml/'.$nameFile);

          // Faz o upload:
          $upload = $fileHtml->storeAs('basehtml', $nameFile);
          // Se tiver funcionado o arquivo foi armazenado em storage/app/public/categories/nomedinamicoarquivo.extensao

          // Verifica se NÃO deu certo o upload (Redireciona de volta)
          if ( !$upload )
              return redirect()
                          ->back()
                          ->with('error', 'Falha ao fazer upload')
                          ->withInput();
        }

        if(EstabelecimentoSies::htmlLoad($tableName, $upload)){
          //return $upload;
          return "ok;";
        };
        
    }

    public function editGeocodes()
    {
        $options['titulo'] = "Edição da localização da Estabelecimentos de Saúde";
        return view('painel.estabelecimento-cnes-editgeocodes', compact('options'));
    }

    public function getDataGeocodes(Request $request)
    {
        if($request->has('per_page')){
            $per_page = $request->get('per_page');
        }else {
            $per_page = 5;
        }
        $estabelecimentos = EstabelecimentoCnes::where([
            'nu_latitude' => env('CENTER_MAP_LAT'),
            'nu_longitude' => env('CENTER_MAP_LNG')
            ])
        ->orWhere([
            'nu_latitude' => null,
            'nu_longitude' => null
        ])
        ->paginate($per_page);

        $it = $estabelecimentos->items();

        foreach ($it as $estabelecimento)
        {
            if ($estabelecimento->nu_latitude == null && $estabelecimento->nu_longitude == null) {
                $estabelecimento->nu_latitude = env('CENTER_MAP_LAT');
                $estabelecimento->nu_longitude = env('CENTER_MAP_LNG');
            }elseif ($estabelecimento->nu_latitude == '' && $notificacao->nu_longitude == '') {
                $estabelecimento->nu_latitude = env('CENTER_MAP_LAT');
                $estabelecimento->nu_longitude = env('CENTER_MAP_LNG');
            }
        }

        return $estabelecimentos->toJson();
    }

    public function editGeocode($co_cnes)
    {
        $options['titulo'] = "Edição da localização da Estabelecimento";
        $options['estabelecimento'] = EstabelecimentoCnes::where('co_cnes', $co_cnes)->first();

        if($options['estabelecimento']->nu_latitude == null && $options['estabelecimento']->nu_longitude == null){
            $options['estabelecimento']->nu_latitude = env('CENTER_MAP_LAT');
            $options['estabelecimento']->nu_longitude = env('CENTER_MAP_LNG');
        }elseif ($options['estabelecimento']->nu_latitude == '' && $options['estabelecimento']->nu_longitude == '') {
            $options['estabelecimento']->nu_latitude = env('CENTER_MAP_LAT');
            $options['estabelecimento']->nu_longitude = env('CENTER_MAP_LNG');
        }

        return view('painel.estabelecimento-cnes-editgeocode', compact('options'));
    }

    public function getGps(Request $request)
    {
        foreach ($request->checkboxs as $item) {
            $estabelecimento = EstabelecimentoCnes::where('co_cnes', $item)
                ->first();

            //dd($notificacao);
            if(
                $estabelecimento->nu_endereco != null &&
                $estabelecimento->no_logradouro != null &&
                $estabelecimento->no_bairro != null
            ){
                $coordinates = GoogleMaps::geocodeAddress(
                    $estabelecimento->nu_endereco,
                    $estabelecimento->no_logradouro,
                    $estabelecimento->no_bairro
                );

                EstabelecimentoCnes::where('co_cnes', $item)
                ->update(['nu_latitude' => $coordinates['lat'], 'nu_longitude' => $coordinates['lng']]);
            } elseif (
                $estabelecimento->nu_endereco == null &&
                $estabelecimento->no_logradouro != null &&
                $estabelecimento->no_bairro != null
            ) {
                $coordinates = GoogleMaps::geocodeAddress(
                    '',
                    $estabelecimento->no_logradouro,
                    $estabelecimento->no_bairro
                );

                EstabelecimentoCnes::where('co_cnes', $item)
                ->update(['nu_latitude' => $coordinates['lat'], 'nu_longitude' => $coordinates['lng']]);
            } elseif (
                $estabelecimento->nu_endereco == null &&
                $estabelecimento->no_logradouro == null &&
                $estabelecimento->no_bairro != null
            ) {
                $coordinates = GoogleMaps::geocodeAddress(
                    '',
                    '',
                    $estabelecimento->no_bairro
                );

                EstabelecimentoCnes::where('co_cnes', $item)
                ->update(['nu_latitude' => $coordinates['lat'], 'nu_longitude' => $coordinates['lng']]);
            }  elseif (
                $estabelecimento->nu_endereco == null &&
                $estabelecimento->no_logradouro == null &&
                $estabelecimento->no_bairro == null
            ) {
                $coordinates = GoogleMaps::geocodeAddress(
                    '',
                    '',
                    $notificacao->NM_REFEREN
                );

                EstabelecimentoCnes::where('co_cnes', $item)
                ->update(['nu_latitude' => $coordinates['lat'], 'nu_longitude' => $coordinates['lng']]);
            } elseif (
                $estabelecimento->nu_endereco != null &&
                $estabelecimento->no_logradouro == null &&
                $estabelecimento->no_bairro != null
            ) {
                $coordinates = GoogleMaps::geocodeAddress(
                    '',
                    '',
                    $estabelecimento->no_bairro
                );

                EstabelecimentoCnes::where('co_cnes', $item)
                ->update(['nu_latitude' => $coordinates['lat'], 'nu_longitude' => $coordinates['lng']]);
            } elseif (
                $estabelecimento->nu_endereco == null &&
                $estabelecimento->no_logradouro != null &&
                $estabelecimento->no_bairro == null
            ) {
                $coordinates = GoogleMaps::geocodeAddress(
                    '',
                    '',
                    $estabelecimento->no_logradouro
                );

                EstabelecimentoCnes::where('co_cnes', $item)
                ->update(['nu_latitude' => $coordinates['lat'], 'nu_longitude' => $coordinates['lng']]);
            } elseif (
                $estabelecimento->nu_endereco !=null &&
                $estabelecimento->no_logradouro != null &&
                $estabelecimento->no_bairro == null
            ) {
                $coordinates = GoogleMaps::geocodeAddress(
                    '',
                    '',
                    $estabelecimento->no_logradouro
                );

                EstabelecimentoCnes::where('co_cnes', $item)
                ->update(['nu_latitude' => $coordinates['lat'], 'nu_longitude' => $coordinates['lng']]);
            }
        }
        return json_encode(true);
    }

    public function relationship($idSies, $idCnes){
        $estabelecimento = EstabelecimentoSies::find($idSies);
        $estabelecimento->estabelecimento_cnes_id = $idCnes;
        $estabelecimento->save();
    }

}
