<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;
use App\Models\LoadData;
use App\Models\LeishVisc;


class LeishViscController extends Controller
{
    public static $resouce= 'leishvisc';

    public function index()
    {
        $cores = self::$cores;
        $options['titulo'] = "Tabelas de leishmaniose visceral";
        $options['descricao'] = "Lista tabelas leishmaniose visceral";
        $options['store'] = route('painel.'.self::$resouce.'.store');
        $options['getindex'] = route('painel.'.self::$resouce.'.getindex');
        return view('painel.agravos-anos', compact('cores', 'options'));
    }

    public function getIndex(Request $request)
    {
        $tabela = 'load_datas';
        $data = LeishVisc::toDataTables($request, $tabela, self::$resouce);
        $cores = self::$cores;

        $data = LeishVisc::addColumn(
            $data,
            'color',
            function($item) use ($cores){
                $componente =
                '<select class="colorselector-table" name="color">';
                foreach ($cores as $key => $value){
                    if($value == $item->color){
                        $componente .='<option value="'.$value.'" data-color="'.$value.'" selected>'.$key.'</option>';
                    }else{
                        $componente .='<option value="'.$value.'" data-color="'.$value.'">'.$key.'</option>';
                    }
                }
                $componente .='</select>';

                return '<form class="colorForm" action="'.route('painel.'.self::$resouce.'.storecolor').'" method="post" enctype="multipart/form-data">
                          <input type="hidden" name="inputAno" value="'.$item->ano.'">
                          '.$componente.'
                        </form>';
            }
        );

        $data = LeishVisc::addColumn(
            $data,
            'action',
            function($item){
                return '<form class="updateForm form-inline" action="'.route('painel.'.self::$resouce.'.store').'" method="post" enctype="multipart/form-data">
                          <div class="form-group">
                            <input type="hidden" class="form-control" name="inputAno" value="'.$item->ano.'">
                          </div>
                          <div class="form-group">
                            <input type="file" class="form-control-file"  name="fileDbf" required>
                          </div>
                          <div class="loadDbf d-none">
                            <div class="spinner-border" role="status">
                              <span class="sr-only">Loading...</span>
                            </div>
                          </div>
                          <button type="submit" class="carregar btn btn-primary">Carregar</button>
                        </form>';
            }
        );

        $data = LeishVisc::addColumn(
            $data,
            'link',
            function($item){
                $tabela = self::$resouce.$item->ano;
                if ($item->status == 0) {
                    $btn = 'btn-danger';
                }elseif ($item->status == 1) {
                    $btn = 'btn-warning';
                } else {
                    $btn = 'btn-success';
                }


                return '<a class="btn '.$btn.'" href="'.route('painel.notificacao.index', compact('tabela')).'" role="button">Notificações</a>';
            }
        );

        return $data;
    }

    public function store(Request $request)
    {
        $ano = $request->inputAno;
        $fileDbf = $request->file('fileDbf');
        $tableName = self::$resouce.$ano;
        $cid = 'B550';
        $idMunicipio = '221100';

        // Define o valor default para a variável que contém o nome da imagem
        $nameFile = null;

        // Verifica se informou o arquivo e se é válido
        if ($request->hasFile('fileDbf') && $request->file('fileDbf')->isValid()) {

            // Define um aleatório para o arquivo baseado no timestamps atual
            $name = uniqid(date('HisYmd'));

            // Recupera a extensão do arquivo
            $extension = $fileDbf->getClientOriginalExtension();

            // Define finalmente o nome
            $nameFile = "{$name}.{$extension}";

            // Faz o upload:
            $upload = $fileDbf->storeAs('basedbf', $nameFile);
            // Se tiver funcionado o arquivo foi armazenado em storage/app/public/categories/nomedinamicoarquivo.extensao

            // Verifica se NÃO deu certo o upload (Redireciona de volta)
            if ( !$upload )
                return redirect()
                            ->back()
                            ->with('error', 'Falha ao fazer upload')
                            ->withInput();
        }
        //create table
        if(!Schema::hasTable($tableName)){

            if(LeishVisc::createTable($tableName, $upload)){

                if (LeishVisc::dbfLoad($tableName, $upload, $ano, $cid, $idMunicipio, false)) {
                    echo "Carreguei os dados";
                    $loadData = new LoadData;
                    if(isset($request->color)){
                        $loadData->color = $request->color;
                    }
                    $loadData->ano = $ano;
                    $loadData->table_name = self::$resouce;
                    $loadData->file_name = $upload;
                    $loadData->user_id = Auth::id();
                    $loadData->status = 0;
                    $loadData->save();
                    return "Carga de dados concluida";
                } else {
                    return "A tabala foi criada mais não foi possivel carregar de dados";
                }

            }else {
                Storage::delete($upload);
                return "Não foi possivel criar tabela 02";
            }

        }else {
            if (LeishVisc::dbfLoad($tableName, $upload, $ano, $cid, $idMunicipio, true)) {
                echo "tô aqui";
                $loadData = LoadData::where('ano', $ano)->where('table_name', self::$resouce)->first();
                Storage::delete($loadData->file_name);
                if(isset($request->color)){
                    $loadData->color = $request->color;
                }
                $loadData->file_name = $upload;
                $loadData->updated_at = date('Y-m-d H:i:s');
                $loadData->update();
                return "Reload dados concluida";
            } else {
                return "Não foi possivel recarregas os dados";
            }
        }

        //return $upload;
        return "ok;";
    }

    public function storeColor(Request $request)
    {
        $ano = $request->inputAno;
        $color = $request->color;
        $loadData = LoadData::where('ano', $ano)->where('table_name', self::$resouce)->first();
        $loadData->color = $color;
        $loadData->update();
    }
}
