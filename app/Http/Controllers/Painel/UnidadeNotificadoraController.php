<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;
use App\Models\LoadData;
use App\Models\UnidadeNotificadora;


class UnidadeNotificadoraController extends Controller
{
    public static $route = 'unidadesnotificadoras';
    public static $tableName = 'unidades_notificadoras';

    public function index()
    {
        $cores = self::$cores;
        $options['titulo'] = "Unidades Notificadoras";
        $options['descricao'] = "Lista De unidade que realizam notificações";
        $options['store'] = route('painel.'.self::$route.'.store');
        $options['getindex'] = route('painel.'.self::$route.'.getindex');
        return view('painel.unidades-notificadoras', compact('cores', 'options'));
    }

    public function getIndex(Request $request)
    {
        $tabela = 'unidades_notificadoras';
        $data = UnidadeNotificadora::toDataTables($request, $tabela);

        return $data;
    }

    public function store(Request $request)
    {
        $fileDbf = $request->file('fileDbf');
        $tableName = self::$tableName;

        // Define o valor default para a variável que contém o nome da imagem
        $nameFile = null;

        // Verifica se informou o arquivo e se é válido
        if ($request->hasFile('fileDbf') && $request->file('fileDbf')->isValid()) {

            // Define um aleatório para o arquivo baseado no timestamps atual
            $name = uniqid(date('HisYmd'));

            // Recupera a extensão do arquivo
            $extension = $fileDbf->getClientOriginalExtension();

            // Define finalmente o nome
            $nameFile = "{$name}.{$extension}";

            // Faz o upload:
            $upload = $fileDbf->storeAs('basedbf', $nameFile);
            // Se tiver funcionado o arquivo foi armazenado em storage/app/public/categories/nomedinamicoarquivo.extensao

            // Verifica se NÃO deu certo o upload (Redireciona de volta)
            if ( !$upload )
                return redirect()
                            ->back()
                            ->with('error', 'Falha ao fazer upload')
                            ->withInput();
        }
        //create table
        if(!Schema::hasTable($tableName)){

            if(UnidadeNotificadora::createTable($tableName, $upload)){
                UnidadeNotificadora::dbfLoad($tableName, $upload, false);
            }else {
                return "Não foi possivel criar tabela 02";
            }

        }else {
            if (UnidadeNotificadora::dbfLoad($tableName, $upload, true)) {
                return "Reload dados concluida";
            } else {
                return "Não foi possivel recarregas os dados";
            }
        }
        Storage::delete($upload);

        //return $upload;
        return "ok;";
    }

}
