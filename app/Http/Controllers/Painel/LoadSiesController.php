<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;
use App\Models\LoadSies;
use App\Models\EstabelecimentoCnes;
use App\Utilities\GoogleMaps;
use Predis\Configuration\Options;
use PhpParser\Node\Scalar\MagicConst\Class_;


class LoadSiesController extends Controller
{
    public static $resouce = 'load-sies';
    public static $tableName = 'load_sies';

    public function index()
    {
        $options['titulo'] = "Tabelas de Carga do Sies";
        $options['descricao'] = "Lista de Carga do Sies";
        $options['store'] = route('painel.'.self::$resouce.'.store');
        $options['getindex'] = route('painel.'.self::$resouce.'.getindex');
        return view('painel.load-sies', compact('options'));
    }

    public function getIndex(Request $request)
    {
      $tabela = self::$tableName;
      $data = LoadSies::toDataTables($request, $tabela);

      $data = LoadSies::addColumn(
              $data,
              'edit',
                  function($item) use ($tabela){
                    $item_id = $item->id;
                    $url = route('painel.movimento-sies', compact('item_id'));
                      return '<a class="btn btn-primary" href="'.$url.'" role="button"><i class="fas fa-list"></i></a>';
                  }
              );

      return $data;
    }

    public function store(Request $request)
    {
        $fileHtml = $request->file('fileHtml');
        $tableName = "estabelecimento_sies";        

        // Define o valor default para a variável que contém o nome da imagem
        $nameFile = null;

        // Verifica se informou o arquivo e se é válido
        if ($request->hasFile('fileHtml') && $request->file('fileHtml')->isValid()) {

          // Define um aleatório para o arquivo baseado no timestamps atual
          $name = 'tbEstabelecimento';

          // Recupera a extensão do arquivo
          $extension = $fileHtml->getClientOriginalExtension();

          // Define finalmente o nome
          $nameFile = "{$name}.{$extension}";

          // Deletar aquivos antigo
          Storage::delete('basehtml/'.$nameFile);

          // Faz o upload:
          $upload = $fileHtml->storeAs('basehtml', $nameFile);
          // Se tiver funcionado o arquivo foi armazenado em storage/app/public/categories/nomedinamicoarquivo.extensao

          // Verifica se NÃO deu certo o upload (Redireciona de volta)
          if ( !$upload )
              return redirect()
                          ->back()
                          ->with('error', 'Falha ao fazer upload')
                          ->withInput();
        }

        if(LoadSies::htmlLoad($tableName, $upload)){
          //return $upload;
          return "ok;";
        };
        
    }

}
