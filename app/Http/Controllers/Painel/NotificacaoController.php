<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;
use App\Models\Notificacao;
use App\Utilities\GoogleMaps;
use Predis\Configuration\Options;
use PhpParser\Node\Scalar\MagicConst\Class_;

class NotificacaoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($tabela)
    {
        $options['titulo'] = "Lista de Notificações";
        $porcentagem = Notificacao::getPorcentagemGeocode($tabela);
        return view('painel.notificacao', compact('tabela', 'porcentagem', 'options'));
    }

    public function getindex(Request $request, $tabela)
    {

        $data = Notificacao::toDataTables($request, $tabela);

        $data = Notificacao::addColumn(
                $data,
                'checkbox',
                    function($item){
                        return '<div class="form-check">
                                    <input class="form-check-input" name="checkboxs[]" type="checkbox" value="'.$item->NU_NOTIFIC.'" id="defaultCheck1">
                                </div>';
                    }
                );

        $data = Notificacao::addColumn(
                $data,
                'edit',
                    function($item) use ($tabela){
                        $agravo = $item->NU_NOTIFIC;
                        return '<a class="btn btn-primary" href="'.route('painel.notificacao.editgeocode', ['tabela' => $tabela, 'agravo'=> $agravo]).'" role="button"><i class="far fa-edit"></i></a>';
                    }
                );

        return $data;
    }

    public function getGps(Request $request, $tabela)
    {
        foreach ($request->checkboxs as $item) {
            $notificacao = DB::table($tabela)
                ->where('NU_NOTIFIC', $item)
                ->first();

            //dd($notificacao);
            if(
                $notificacao->NU_NUMERO != null &&
                $notificacao->NM_LOGRADO != null &&
                $notificacao->NM_BAIRRO != null
            ){
                $coordinates = GoogleMaps::geocodeAddress(
                    $notificacao->NU_NUMERO,
                    $notificacao->NM_LOGRADO,
                    $notificacao->NM_BAIRRO
                );

                DB::table($tabela)
                ->where('NU_NOTIFIC', $item)
                ->update(['ID_GEO1' => $coordinates['lat'], 'ID_GEO2' => $coordinates['lng']]);
            } elseif (
                $notificacao->NU_NUMERO == null &&
                $notificacao->NM_LOGRADO != null &&
                $notificacao->NM_BAIRRO != null
            ) {
                $coordinates = GoogleMaps::geocodeAddress(
                    '',
                    $notificacao->NM_LOGRADO,
                    $notificacao->NM_BAIRRO
                );

                DB::table($tabela)
                ->where('NU_NOTIFIC', $item)
                ->update(['ID_GEO1' => $coordinates['lat'], 'ID_GEO2' => $coordinates['lng']]);
            } elseif (
                $notificacao->NU_NUMERO == null &&
                $notificacao->NM_LOGRADO == null &&
                $notificacao->NM_BAIRRO != null
            ) {
                $coordinates = GoogleMaps::geocodeAddress(
                    '',
                    '',
                    $notificacao->NM_BAIRRO
                );

                DB::table($tabela)
                ->where('NU_NOTIFIC', $item)
                ->update(['ID_GEO1' => $coordinates['lat'], 'ID_GEO2' => $coordinates['lng']]);
            }  elseif (
                $notificacao->NU_NUMERO == null &&
                $notificacao->NM_LOGRADO == null &&
                $notificacao->NM_BAIRRO == null
            ) {
                $coordinates = GoogleMaps::geocodeAddress(
                    '',
                    '',
                    $notificacao->NM_REFEREN
                );

                DB::table($tabela)
                ->where('NU_NOTIFIC', $item)
                ->update(['ID_GEO1' => $coordinates['lat'], 'ID_GEO2' => $coordinates['lng']]);
            } elseif (
                $notificacao->NU_NUMERO != null &&
                $notificacao->NM_LOGRADO == null &&
                $notificacao->NM_BAIRRO != null
            ) {
                $coordinates = GoogleMaps::geocodeAddress(
                    '',
                    '',
                    $notificacao->NM_BAIRRO
                );

                DB::table($tabela)
                ->where('NU_NOTIFIC', $item)
                ->update(['ID_GEO1' => $coordinates['lat'], 'ID_GEO2' => $coordinates['lng']]);
            } elseif (
                $notificacao->NU_NUMERO == null &&
                $notificacao->NM_LOGRADO != null &&
                $notificacao->NM_BAIRRO == null
            ) {
                $coordinates = GoogleMaps::geocodeAddress(
                    '',
                    '',
                    $notificacao->NM_LOGRADO
                );

                DB::table($tabela)
                ->where('NU_NOTIFIC', $item)
                ->update(['ID_GEO1' => $coordinates['lat'], 'ID_GEO2' => $coordinates['lng']]);
            } elseif (
                $notificacao->NU_NUMERO !=null &&
                $notificacao->NM_LOGRADO != null &&
                $notificacao->NM_BAIRRO == null
            ) {
                $coordinates = GoogleMaps::geocodeAddress(
                    '',
                    '',
                    $notificacao->NM_LOGRADO
                );

                DB::table($tabela)
                ->where('NU_NOTIFIC', $item)
                ->update(['ID_GEO1' => $coordinates['lat'], 'ID_GEO2' => $coordinates['lng']]);
            }
        }
        return json_encode(true);
    }

    public function editGeocode($tabela, $notificacao)
    {
        $options['titulo'] = "Edição da localização da Notificações";
        $options['notificacao'] = DB::table($tabela)->where('NU_NOTIFIC', $notificacao)->first();

        if($options['notificacao']->ID_GEO1 == null && $options['notificacao']->ID_GEO2 == null){
            $options['notificacao']->ID_GEO1 = env('CENTER_MAP_LAT');
            $options['notificacao']->ID_GEO2 = env('CENTER_MAP_LNG');
        }elseif ($options['notificacao']->ID_GEO1 == '' && $options['notificacao']->ID_GEO2 == '') {
            $options['notificacao']->ID_GEO1 = env('CENTER_MAP_LAT');
            $options['notificacao']->ID_GEO2 = env('CENTER_MAP_LNG');
        }

        return view('painel.notificacao-editgeocode', compact('tabela', 'notificacao', 'options'));
    }

    public function editGeocodes($tabela)
    {
        $options['titulo'] = "Edição da localização da Notificações";
        return view('painel.notificacao-editgeocodes', compact('tabela', 'options'));
    }

    public function getDataGeocodes(Request $request, $tabela)
    {
        if($request->has('per_page')){
            $per_page = $request->get('per_page');
        }else {
            $per_page = 5;
        }
        $notificacoes = DB::table($tabela)
        ->where([
            'ID_GEO1' => env('CENTER_MAP_LAT'),
            'ID_GEO2' => env('CENTER_MAP_LNG')
            ])
        ->orWhere([
            'ID_GEO1' => null,
            'ID_GEO2' => null
        ])
        ->paginate($per_page);

        $it = $notificacoes->items();

        foreach ($it as $notificacao)
        {
            if ($notificacao->ID_GEO1 == null && $notificacao->ID_GEO2 == null) {
                $notificacao->ID_GEO1 = env('CENTER_MAP_LAT');
                $notificacao->ID_GEO2 = env('CENTER_MAP_LNG');
            }elseif ($notificacao->ID_GEO1 == '' && $notificacao->ID_GEO2 == '') {
                $notificacao->ID_GEO1 = env('CENTER_MAP_LAT');
                $notificacao->ID_GEO2 = env('CENTER_MAP_LNG');
            }
        }

        return $notificacoes->toJson();
    }

    public function updateGeocode(Request $request, $tabela)
    {
        $notificacao = DB::table($tabela)->where('NU_NOTIFIC', $request->get('NU_NOTIFIC'))
        ->update([
            'ID_GEO1' => $request->get('ID_GEO1'),
            'ID_GEO2' => $request->get('ID_GEO2'),
            'updated_at' => date('Y-m-d H:i:s')
            ]);

        return redirect()->route('painel.notificacao.index', ['tabela' => $tabela]);
    }

    public function updateGeocodes(Request $request, $tabela)
    {
        $notificacoes = $request->get('notificacoes');

        foreach ($notificacoes as $notificacao) {
            $notificacao = DB::table($tabela)->where('NU_NOTIFIC', $notificacao['NU_NOTIFIC'])
            ->update([
                'ID_GEO1' => $notificacao['ID_GEO1'],
                'ID_GEO2' => $notificacao['ID_GEO2'],
                'updated_at' => date('Y-m-d H:i:s')
                ]);

            return json_encode(true);
        }

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
