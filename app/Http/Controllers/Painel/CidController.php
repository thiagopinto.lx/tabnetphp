<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;
use App\Models\LoadData;
use App\Models\Cid;


class CidController extends Controller
{
    public static $route = 'cids';
    public static $tableName = 'cids';

    public function index()
    {
        $cores = self::$cores;
        $options['titulo'] = "Codigo Internacional de Doencas";
        $options['descricao'] = "Lista De Codigo Internacional de Doencas";
        $options['store'] = route('painel.'.self::$route.'.store');
        $options['getindex'] = route('painel.'.self::$route.'.getindex');
        return view('painel.cids', compact('cores', 'options'));
    }

    public function getIndex(Request $request)
    {
        $tabela = 'cids';
        $data = Cid::toDataTables($request, $tabela);

        return $data;
    }

    public function store(Request $request)
    {
        $fileDbf = $request->file('fileDbf');
        $tableName = self::$tableName;

        // Define o valor default para a variável que contém o nome da imagem
        $nameFile = null;

        // Verifica se informou o arquivo e se é válido
        if ($request->hasFile('fileDbf') && $request->file('fileDbf')->isValid()) {

            // Define um aleatório para o arquivo baseado no timestamps atual
            $name = uniqid(date('HisYmd'));

            // Recupera a extensão do arquivo
            $extension = $fileDbf->getClientOriginalExtension();

            // Define finalmente o nome
            $nameFile = "{$name}.{$extension}";

            // Faz o upload:
            $upload = $fileDbf->storeAs('basedbf', $nameFile);
            // Se tiver funcionado o arquivo foi armazenado em storage/app/public/categories/nomedinamicoarquivo.extensao

            // Verifica se NÃO deu certo o upload (Redireciona de volta)
            if ( !$upload )
                return redirect()
                            ->back()
                            ->with('error', 'Falha ao fazer upload')
                            ->withInput();
        }
        //create table
        if(!Schema::hasTable($tableName)){

            if(Cid::createTable($tableName, $upload)){
                Cid::dbfLoad($tableName, $upload, false);
            }else {
                return "Não foi possivel criar tabela 02";
            }

        }else {
            if (Cid::dbfLoad($tableName, $upload, true)) {
                return "Reload dados concluida";
            } else {
                return "Não foi possivel recarregas os dados";
            }
        }
        Storage::delete($upload);

        //return $upload;
        return "ok;";
    }

}
