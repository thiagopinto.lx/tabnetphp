<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public static $cores = [
        'blue'        => '#1976d2',
        'light blue'  => '#0288d1',
        'indigo'      => '#303f9f',
        'purple'      => '#7b1fa2',
        'pink'        => '#e83e8c',
        'pink dark'   => '#c2185b',
        'Cherry 01'   => '#FBD3E9',
        'Cherry 02'   => '#BB377D',
        'Ash 01'      => '#606c88',
        'Ash 02'      => '#3f4c6b',
        'Virgin 01'   => '#C9FFBF',
        'Virgin 02'   => '#FFAFBD',
        'Earthly 01'  => '#649173',
        'Earthly 02'  => '#DBD5A4',
        'DirtyFog 01' => '#B993D6',
        'DirtyFog 02' => '#8CA6DB',
        'palhetaA 01' => '#6F694E',
        'palhetaA 02' => '#65D0B2',
        'palhetaA 03' => '#D8F546',
        'palhetaA 04' => '#FF724B',
        'palhetaA 05' => '#D6523E',
        'palhetaB 01' => '#EFDFBB',
        'palhetaB 02' => '#DCB85A',
        'palhetaB 03' => '#046C46',
        'palhetaB 04' => '#043A33',
        'palhetaB 05' => '#041724',
        'palhetaC 01' => '#5F0B2B',
        'palhetaC 02' => '#D11638',
        'palhetaC 03' => '#F08801',
        'palhetaC 04' => '#FACE00',
        'palhetaC 05' => '#ADA20B',
        'palhetaD 01' => '#745437',
        'palhetaD 02' => '#6D815A',
        'palhetaD 03' => '#5AB28E',
        'palhetaD 04' => '#BAE294',
        'palhetaD 05' => '#EDFCB0',
        'palhetaE 01' => '#00A698',
        'palhetaE 02' => '#8A4F2A',
        'palhetaE 03' => '#DB362C',
        'palhetaE 05' => '#F2CD39',
        'palhetaF 01' => '#F05353',
        'palhetaF 02' => '#E28966',
        'palhetaF 03' => '#D7AA7C',
        'palhetaF 04' => '#D7E7CE',
        'palhetaF 05' => '#F7EBC8',
        'palhetaF 01' => '#52BD97',
        'palhetaF 02' => '#A5CB97',
        'palhetaF 03' => '#E5DB8D',
        'palhetaF 04' => '#FFA580',
        'palhetaF 05' => '#FF4159',
        'palhetaF 01' => '#B8BD79',
        'palhetaF 02' => '#FED18E',
        'palhetaF 03' => '#FF8C57',
        'palhetaF 04' => '#EF4F43',
        'palhetaF 05' => '#373A29',
        'dark red'    => '#d32f2f',
        'orange'      => '#fd7e14',
        'dark orange' => '#e64a19',
        'yellow'      => '#f6c23e',
        'lime'        => '#afb42b',
        'green'       => '#1cc88a',
        'teal'        => '#20c9a6',
        'cyan'        => '#36b9cc',
        'white'       => '#fff',
        'gray'        => '#858796',
        'gray-dark'   => '#5a5c69',
        'light'       => '#f8f9fc'
    ];

    public static $meses = [
        'Janeiro',
        'Fevereiro',
        'Março',
        'Abril',
        'Maio',
        'Junho',
        'Julho',
        'Agosto',
        'Setembro',
        'Outubro',
        'Novembro',
        'Desembro'
    ];

    public function createUrl($resouce, $routeBefore=null, $routeAfter=null){

        $urls[$resouce] = $resouce;
        if ($routeBefore != null) {
            foreach ($routeBefore as $route) {
                $urls[$route] = route($resouce.'.'.$route);
            }
        }

        if ($routeAfter != null) {
            foreach ($routeAfter as $route) {
                $urls[$route] = $resouce.'.'.$route;
            }
        }

        return $urls;

    }

    public function getDataHeatMap(Request $request, $tabela)
    {
      $inicio = $request->get('inicio');
      $fim = $request->get('fim');

      $notificacoes = DB::table($tabela)->select('ID_GEO1', 'ID_GEO2')->where(function($q) use ($inicio, $fim){
          $q->where('DT_NOTIFIC', '>=', $inicio);
          $q->where('DT_NOTIFIC', '<=', $fim);
          $q->whereNotNull('ID_GEO1');
          $q->whereNotNull('ID_GEO2');
      })
      ->get();

        return json_encode($notificacoes);
    }

}
