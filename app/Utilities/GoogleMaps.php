<?php

namespace App\Utilities;

class GoogleMaps{

    public static function geocodeAddress($number, $address, $neighborhood){
        $client = new \GuzzleHttp\Client();

        $coordinates['lat'] = null;
        $coordinates['lng'] = null;

        $url[0] = 'https://maps.googleapis.com/maps/api/geocode/json?address='.
        urlencode($number.' '.$address.', '.$neighborhood.', '.env('GOOGLE_MAPS_CITY').', '.env('GOOGLE_MAPS_STATE'))
        .'&key='.env('GOOGLE_MAPS_KEY');

        $url[1] = 'https://maps.googleapis.com/maps/api/geocode/json?address='.
        urlencode($address.', '.$neighborhood.', '.env('GOOGLE_MAPS_CITY').', '.env('GOOGLE_MAPS_STATE'))
        .'&key='.env('GOOGLE_MAPS_KEY');

        $url[2] = 'https://maps.googleapis.com/maps/api/geocode/json?address='.
        urlencode($neighborhood.', '.env('GOOGLE_MAPS_CITY').', '.env('GOOGLE_MAPS_STATE'))
        .'&key='.env('GOOGLE_MAPS_KEY');
        $request = $client->get($url[0]);
        $geocodeData = json_decode( $request->getBody() );

        if($geocodeData->status == 'ZERO_RESULTS'){
            $request = $client->get($url[1]);
            $geocodeData = json_decode( $request->getBody() );

            if($geocodeData->status == 'ZERO_RESULTS'){
                $request = $client->get($url[2]);
                $geocodeData = json_decode( $request->getBody() );
                echo $url[2];
            }
        }


        if(!empty($geocodeData)
          && $geocodeData->status != 'ZERO_RESULTS'
          && isset( $geocodeData->results )
          && isset( $geocodeData->results[0] ) ){

            $coordinates['lat'] = $geocodeData->results[0]->geometry->location->lat;
            $coordinates['lng'] = $geocodeData->results[0]->geometry->location->lng;
        }

        return $coordinates;
    }

}
