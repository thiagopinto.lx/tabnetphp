<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Schema\Blueprint;
use App\Models\EstabelecimentoCnes;
use App\Models\EstabelecimentoSies;
use App\Models\ImunoSies;
use App\Models\MovimentoSies;
use DOMDocument;
use DateTime;


class LoadSies extends Model
{
  public function imuno()
  { 
    $imuno = ImunoSies::where('id', $this->imuno_sies_id)->get();
    return $imuno[0];
  }

  protected $guarded = [];
  public static function toDataTables($request, $tabela, $where = NULL)
  {

    try {
      $columnsName = array();
      $columnsSearchable = array();

      $foreignKeys = null;
      foreach ($request->columns as $column) {

        if ($column['orderable'] != 'false') {
          if (strpos($column['name'], '_id') !== false) {
            
            $foreignTable[0] = substr($column['name'], 0, strpos($column['name'], "_id"));
            $foreignTable[1] = substr($column['name'], strpos($column['name'], "_id")+1, 2);
            
            $foreignTable[] = $column['name'];
            $foreignKeys[] = $foreignTable;
            $columnsName[] = $foreignTable[0] .
              '.nome';
          } else {
            $columnsName[] = $tabela . '.' . $column['name'];
          }
        }

        if ($column['searchable'] != 'false') {
          $columnsSearchable[] = $tabela . '.' . $column['name'];
        }
      }
      $columnsNameCont = count($columnsName);
      $columnsSearchableCont = count($columnsSearchable);

      if ($where == NULL) {
        $totalData = DB::table($tabela)->count();
      } else {
        $totalData = DB::table($tabela)->where("agravo", $where)->count();
      }

      $totalFiltered = $totalData;
      $limit = $request->input('length');
      $start = $request->input('start');
      $order = $columnsName[$request->input('order.0.column')];
      $dir = $request->input('order.0.dir');

      if (empty($request->input('search.value'))) {
        if ($where == NULL) {
          $query = DB::table($tabela)
            ->select($columnsName)
            ->offset($start)
            ->limit($limit)
            ->orderBy($order, $dir);
        } else {
          $query = DB::table($tabela)
            ->select($columnsName)
            ->where("agravo", $where)
            ->offset($start)
            ->limit($limit)
            ->orderBy($order, $dir);
        }

        if ($foreignKeys !== null) {
          foreach ($foreignKeys as $foreignKey) {
            $query->join(
              $foreignKey[0],
              $foreignKey[0] . '.' . $foreignKey[1],
              '=',
              $tabela . '.' . $foreignKey[2]
            );
          }
        }

        $registros = $query->get();
      } else {

        $search = $request->input('search.value');
        $query = DB::table($tabela)
          ->select($columnsName)
          ->where(function ($q) use ($columnsSearchable, $search) {
            foreach ($columnsSearchable as $columnSearchable) {
              $q->orWhere($columnSearchable, 'ILIKE', "%$search%");
              if (isset($where)) {
                $q->where("agravo", $where);
              }
            }
          })
          ->offset($start)
          ->limit($limit)
          ->orderBy($order, $dir);
        $registros = $query->get();

        $totalFiltered = DB::table($tabela)
          ->where(function ($q) use ($columnsSearchable, $search) {
            foreach ($columnsSearchable as $columnSearchable) {
              $q->orWhere($columnSearchable, 'ILIKE', "%$search%");
              if (isset($where)) {
                $q->where("agravo", $where);
              }
            }
          })
          ->count();
      }

      $data = array();
      if (!empty($registros)) {
        foreach ($registros as $registro) {
          if ($foreignKeys !== null) {
            $nestedData = (array) $registro;
            foreach ($foreignKeys as $foreignKey) {
              $nestedData[$foreignKey[2]] = $nestedData['nome'];
              unset($nestedData['nome']);
            }
          } else {
            $nestedData = (array) $registro;
          }

          $data[] = $nestedData;
        }

        $datas = array(
          "draw"            => intval($request->input('draw')),
          "recordsTotal"    => intval($totalData),
          "recordsFiltered" => intval($totalFiltered),
          "data"            => $data
        );
      } else {
        $datas = null;
      }
    } catch (\Throwable $th) {
      dd($th);
      $datas = null;
    }

    return json_encode($datas);
  }

  //carregar dados
  public static function htmlLoad($tableName, $upload)
  {
    error_reporting(-1);
    ini_set("display_errors", 1);
    $delimitador = ';';
    $cerca = '"';
    
    $file = file_get_contents(storage_path('app/' . $upload));

    if ($file) {

      $dom = new DOMDocument();
      libxml_use_internal_errors(true);

      $dom->loadHTML($file);
      $imuno = null;
      $cabecalhoTabela = array();
      $is_imuno = false;
      $dose = null;
      $ml = null;
      $maiorData = null;
      $menorData = null;
      $mensager = false;

      DB::beginTransaction();
      $tables = $dom->getElementsByTagName('table');
      for ($i = 1; $i < count($tables) - 1; $i++) {

        $trs = $tables[$i]->getElementsByTagName('tr');
        $no_fantasia = $trs[0]->nodeValue;
        $no_fantasia = str_replace("Requisitante:", "", $no_fantasia);
        $no_fantasia = str_replace("-TERESINA/PI", "", $no_fantasia);
        $no_fantasia = str_replace("TERESINA/PI", "", $no_fantasia);
        $no_fantasia = str_replace("-PI", "", $no_fantasia);
        $no_fantasia = str_replace("/PI", "", $no_fantasia);
        $no_fantasia = str_replace(":", "", $no_fantasia);
        $no_fantasia = trim($no_fantasia);
        $estabelecimento = EstabelecimentoSies::updateOrCreate(['no_fantasia' => $no_fantasia]);
        $estabelecimento->no_fantasia = $no_fantasia;
        $estabelecimento->save();
        if (count($trs) > 2) {
          $mensager = true;

          if ($imuno == null) {

            $imuno = $trs[1]->nodeValue;
            if (strpos($imuno, "VACINA") && !strpos($imuno, "P/VACINA")) {

              $is_imuno = true;

              if (strpos($imuno, "DOSE")) {
                $dose = (int) trim(substr($imuno, strpos($imuno, "DOSE") - 3, 2));
              } elseif (strpos($imuno, "SERINGA")) {
                $dose = 1;
              } elseif (strpos($imuno, " ML ")) {
                $tmp = trim(substr($imuno, strpos($imuno, "ML") - 3, 2));
                $ml = (float) str_replace(",", ".", $tmp);
              } elseif (strpos($imuno, "UNIDOSE")) {
                $dose = 1;
              } elseif (strpos($imuno, "dose")) {
                $dose = (int) trim(substr($imuno, strpos($imuno, "dose") - 3, 2));
              } elseif (strpos($imuno, "DS")) {
                $dose = (int) trim(substr($imuno, strpos($imuno, "DS") - 3, 2));
              } else {
                return "Não foi posssivel carregar este arquivo, não achei o imuno!";
              }

              $imuno = str_replace("Material:", "", $imuno);
              $imuno = str_replace("- FRASCO", "", $imuno);
              $imuno = str_replace("- AMPOLA:", "", $imuno);
              $imuno = str_replace("-FRASCO", "", $imuno);
              $imuno = str_replace("-AMPOLA:", "", $imuno);
              $imuno = str_replace("- SERINGA:", "", $imuno);
              $imuno = str_replace("-SERINGA:", "", $imuno);
              $imuno = str_replace("-", " ", $imuno);
              $imuno = trim($imuno);
              $imunoSies = ImunoSies::updateOrCreate(['nome' => $imuno]);
              $imunoSies->nome = $imuno;
              $imunoSies->save();
              echo('<div class="p-3 mb-2 bg-success text-white">Imuno: '.$imunoSies->nome.'</div>');

              if (empty($cabecalhoTabela)) {
                $colunas = $trs[2]->getElementsByTagName('td');
                foreach ($colunas as $coluna) {
                  $cabecalhoColuna = strtolower($coluna->nodeValue);
                  $cabecalhoColuna = str_replace(".", "", $cabecalhoColuna);
                  $cabecalhoColuna = str_replace(" ", "_", $cabecalhoColuna);

                  $cabecalhoTabela[] = $cabecalhoColuna;
                }
              }
              $datas = null;
              for ($j = 3; $j < count($trs)-2; $j++) { 
                $colunas = $trs[$j]->getElementsByTagName('td');
                $dadosTabela = null;
                foreach ($colunas as $coluna) {
                  $dadosTabela[] = $coluna->nodeValue;
                }
                if(count($cabecalhoTabela) == count($dadosTabela)){
                  $datas[] = array_combine($cabecalhoTabela, $dadosTabela);
                }
                
              }
              
              try {
                foreach ($datas as $data) {
                  $formato = 'd/m/Y';
                  $data['data'] = DateTime::createFromFormat($formato, $data['data']);
                  //$data['data'] = str_replace("/", "-", $data['data']);
                  //$data['data'] = date('Y-m-d', strtotime($data['data']));
                  $data['mov'] = (int)$data['mov'];
                  //$data['validade'] = str_replace("/", "-", $data['validade']);
                  //$data['validade'] = date('Y-m-d', strtotime($data['validade']));
                  $data['validade'] = DateTime::createFromFormat($formato, $data['validade']);
                  $data['quantidade'] = (int)str_replace(".", "", $data['quantidade']);
                  $data['valor_total'] = str_replace(".", "", $data['valor_total']);
                  $data['valor_total'] = (float)str_replace(",", ".", $data['valor_total']);
  
                  if ($maiorData == null) {
                    $maiorData = $data['data'];
                  }else{
                    if($data['data'] > $maiorData){
                      $maiorData = $data['data'];
                    }
                  }
  
                  if ($menorData == null) {
                    $menorData = $data['data'];
                  }else{
                    if($data['data'] < $menorData){
                      $menorData = $data['data'];
                    }
                  }

                  $tableName = "movimento_sies".$data['data']->format('Y');
                  if(!Schema::hasTable($tableName)){
                    LoadSies::createTable($tableName);
                  }

                  $loadSies = LoadSies::updateOrCreate(
                    [
                      'ano' => $data['data']->format('Y'),
                      'imuno_sies_id' => $imunoSies->id
                    ]
                  );
                  //echo($estabelecimento->id. '/n<br>');
                  $data['imuno_sies_id'] = $imunoSies->id;
                  $data['estabelecimento_sies_id'] = $estabelecimento->id;
                  $data['load_sies_id'] = $loadSies->id;
                  $data['created_at'] = $loadSies->created_at;
                  $data['updated_at'] = date('Y-m-d H:i:s');
                  DB::table($tableName)
                  ->updateOrInsert(
                    [
                      'imuno_sies_id' => $data['imuno_sies_id'],
                      'estabelecimento_sies_id' => $data['estabelecimento_sies_id'],
                      'data' => $data['data'],
                      'nfm' => $data['nfm'],
                      'mov' => $data['mov'],
                      'lote' => $data['lote'] 
                    ],
                    $data
                  );
    
                }
              } catch (\Throwable $th) {
                echo($th);
                DB::rollback();
              }

              
            }
          } else {
            if ($is_imuno) {
              
              if (!empty($cabecalhoTabela)) {
                $datas = null;
                for ($j = 1; $j < count($trs)-2; $j++) { 
                  $colunas = $trs[$j]->getElementsByTagName('td');
                  $dadosTabela = null;
                  foreach ($colunas as $coluna) {
                    $dadosTabela[] = $coluna->nodeValue;
                  }
                  if(count($cabecalhoTabela) == count($dadosTabela)){
                    $datas[] = array_combine($cabecalhoTabela, $dadosTabela);
                  }
                  
                }

                try {
                  foreach ($datas as $data) {
                    $formato = 'd/m/Y';
                    $data['data'] = DateTime::createFromFormat($formato, $data['data']);
                    //$data['data'] = str_replace("/", "-", $data['data']);
                    //$data['data'] = date('Y-m-d', strtotime($data['data']));
                    $data['mov'] = (int)$data['mov'];
                    //$data['validade'] = str_replace("/", "-", $data['validade']);
                    //$data['validade'] = date('Y-m-d', strtotime($data['validade']));
                    $data['validade'] = DateTime::createFromFormat($formato, $data['validade']);
                    $data['quantidade'] = (int)str_replace(".", "", $data['quantidade']);
                    $data['valor_total'] = str_replace(".", "", $data['valor_total']);
                    $data['valor_total'] = (float)str_replace(",", ".", $data['valor_total']);
    
                    if ($maiorData == null) {
                      $maiorData = $data['data'];
                    }else{
                      if($data['data'] > $maiorData){
                        $maiorData = $data['data'];
                      }
                    }
    
                    if ($menorData == null) {
                      $menorData = $data['data'];
                    }else{
                      if($data['data'] < $menorData){
                        $menorData = $data['data'];
                      }
                    }
                    
                    $tableName = "movimento_sies".$data['data']->format('Y');
                    if(!Schema::hasTable($tableName)){
                      LoadSies::createTable($tableName);
                    }

                    $loadSies = LoadSies::updateOrCreate(
                      [
                        'ano' => $data['data']->format('Y'),
                        'imuno_sies_id' => $imunoSies->id
                      ]
                    );

                    $loadSies->save();
                    //echo($estabelecimento->id. '/n<br>');
                    $data['imuno_sies_id'] = $imunoSies->id;
                    $data['estabelecimento_sies_id'] = $estabelecimento->id;
                    $data['load_sies_id'] = $loadSies->id;
                    $data['created_at'] = $loadSies->created_at;
                    $data['updated_at'] = date('Y-m-d H:i:s');
                    DB::table($tableName)
                    ->updateOrInsert(
                      [
                        'imuno_sies_id' => $data['imuno_sies_id'],
                        'estabelecimento_sies_id' => $data['estabelecimento_sies_id'],
                        'data' => $data['data'],
                        'nfm' => $data['nfm'],
                        'mov' => $data['mov'],
                        'lote' => $data['lote'] 
                      ],
                      $data
                    );
                  }

                } catch (\Throwable $th) {
                  echo($th);
                  DB::rollback();
                }


              }

            }
          }
        }
      }
      
    }
    if ($mensager) {
      echo('<div class="p-3 mb-2 bg-warning text-dark">Período de: '.$menorData->format('d-m-Y').' a '.$maiorData->format('d-m-Y').'</div>');
    }
    
    DB::commit();
    
    return false;
  }

  public static function addColumn($itens, $newKey, $function)
  {
    $itens = json_decode($itens);
    $newData = array();
    foreach ($itens->data as $item) {
      $item->$newKey = $function($item);
      $newData[] = $item;
    }

    $itens->data = $newData;

    return json_encode($itens);
  }

  public static function createTable($tableName){

      try {

        Schema::create($tableName, function (Blueprint $table) {
          $table->increments('id');
          $table->date('data')->nullable();
          $table->string('nfm')->nullable();
          $table->integer('mov')->nullable();
          $table->string('lote')->nullable();
          $table->date('validade')->nullable();
          $table->integer('quantidade')->nullable();
          $table->double('valor_total')->nullable();
          $table->unsignedInteger('load_sies_id')->nullable();
          $table->foreign('load_sies_id')->references('id')->on('load_sies');
          $table->unsignedInteger('imuno_sies_id')->nullable();
          $table->foreign('imuno_sies_id')->references('id')->on('imuno_sies');
          $table->unsignedInteger('estabelecimento_sies_id')->nullable();
          $table->foreign('estabelecimento_sies_id')->references('id')->on('estabelecimento_sies');
          $table->timestamps();
          
        });

      } catch (Exception $e) {

        echo "Desculpe, não consegui criar a tabela: "
          . $tableName
          . $e->getMessage();
        return false;

      }
    
    return true;

  }
}
