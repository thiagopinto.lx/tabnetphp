<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Schema\Blueprint;


class Obito extends Agravo
{
    //cria tabela dinamicamente
    public static function createTable($tableName, $upload)
    {

        $db = dbase_open(storage_path('app/'.$upload), 0);

        if($db){

            $columns = dbase_get_header_info($db);

            try {
                Schema::create($tableName, function (Blueprint $table) use ($columns){

                    $laravel_type['memo']      = 'text';
                    $laravel_type['character'] = 'string';
                    $laravel_type['number']    = 'integer';
                    $laravel_type['float']     = 'float';
                    $laravel_type['date']      = 'date';

                    $table->increments('id');

                    foreach ($columns as $column) {

                        $prefix = str_split($column['name'], 3);
                        $prefixData = str_split($column['name'], 2);
                        if($column['name'] == "TIPOBITO"){
                            $table->integer($column['name'])->nullable();
                        }
                        elseif($column['name'] == "CODINST"){
                            $table->string($column['name'])->nullable();
                        }
                        elseif($column['name'] == "CODIFICADO"){
                            $table->string($column['name'])->nullable();
                        }
                        elseif($column['name'] == "NUMRES"){
                            $table->string($column['name'])->nullable();
                        }
                        elseif($column['name'] == "NUMENDOCOR"){
                            $table->string($column['name'])->nullable();
                        }
                        elseif($column['name'] == "NUMSUS"){
                            $table->bigInteger($column['name'])->nullable();
                        }
                        elseif($column['name'] == "CODENDRES"){
                            $table->bigInteger($column['name'])->nullable();
                        }
                        elseif ($prefixData[0] == 'DT') {
                            $table->date($column['name'])->nullable();
                        }
                        elseif ($prefix[0] == 'COD' || $prefix[0] == "NUM") {
                            $table->integer($column['name'])->nullable();
                        }
                        elseif ($laravel_type[$column['type']] == 'string') {
                            $table->string($column['name'])->nullable();
                        }
                        elseif ($laravel_type[$column['type']] == 'integer') {
                            $table->integer($column['name'])->nullable();
                        }
                        elseif ($laravel_type[$column['type']] == 'text') {
                            $table->text($column['name'])->nullable();
                        }
                        elseif ($laravel_type[$column['type']] == 'float') {
                            $table->float($column['name'])->nullable();
                        }
                        elseif ($laravel_type[$column['type']] == 'date') {
                            $table->date($column['name'])->nullable();
                        }
                    }
                    $table->integer('NU_ANO')->nullable();
                    $table->double('ID_GEO1')->nullable();
                    $table->double('ID_GEO2')->nullable();
                    $table->timestamps();
                });
                dbase_close($db);
            } catch (Exception $e) {
                dbase_close($db);
                Storage::delete($upload);
                echo "Não foi possivel criar a tabela 03"
                    .$tableName
                    .$e->getMessage();
                return false;
            }

        }
        return true;
    }

    //carga inicial de dados feita via dbf
    public static function dbfLoadObito($tableName, $upload, $ano, $idMunicipio, $isUpdate)
    {
      $db = dbase_open(storage_path('app/'.$upload), 0);

        if($db){
            $num_rows    = dbase_numrecords($db);

            try {
                DB::beginTransaction();
                # Loop the Dbase records
                for($index=1; $index <= $num_rows; $index ++)
                {
                    # Get one record
                    $record = dbase_get_record_with_names($db, $index);
                    # Ignore deleted fields
                    if (
                        $record["deleted"] != "1" &&
                        $record["CODMUNRES"] == $idMunicipio
                    )
                    {
                        # Insert the record
                        foreach ($record as $key=>$field)
                        {
                            if ($key!=='deleted')
                            {
                                $field = str_replace("'", "\'", $field);
                                $field = str_replace("", "SN", $field);
                                $field = trim($field);
                                $field =  utf8_encode($field);
                                if ($key == "DTOBITO" && substr($field, -4) != $ano) {
                                    DB::rollback();
                                    dbase_close($db);
                                    if (!$isUpdate) {
                                        Schema::drop($tableName);
                                    }
                                    Storage::delete($upload);
                                    return false;
                                }
                                $key = str_replace("'", "\'", $key);
                                $key = trim($key);
                                $key =  utf8_encode($key);
                                if($isUpdate){
                                    if($field != ""){
                                        $tipoCol = str_split($key, 2);
                                        if ($tipoCol[0] == "DT") {
                                            $arraDate = str_split($field, 2);
                                            $field = $arraDate[2].$arraDate[3].
                                                     '-'.$arraDate[1].'-'.$arraDate[0];

                                            if ($key == "DTOBITO") {
                                                $data["NU_ANO"] = $arraDate[2].$arraDate[3];
                                            }
                                            $data[$key] = $field;
                                            unset($arraDate);
                                        } else {
                                            $data[$key] = $field;
                                        }
                                    }else{
                                        if($field == "" ){
                                            $data[$key] = null;
                                        }
                                    }

                                }else{
                                    if($field != ""){
                                        $tipoCol = str_split($key, 2);
                                        if ($tipoCol[0] == "DT") {
                                            $arraDate = str_split($field, 2);
                                            $field = $arraDate[2].$arraDate[3].
                                                     '-'.$arraDate[1].'-'.$arraDate[0];
                                            if ($key == "DTOBITO") {
                                                $data["NU_ANO"] = $arraDate[2].$arraDate[3];
                                            }
                                            $data[$key] = $field;
                                            unset($arraDate);
                                        } else {
                                            $data[$key] = $field;
                                        }
                                    }else{
                                        if($field == ""){
                                            $data[$key] = null;
                                        }
                                    }
                                }
                            }
                        }
                        if($isUpdate){
                            $data['updated_at'] = date('Y-m-d H:i:s');
                            DB::table($tableName)
                                ->updateOrInsert(
                                    ['NUMERODO' => $data['NUMERODO']],
                                    $data
                                );                          
                        }else{
                            $data['created_at'] = date('Y-m-d H:i:s');
                            $data['updated_at'] = date('Y-m-d H:i:s');
                            DB::table($tableName)->insert($data);
                        }
                    }
                }
                dbase_close($db);
                DB::commit();
            } catch (\Throwable $e) {
                echo "Não foi possível carregar os dados "
                .$tableName
                .$e->getMessage();
                dbase_close($db);
                return false;
            }

        }
      return true;
    }

}
