<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Schema\Blueprint;
use App\Models\EstabelecimentoCnes;
use App\Models\ImunoSipni;
use App\Models\MovimentoSies;
use DOMDocument;
use DateTime;


class LoadSipni extends Model
{
  public function imuno()
  { 
    $imuno = ImunoSipni::where('id', $this->imuno_sipnis_id)->get();
    return $imuno[0];
  }

  protected $guarded = [];
  
  public static function toDataTables($request, $tabela, $where = NULL)
  {

    try {
      $columnsName = array();
      $columnsSearchable = array();

      $foreignKeys = null;
      foreach ($request->columns as $column) {

        if ($column['orderable'] != 'false') {
          if (strpos($column['name'], '_id') !== false) {
            
            $foreignTable[0] = substr($column['name'], 0, strpos($column['name'], "_id"));
            $foreignTable[1] = substr($column['name'], strpos($column['name'], "_id")+1, 2);
            
            $foreignTable[] = $column['name'];
            $foreignKeys[] = $foreignTable;
            $columnsName[] = $foreignTable[0] .
              '.nome';
          } else {
            $columnsName[] = $tabela . '.' . $column['name'];
          }
        }

        if ($column['searchable'] != 'false') {
          $columnsSearchable[] = $tabela . '.' . $column['name'];
        }
      }
      $columnsNameCont = count($columnsName);
      $columnsSearchableCont = count($columnsSearchable);

      if ($where == NULL) {
        $totalData = DB::table($tabela)->count();
      } else {
        $totalData = DB::table($tabela)->where("agravo", $where)->count();
      }

      $totalFiltered = $totalData;
      $limit = $request->input('length');
      $start = $request->input('start');
      $order = $columnsName[$request->input('order.0.column')];
      $dir = $request->input('order.0.dir');

      if (empty($request->input('search.value'))) {
        if ($where == NULL) {
          $query = DB::table($tabela)
            ->select($columnsName)
            ->offset($start)
            ->limit($limit)
            ->orderBy($order, $dir);
        } else {
          $query = DB::table($tabela)
            ->select($columnsName)
            ->where("agravo", $where)
            ->offset($start)
            ->limit($limit)
            ->orderBy($order, $dir);
        }

        if ($foreignKeys !== null) {
          foreach ($foreignKeys as $foreignKey) {
            $query->join(
              $foreignKey[0],
              $foreignKey[0] . '.' . $foreignKey[1],
              '=',
              $tabela . '.' . $foreignKey[2]
            );
          }
        }

        $registros = $query->get();
      } else {

        $search = $request->input('search.value');
        $query = DB::table($tabela)
          ->select($columnsName)
          ->where(function ($q) use ($columnsSearchable, $search) {
            foreach ($columnsSearchable as $columnSearchable) {
              $q->orWhere($columnSearchable, 'ILIKE', "%$search%");
              if (isset($where)) {
                $q->where("agravo", $where);
              }
            }
          })
          ->offset($start)
          ->limit($limit)
          ->orderBy($order, $dir);
        $registros = $query->get();

        $totalFiltered = DB::table($tabela)
          ->where(function ($q) use ($columnsSearchable, $search) {
            foreach ($columnsSearchable as $columnSearchable) {
              $q->orWhere($columnSearchable, 'ILIKE', "%$search%");
              if (isset($where)) {
                $q->where("agravo", $where);
              }
            }
          })
          ->count();
      }

      $data = array();
      if (!empty($registros)) {
        foreach ($registros as $registro) {
          if ($foreignKeys !== null) {
            $nestedData = (array) $registro;
            foreach ($foreignKeys as $foreignKey) {
              $nestedData[$foreignKey[2]] = $nestedData['nome'];
              unset($nestedData['nome']);
            }
          } else {
            $nestedData = (array) $registro;
          }

          $data[] = $nestedData;
        }

        $datas = array(
          "draw"            => intval($request->input('draw')),
          "recordsTotal"    => intval($totalData),
          "recordsFiltered" => intval($totalFiltered),
          "data"            => $data
        );
      } else {
        $datas = null;
      }
    } catch (\Throwable $th) {
      dd($th);
      $datas = null;
    }

    return json_encode($datas);
  }


  //carregar dados
  public static function csvLoad($upload, $request)
  {
    $delimitador = ',';
    $cerca = '"';

    $file = file_get_contents(storage_path('app/'.$upload));
    $initPro = new \DateTime('now');
    $resposta = "Inicio:".$initPro->format('H:i:s.u');
      if($file){

        $header = NULL;
        $datas = array();
        $numRow = 0;
        //$rows = explode("\n", $file);
        $rows = array_filter(explode("\n", $file));
        $formato = 'd/m/Y';
        $newFormat = 'Y-m-d';
        $currentAno = null;
        $currentImuno = null;
        $currentEstabelecimento = null;
        $currentLoadSipni = null;
        $currentCnes = null;
        $currentEstabelecimento = null;
        $valuesSqlUpSert = "";
        $countRows = count($rows);

        //dd($countRows);

        for ($i=0; $i < $countRows; $i++) {
            $item = str_getcsv($rows[$i], $delimitador, $cerca, "\n");
            if(!$header){
              $itensClean = array();

              $countItem = count($item);
              for ($j=0; $j < $countItem; $j++) { 
                $item[$j] = LoadSipni::tirarAcentos($item[$j]);
                $item[$j] = str_replace(" ", "_", str_replace(".", "", strtolower($item[$j])));
                $header[] = $item[$j];
              }
              continue;
            }


            if(count($header) == count($item)){

              $tempRow = null;
              foreach ($item as $value) {
                $tempRow[] = mb_convert_encoding($value, 'UTF-8', 'ISO-8859-1');
              }
                $data = array_combine($header, $tempRow);
                $data['data_de_nascimento'] = DateTime::createFromFormat($formato, $data['data_de_nascimento']);
                $data['data_de_aplicacao'] = DateTime::createFromFormat($formato, $data['data_de_aplicacao']);
                $nomeImuno = strtolower(str_replace(" ", "_", $data['produto']));
                $ano = (int)$data['data_de_aplicacao']->format('Y');

                if(strcasecmp($nomeImuno, $currentImuno) != 0 && $ano != $currentAno){

                  $tableName = "da_sipni_".$nomeImuno."_".$ano;

                  $imunoSipni = ImunoSipni::updateOrCreate(
                    ['nome' => $data['produto']]
                  );

                  $loadSipni = LoadSipni::updateOrCreate(
                    [
                      'ano' => $data['data_de_aplicacao']->format('Y'),
                      'imuno_sipnis_id' => $imunoSipni->id
                    ]
                  );

                  if(!Schema::hasTable($tableName)){
                    LoadSipni::createTable($tableName);
                  }

                  if(isset($request->color)){
                    $loadSipni->color = $request->color;
                  }
              
                  $loadSipni->save();
                  $currentLoadSipni = $loadSipni; 
                  $currentAno = $ano;
                  $currentImuno = $imunoSipni;
                  $currentTableName = $tableName;
                  
                }


                if ($currentCnes != $data['cnes']) {
                  $estabelecimento = EstabelecimentoCnes::where('co_cnes', $data['cnes'])->first();
                  if ($estabelecimento == null) {
                    $estabelecimento = new EstabelecimentoCnes;
                    $estabelecimento->co_cnes = $data['cnes'];
                    $estabelecimento->no_fantasia = $data['unidade_de_saude'];
                    $estabelecimento->save();
                  }
                  $currentCnes = $data['cnes'];
                  $currentEstabelecimento = $estabelecimento;
                }

                $data['data_de_nascimento'] = $data['data_de_nascimento']->format($newFormat);
                $data['data_de_aplicacao'] = $data['data_de_aplicacao']->format($newFormat);;
                $data['estabelecimento_cnes_id'] = $currentEstabelecimento->id;
                $data['load_sipnis_id'] = $currentLoadSipni->id;
                $data['imuno_sipnis_id'] = $currentImuno->id;
                unset($data['produto']);
                unset($data['cnes']);
                unset($data['unidade_de_saude']);
                
                $data['vacinado'] = str_replace("'", "", $data['vacinado']);
                $data['bairro'] = str_replace("'", "", $data['bairro']);
                $data['municipio'] = str_replace("'", "", $data['municipio']);

                $value= 
                  "(
                    {$i},
                    {$data['load_sipnis_id']},
                    '{$data['vacinado']}',
                    {$data['imuno_sipnis_id']},
                    '{$data['dose']}',
                    '{$data['laboratorio']}',
                    '{$data['lote']}',
                    '{$data['estrategia']}',
                    '{$data['motivo_indicacao']}',
                    '{$data['especialidade']}',
                    '{$data['grupo_de_atendimento']}',
                    '{$data['situacao']}',
                    '{$data['ra']}',
                    '{$data['idade']}',
                    '{$data['data_de_nascimento']}',
                    '{$data['data_de_aplicacao']}',
                    '{$data['bairro']}',
                    '{$data['municipio']}',
                    '{$data['pais']}',
                    {$data['estabelecimento_cnes_id']},
                    '{$data['inadvertida']}'
                  )";
                        

                if ($i < ($countRows-1)) {
                  $valuesSqlUpSert .= "{$value}, ";
                }else{
                  $valuesSqlUpSert .= "{$value}";
                }



                /*
                DB::table($currentTableName)
                ->updateOrInsert(
                  [
                    'vacinado' => $data['vacinado'],
                    'imuno_sipnis_id' => $data['imuno_sipnis_id'],
                    'dose' => $data['dose'],
                    'estrategia' => $data['estrategia'],
                    'data_de_nascimento' => $data['data_de_nascimento'],
                    'data_de_aplicacao' => $data['data_de_aplicacao'],
                    'estabelecimento_cnes_id' => $data['estabelecimento_cnes_id'] 
                  ],
                  $data);
                */


                //$datas[] = $data;

            }else{
              //var_dump($row);
            }
            

            
        }

        
        $columnSQL = "(
          line,
          load_sipnis_id,
          vacinado,
          imuno_sipnis_id,
          dose,
          laboratorio,
          lote,
          estrategia,
          motivo_indicacao,
          especialidade,
          grupo_de_atendimento,
          situacao,
          ra,
          idade,
          data_de_nascimento,
          data_de_aplicacao,
          bairro,
          municipio,
          pais,
          estabelecimento_cnes_id,
          inadvertida
        )";

        $setColumnSQL = "
          line = excluded.line,
          load_sipnis_id = excluded.load_sipnis_id,
          vacinado = excluded.vacinado,
          imuno_sipnis_id = excluded.imuno_sipnis_id,
          dose = excluded.dose,
          laboratorio = excluded.laboratorio,
          lote = excluded.lote,
          estrategia = excluded.estrategia,
          motivo_indicacao = excluded.motivo_indicacao,
          especialidade = excluded.especialidade,
          grupo_de_atendimento = excluded.grupo_de_atendimento,
          situacao = excluded.situacao,
          ra = excluded.ra,
          idade = excluded.idade,
          data_de_nascimento = excluded.data_de_nascimento,
          data_de_aplicacao = excluded.data_de_aplicacao,
          bairro = excluded.bairro,
          municipio = excluded.municipio,
          pais = excluded.pais,
          estabelecimento_cnes_id = excluded.estabelecimento_cnes_id,
          inadvertida = excluded.inadvertida
        ;";


        $SqlUpSert = "
          INSERT INTO {$currentTableName} {$columnSQL}
          VALUES {$valuesSqlUpSert}
          ON CONFLICT ON CONSTRAINT {$currentTableName}_unique_register DO UPDATE 
          SET {$setColumnSQL}";
        
        try {
          DB::statement($SqlUpSert);
        } catch (\Throwable $th) {
          echo($th);
          DB::rollback();
        }

        

        DB::commit();
        //dd($SqlUpSert);
        
        $finshPro = new \DateTime('now');
        $resposta .= "\n Fim:".$finshPro->format('H:i:s.u');
           //Calcula a diferença entre as datas
        $diff   =   $initPro->diff($finshPro, true);   
        //dd($diff);

        echo("<div class='p-3 mb-2 bg-warning text-dark'>Ano: {$ano} Imuno: {$imunoSipni->nome}</div>");
        
        DB::commit();
      }
  return true;
  }

  public static function addColumn($itens, $newKey, $function)
  {
    $itens = json_decode($itens);
    $newData = array();
    foreach ($itens->data as $item) {
      $item->$newKey = $function($item);
      $newData[] = $item;
    }

    $itens->data = $newData;

    return json_encode($itens);
  }

  public static function createTable($tableName){

      try {

        Schema::create($tableName, function (Blueprint $table) use($tableName)   {
          $table->increments('id');
          $table->unsignedInteger('line');          
          $table->unsignedInteger('load_sipnis_id')->nullable(); 
          $table->string('vacinado')->nullable();
          $table->unsignedInteger('imuno_sipnis_id')->nullable();       
          $table->string('dose')->nullable();
          $table->string('laboratorio')->nullable();
          $table->string('lote')->nullable();
          $table->string('estrategia')->nullable();
          $table->string('motivo_indicacao')->nullable();
          $table->string('especialidade')->nullable();
          $table->string('grupo_de_atendimento')->nullable();
          $table->string('situacao')->nullable();
          $table->string('ra')->nullable();
          $table->string('idade')->nullable();
          $table->date('data_de_nascimento')->nullable();
          $table->date('data_de_aplicacao')->nullable();
          $table->string('bairro')->nullable();
          $table->string('municipio')->nullable();
          $table->string('pais')->nullable();
          $table->unsignedInteger('estabelecimento_cnes_id')->nullable();     
          $table->string('inadvertida')->nullable();
          $table->timestamps();
          $table->foreign('load_sipnis_id')->references('id')->on('load_sipnis');
          $table->foreign('imuno_sipnis_id')->references('id')->on('imuno_sipnis');
          $table->foreign('estabelecimento_cnes_id')->references('id')->on('estabelecimento_cnes');
          $table->unique(['line', 'vacinado', 'imuno_sipnis_id', 'dose', 'laboratorio', 'lote', 'estrategia', 'data_de_nascimento', 'data_de_aplicacao', 'bairro', 'municipio', 'estabelecimento_cnes_id'], $tableName.'_unique_register');
          //$table->index(['vacinado', 'imuno_sipnis_id', 'dose', 'estrategia', 'data_de_nascimento', 'data_de_aplicacao', 'estabelecimento_cnes_id']);

        });

      } catch (Exception $e) {

        echo "Desculpe, não consegui criar a tabela: "
          . $tableName
          . $e->getMessage();
        return false;

      }
    
    return true;

  }

  public static function tirarAcentos($string){
    $from = "áàãâéêíóôõúüçÁÀÃÂÉÊÍÓÔÕÚÜÇ";
    $to = "aaaaeeiooouucAAAAEEIOOOUUC";

    return strtr($string, utf8_decode($from), $to);
  }
}
