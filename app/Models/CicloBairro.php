<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Ciclo;

class CicloBairro extends Model
{
  public function ciclo()
  {
      return $this->belongsTo('App\Models\Ciclo');
  }

  public function bairros()
  {
      return $this->hasMany('App\Models\Bairro');
  }

}
