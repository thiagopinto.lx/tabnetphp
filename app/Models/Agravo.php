<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Schema\Blueprint;
use App\Utilities\Helper;

class Agravo extends Model
{
  //cria tabela dinamicamente
  public static function createTable($tableName, $upload)
  {

    $db = dbase_open(storage_path('app/' . $upload), 0);

    if ($db) {

      $columns = dbase_get_header_info($db);

      $record = dbase_get_record_with_names($db, 1);
      # Ignore deleted fields
      if (!array_key_exists("NM_PACIENT", $record)) {
        dbase_close($db);
        echo "Desculpe, mas preciso dos dados de identificação do paciente.";
        return false;
      }

      try {
        Schema::create($tableName, function (Blueprint $table) use ($columns) {

          $laravel_type['memo']      = 'text';
          $laravel_type['character'] = 'string';
          $laravel_type['number']    = 'integer';
          $laravel_type['float']     = 'float';
          $laravel_type['date']      = 'date';

          $table->increments('id');

          foreach ($columns as $column) {

            $prefix = str_split($column['name'], 2);
            if ($column['name'] == "ID_CNS_SUS") {
              $table->bigInteger($column['name'])->nullable();
            } elseif ($column['name'] == "ID_GEO1" || $column['name'] == "ID_GEO2") {
              $table->double($column['name'])->nullable();
            } elseif ($column['name'] == "NU_NUMERO") {
              $table->string($column['name'])->nullable();
            } elseif ($column['name'] == "NU_TELEFON") {
              $table->string($column['name'])->nullable();
            } elseif ($column['name'] == "ID_OCUPA_N") {
              $table->string($column['name'])->nullable();
            } elseif ($prefix[0] == 'NU' || $prefix[0] == "ID" && $column['name'] != "ID_AGRAVO" && $column['name'] != "IDENT_MICR") {
              $table->integer($column['name'])->nullable();
            } elseif ($laravel_type[$column['type']] == 'string') {
              $table->string($column['name'])->nullable();
            } elseif ($laravel_type[$column['type']] == 'integer') {
              $table->integer($column['name'])->nullable();
            } elseif ($laravel_type[$column['type']] == 'text') {
              $table->text($column['name'])->nullable();
            } elseif ($laravel_type[$column['type']] == 'float') {
              $table->float($column['name'])->nullable();
            } elseif ($laravel_type[$column['type']] == 'date') {
              $table->date($column['name'])->nullable();
            }
          }

          $table->timestamps();
        });
        dbase_close($db);
      } catch (Exception $e) {
        dbase_close($db);
        Storage::delete($upload);
        echo "Não foi possivel criar a tabela 03"
          . $tableName
          . $e->getMessage();
        return false;
      }
    }
    return true;
  }

  //carga inicial de dados feita via dbf
  public static function dbfLoad($tableName, $upload, $ano, $cid, $idMunicipio, $isUpdate)
  {
    $db = dbase_open(storage_path('app/' . $upload), 0);

    if ($db) {
      $num_rows    = dbase_numrecords($db);

      try {
        DB::beginTransaction();
        # Loop the Dbase records
        for ($index = 1; $index <= $num_rows; $index++) {
          # Get one record
          $record = dbase_get_record_with_names($db, $index);
          # Ignore deleted fields
          if (!array_key_exists("NM_PACIENT", $record)) {
            DB::rollback();
            dbase_close($db);
            if (!$isUpdate) {
              Schema::drop($tableName);
            }
            echo "Desculpe, mas preciso dos dados de identificação do paciente";
            return false;
          }
          if (
            $record["deleted"] != "1" &&
            trim($record["ID_AGRAVO"]) == $cid &&
            trim($record["ID_MN_RESI"]) == $idMunicipio
          ) {
            //var_dump($record);
            # Insert the record
            foreach ($record as $key => $field) {
              if ($key !== 'deleted') {
                $field = str_replace("'", "\'", $field);
                $field = str_replace(null, "SN", $field);
                $field = trim($field);
                $field =  utf8_encode($field);
                if ($key == "NU_ANO" && $field != $ano) {
                  DB::rollback();
                  dbase_close($db);
                  if (!$isUpdate) {
                    Schema::drop($tableName);
                  }
                  Storage::delete($upload);
                  return false;
                }
                $key = str_replace("'", "\'", $key);
                $key = trim($key);
                $key =  utf8_encode($key);
                if ($isUpdate) {
                  if ($field != "" && $key != "ID_GEO1" && $key != "ID_GEO2") {
                    $data[$key] = $field;
                  } else {
                    if ($field == "" && $key != "ID_GEO1" && $key != "ID_GEO2") {
                      $data[$key] = null;
                    }
                  }
                } else {
                  if ($field != "") {
                    $data[$key] = $field;
                  } else {
                    if ($field == "" && $key != "ID_GEO1" && $key != "ID_GEO2") {
                      $data[$key] = null;
                    }
                  }
                }
              }
            }
            if ($isUpdate) {
              $data['updated_at'] = date('Y-m-d H:i:s');
              DB::table($tableName)
                ->updateOrInsert(
                  ['NU_NOTIFIC' => $data['NU_NOTIFIC']],
                  $data
                );
            } else {
              $data['created_at'] = date('Y-m-d H:i:s');
              $data['updated_at'] = date('Y-m-d H:i:s');
              DB::table($tableName)->insert($data);
            }
          } else {
            //echo 'não passou validacao';
          }
        }
        dbase_close($db);
        DB::commit();
      } catch (\Throwable $e) {
        echo "Não foi possível carregar os dados "
          . $tableName
          . $e->getMessage();
        dbase_close($db);
        return false;
      }
    }
    return true;
  }

  public static function toDataTables($request, $tabela, $where = null)
  {
    $columnsName = array();
    $columnsSearchable = array();

    $foreignKeys = null;
    foreach ($request->columns as $column) {

      if ($column['orderable'] != 'false') {
        if (strpos($column['name'], '_id') !== false) {
          $foreignTable  = explode("_", $column['name']);
          $foreignTable[0] = $foreignTable[0] . 's';
          $foreignTable[] = $column['name'];
          $foreignKeys[] = $foreignTable;
          $columnsName[] = $foreignTable[0] .
            '.name';
        } else {
          $columnsName[] = $tabela . '.' . $column['name'];
        }
      }

      if ($column['searchable'] != 'false') {
        $columnsSearchable[] = $tabela . '.' . $column['name'];
      }
    }
    $columnsNameCont = count($columnsName);
    $columnsSearchableCont = count($columnsSearchable);

    if ($where == NULL) {
      $totalData = DB::table($tabela)->count();
    } else {
      $totalData = DB::table($tabela)->where("table_name", $where)->count();
    }

    $totalFiltered = $totalData;
    $limit = $request->input('length');
    $start = $request->input('start');
    $order = $columnsName[$request->input('order.0.column')];
    $dir = $request->input('order.0.dir');

    if (empty($request->input('search.value'))) {
      if ($where == NULL) {
        $query = DB::table($tabela)
          ->select($columnsName)
          ->offset($start)
          ->limit($limit)
          ->orderBy($order, $dir);
      } else {
        $query = DB::table($tabela)
          ->select($columnsName)
          ->where("table_name", $where)
          ->offset($start)
          ->limit($limit)
          ->orderBy($order, $dir);
      }

      if ($foreignKeys !== null) {
        foreach ($foreignKeys as $foreignKey) {
          $query->join(
            $foreignKey[0],
            $foreignKey[0] . '.' . $foreignKey[1],
            '=',
            $tabela . '.' . $foreignKey[2]
          );
        }
      }

      $registros = $query->get();
    } else {

      $search = $request->input('search.value');
      $query = DB::table($tabela)
        ->select($columnsName)
        ->where(function ($q) use ($columnsSearchable, $search) {
          foreach ($columnsSearchable as $columnSearchable) {
            $q->orWhere($columnSearchable, 'ILIKE', "%$search%");
            if (isset($where)) {
              $q->where("table_name", $where);
            }
          }
        })
        ->offset($start)
        ->limit($limit)
        ->orderBy($order, $dir);
      $registros = $query->get();

      $totalFiltered = DB::table($tabela)
        ->where(function ($q) use ($columnsSearchable, $search) {
          foreach ($columnsSearchable as $columnSearchable) {
            $q->orWhere($columnSearchable, 'ILIKE', "%$search%");
            if (isset($where)) {
              $q->where("table_name", $where);
            }
          }
        })
        ->count();
    }

    $data = array();
    if (!empty($registros)) {
      foreach ($registros as $registro) {
        if ($foreignKeys !== null) {
          $nestedData = (array) $registro;
          foreach ($foreignKeys as $foreignKey) {
            $nestedData[$foreignKey[2]] = $nestedData['name'];
            unset($nestedData['name']);
          }
        } else {
          $nestedData = (array) $registro;
        }

        $data[] = $nestedData;
      }

      $datas = array(
        "draw"            => intval($request->input('draw')),
        "recordsTotal"    => intval($totalData),
        "recordsFiltered" => intval($totalFiltered),
        "data"            => $data
      );
    } else {
      $datas = null;
    }

    return json_encode($datas);
  }

  public static function addColumn($itens, $newKey, $function)
  {
    $itens = json_decode($itens);
    $newData = array();
    foreach ($itens->data as $item) {
      $item->$newKey = $function($item);
      $newData[] = $item;
    }

    $itens->data = $newData;

    return json_encode($itens);
  }

  public static function getPorcentagemGeocode($tableName)
  {
    $totalData = DB::table($tableName)->count();
    $totalDataGeocode = DB::table($tableName)->where(function ($q) {
      $q->whereNotNull('ID_GEO1');
      $q->whereNotNull('ID_GEO2');
    })
      ->count();
    if ($totalData > 0 && $totalDataGeocode > 0) {
      $porcentagem = round($totalDataGeocode / ($totalData / 100), 2);
      return str_replace('.', ',', $porcentagem);
    } else {
      return 0;
    }
  }

  public static function createDataSetBar($mydata)
  {
    $data = new \stdClass;

    foreach ($mydata->labels as $label) {
      $data->labels[] = $label;
      $mydata->soma[] = 0;
    }
    $data->datasets = array();
    $count = count($mydata->periodos);

    for ($i = 0; $i < $count; $i++) {
      $data->datasets[$i] = new \stdClass;
      $data->datasets[$i]->label[] = $mydata->periodos[$i];

      $loadData = LoadData::where([
        ['table_name', '=', $mydata->table_name],
        ['ano', '=', $mydata->periodos[$i]],
      ])
        ->first();

      for ($j = 0; $j < count($mydata->querys); $j++) {
        $data->datasets[$i]->data[$j] = $mydata->querys[$j]($mydata->table_name . $mydata->periodos[$i]);
        $mydata->soma[$j] = $mydata->soma[$j] + $data->datasets[$i]->data[$j];
        $data->datasets[$i]->backgroundColor[$j] = Helper::hex2rgba($loadData->color, 0.5);
        $data->datasets[$i]->borderColor[$j] = $loadData->color;
      }
      $data->datasets[$i]->borderWidth = 1;
    }
    $data->datasets[$count] = new \stdClass;
    $data->datasets[$count]->label[] = 'Média';
    for ($i = 0; $i < count($mydata->soma); $i++) {
      $data->datasets[$count]->data[$i] = $mydata->soma[$i] / $count;
      $data->datasets[$count]->backgroundColor[$i] = Helper::hex2rgba('#FF0000', 0.5);
      $data->datasets[$count]->borderColor[$i] = '#FF0000';
    }
    $data->datasets[$count]->borderWidth = 1;

    return $data;
  }

  public static function createDataSetLine($mydata)
  {
    $data = new \stdClass;

    foreach ($mydata->labels as $label) {
      $data->labels[] = $label;
      $mydata->soma[] = 0;
    }
    $data->datasets = array();
    $count = count($mydata->periodos);
    for ($i = 0; $i < $count; $i++) {
      $data->datasets[$i] = new \stdClass;
      $data->datasets[$i]->label[] = $mydata->periodos[$i];
      for ($j = 0; $j < count($mydata->querys); $j++) {
        $resultQuery = $mydata->querys[$j]($mydata->table_name . $mydata->periodos[$i]);
        $meses = array();
        foreach ($resultQuery as $item) {
          $data->datasets[$i]->data[] = $item->count;
        }
      }
      $arrayMediaItens[] = $data->datasets[$i]->data;
      $loadData = LoadData::where([
        ['table_name', '=', $mydata->table_name],
        ['ano', '=', $mydata->periodos[$i]]
      ])
        ->first();
      $data->datasets[$i]->backgroundColor[] = Helper::hex2rgba($loadData->color, 0.5);
      $data->datasets[$i]->borderColor[] = Helper::hex2rgba($loadData->color, 0.5);
      $data->datasets[$i]->fill = 'false';
      if (isset($mydata->optionLine)) {
        $data->datasets[$i]->steppedLine = $mydata->optionLine->steppedLine;
        if (isset($mydata->optionLine->pointRadius)) {
          $data->datasets[$i]->pointRadius = $mydata->optionLine->pointRadius;
          $data->datasets[$i]->pointHoverRadius = $mydata->optionLine->pointHoverRadius;
          $data->datasets[$i]->pointStyle = $mydata->optionLine->pointStyles[$i];
          $data->datasets[$i]->pointBackgroundColor = $loadData->color;
          $data->datasets[$i]->pointBorderColor = $loadData->color;
        }
      }
    }
    $data->datasets[$count] = new \stdClass;
    $data->datasets[$count]->label[] = 'Média';

    for ($i = 0; $i < $mydata->quantMedia; $i++) {
      $soma = 0;
      $countMedia = 0;
      for ($j = 0; $j < $count; $j++) {
        if (isset($arrayMediaItens[$j][$i])) {
          $soma = $arrayMediaItens[$j][$i] + $soma;
          $countMedia++;
        }
      }
      if ($countMedia == 0) {
        $data->datasets[$count]->data[] = null;
      } else {
        $data->datasets[$count]->data[] = $soma / $countMedia;
      }
    }
    $data->datasets[$count]->backgroundColor[] = Helper::hex2rgba('#FF0000', 0.5);
    $data->datasets[$count]->borderColor[] = '#FF0000';
    $data->datasets[$count]->fill = 'false';
    if (isset($mydata->optionLine)) {
      $data->datasets[$count]->steppedLine = $mydata->optionLine->steppedLine;
      if (isset($mydata->optionLine->pointRadius)) {
        $data->datasets[$count]->pointRadius = $mydata->optionLine->pointRadius;
        $data->datasets[$count]->pointHoverRadius = $mydata->optionLine->pointHoverRadius;
        $data->datasets[$count]->pointBackgroundColor = '#FF0000';
        $data->datasets[$count]->pointBorderColor = '#FF0000';
      }
    }

    return $data;
  }

  public static function createDataDetalhamento($mydata)
  {

    $data = array();
    $notif[] = "Notificações";
    foreach ($mydata->periodos as $periodo) {
      $notif[] = DB::table($mydata->table_name . $periodo)->count();
    }
    $data[] = $notif;

    for ($i = 1; $i <= count($mydata->criterios); $i++) {
      $data[$i][0] = $mydata->criterios[$i - 1][0];
      for ($j = 1; $j <= count($mydata->periodos); $j++) {
        $data[$i][$j] = $mydata->criterios[$i - 1][1]($mydata->table_name . $mydata->periodos[$j - 1]);
      }
    }

    return $data;
  }

  public static function getBySexo($table_name, $anos)
  {
    $mydata = new \stdClass;
    $mydata->table_name = $table_name;

    $mydata->periodos = $anos;

    $mydata->criterios[] = ["Masculino", function ($tabela) {
      return DB::table($tabela)->where('CS_SEXO', 'M')->count();
    }];
    $mydata->criterios[] = ["Femenino", function ($tabela) {
      return DB::table($tabela)->where('CS_SEXO', 'F')->count();
    }];
    $mydata->criterios[] = ["Indefinido", function ($tabela) {
      return DB::table($tabela)->where('CS_SEXO', 'I')->count();
    }];

    return Agravo::createDataDetalhamento($mydata);
  }
}
