@extends('layouts.app')
@section('css')
<link href="{{ mix('/css/Chart.min.css') }}" rel="stylesheet" type="text/css">
@endsection
@section('content')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <div class="d-sm-flex align-items-right right-content-between mb-4">
            <a href="{{$urls['detalhamentos']}}" style="margin-right: 10px;" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
                <i class="fas fa-info-circle fa-sm text-white-50"></i> Detalhamento
            </a>
            <a href="{{$urls['clustersmaps']}}" style="margin-right: 10px;" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
                <i class="fas fa-globe-americas fa-sm text-white-50"></i>Mapas
            </a>
            <a href="{{$urls['getsemepdem']}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
                <i class="far fa-calendar-alt fa-sm text-white-50"></i> Semanas Epidemiológicas
            </a>
        </div>
    </div>
    <div class="row">
        @include('components.bar-list')
        @isset($total)
            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-2 col-md-4 mb-2">
                <div class="card shadow h-100 py-2" style="border-left: 0.25rem solid {{$total->color}}">
                    <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-uppercase mb-1" style="color:{{$total->color}} ">{{$total->titulo}}</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{$total->media}}</div>
                        </div>
                        <div class="col-auto">
                        <i class="fas fa-stethoscope fa-2x text-gray-400"></i>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        @endisset
    </div>
    <div class="row">
        <div class="col-lg-12 mb-10">
            <!-- Maps -->
            <div class="card shadow mb-10">
                <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Comparativos de semanas por ano</h6>
                </div>
                <div class="card-body">
                    <div id="map" class="z-depth-1-half map-container" style="height: 750px"></div>
                    <hr>
                    Dados SinanOnline
                </div>
            </div>
        </div>
    </div>

    <div class="row" id="incidencia-bairros">

    </div>
@endsection
@section('scripts')
<!-- Page level plugins -->
<script src="{{ mix('/js/markerclusterer.js') }}"></script>
<script src="{{ mix('/js/datatables.js') }}"></script>
<script type="text/javascript">
    var xcsrftoken = $('meta[name="csrf-token"]').attr('content');
    var token = "{{csrf_token()}}";
    var get_geocodes = "{{$urls['getgeocodes']}}";
    var get_data_bairro = "{{$urls['getdatabairro']}}";


    if(markerClusters){
        delete markerClusters;
    }

    var markerClusters;
    var ano;
    var tableBairro;
    var fields;

    function createDataTable(idDiv, ano){
        let anos = [ano];
        tableBairro = $("#"+idDiv).DataTable( {
        serverSide: true,
        processing: true,
        ajax:{
            url: get_data_bairro+'?'+decodeURIComponent( $.param( anos )),
            dataType: "json"
        },
        info: false,
        paging: false,
        searching: false,
        ordering: false,
        columns: [{title: ano.value}, {title: "Qnt"}]
        } );

    }

    function createTable(fields){

        for (i = 0; i < fields.length; i++) {
            $('#incidencia-bairros').append(
              `<div class="col-lg-3 mb-4">
                 <div class="card shadow mb-4">
                   <div class="card-header py-3">
                     <h6 class="m-0 font-weight-bold text-primary">Notificações por bairro</h6>
                   </div>
                   <div class="card-body">
                     <table class="table-striped compact table-bordered" id="bairros${fields[i].value}" width="100%" cellspacing="0">
                     </table>
                   </div>
                </div>
              </div>`
            );
            createDataTable("bairros"+fields[i].value, fields[i]);
        }

    }

    function updateTable(fields, oldFields){
        for (i = 0; i < oldFields.length; i++) {
            $("#bairros"+oldFields[i].value).DataTable().destroy();
            $("#bairros"+oldFields[i].value).empty();
        }
        $('#incidencia-bairros').empty();

        createTable(fields);
    }

    function createCluster(fields){
        for(var i=0; i<fields.length; i++) {
            ano = fields[i].value;
            $.ajax({
            url : get_geocodes+'?ano='+ano,
            contentType: 'application/x-www-form-urlencoded',
            cache: false
            }).done(function(response){
                var data = JSON.parse(response);
                //console.log(JSON.parse(response));
                var markers = [];
                for (let j = 0; j < data.geocodes.length; j++) {
                    var latLng = new google.maps.LatLng(
                                        data.geocodes[j].ID_GEO1,
                                        data.geocodes[j].ID_GEO2
                                        );
                    var marker = new google.maps.Marker({
                                        position: latLng,
                                        title: data.geocodes[j].NU_ANO.toString()
                                    });
                    markers.push(marker);
                }

                markerCluster = new MarkerClusterer(map, markers,{
                styles: [{
                            height: 35,
                            width: 35,
                            anchor: [10, 0],
                            textColor: '#ffffff',
                            backgroundColor: data.color,
                            textSize: 10
                        }, {
                            height: 45,
                            width: 45,
                            anchor: [15, 0],
                            textColor: '#ffffff',
                            backgroundColor: data.color,
                            textSize: 11
                        }, {
                            height: 55,
                            width: 55,
                            anchor: [20, 0],
                            textColor: '#ffffff',
                            backgroundColor: data.color,
                            textSize: 12
                        }]
                });
                markerClusters.set(data.ano, markerCluster);
                //console.log(markerClusters);
            }).fail(function(response){
            console.log(response);
            }).always(function(response){
            });

        }
    }

    $(document).ready(function(){

        $('.anos').change(function(){

            for (var [key, value] of markerClusters) {
                 value.clearMarkers();
            }
            oldFields = fields;
            fields = $( ".anos:checked" ).serializeArray();
            createCluster(fields);
            updateTable(fields, oldFields);
        });

    });

    function initMap() {
          map = new google.maps.Map(document.getElementById('map'), {
          zoom: 12,
          center: {lat: -5.082324, lng: -42.796397}
        });
        markerClusters = new Map();
        fields = $( ".anos:checked" ).serializeArray();
        createCluster(fields);
        createTable(fields);

      }

</script>
<script async defer
src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAPS_KEY')}}&callback=initMap">
//google.maps.event.addDomListener(window, 'load', initialize);
</script>
@endsection
