@extends('layouts.app')
@section('css')
<link href="{{ mix('/fonts/icofont/icofont.min.css') }}" rel="stylesheet" type="text/css">
@endsection
@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">

      @if (count($imunosSipni) > 0)
      @forelse ($imunosSipni as $imuno)

        <div class="col-xl-3 col-md-6 mb-4">
          <div class="card border-left-primary shadow h-100 py-2">
            <div class="card-body">
              <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                  @isset($imuno->sigla)
                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">{{ $imuno->sigla }}</div>
                  @endisset   
                  <div class="h5 mb-0 font-weight-bold text-gray-800"> 
                    <a href="{{route('imunizacao.imuno', ['id' => $imuno->id])}}">
                    {{ $imuno->nome }}
                    </a>
                  </div>
                </div>
                <div class="col-auto">
                  <a href="{{route('imunizacao.imuno', ['id' => $imuno->id])}}">
                    <i class="icofont-injection-syringe icofont-3x  text-gray-500"></i>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      @empty
          <p>No Imuno</p>
      @endforelse
      @endif

    </div>
</div>
@endsection
