@extends('layouts.app')
  @section('css')
  <link href="{{ mix('/css/dataTables.bootstrap4.css') }}" rel="stylesheet" type="text/css">
  @endsection

  @section('content')

  <!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Bairro: {{$regional->name}} </h1>
  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
    </div>
    <div class="card-body">
      <div id="map" style="height: 750px;">

      </div>
    </div>
  </div>


  @endsection
  @section('scripts')

    <!-- Page level plugins -->
    <script type="text/javascript">
    var xcsrftoken = $('meta[name="csrf-token"]').attr('content');
    var token = "{{csrf_token()}}";
    </script>
    <script>

      var divContent = document.createElement('div');
      var h4Title = document.createElement('h4');
      divContent.className = 'content';
      var titulo = document.createTextNode('Bairro: {{$regional->name}}');
      h4Title.appendChild(titulo);
      divContent.appendChild(h4Title);

      var map;
      var markers = new Array();
      function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: {{env('CENTER_MAP_LAT')}}, lng: {{env('CENTER_MAP_LNG')}}},
        zoom: 12
        });

        var infoWindow = new google.maps.InfoWindow({
        content: divContent.outerHTML
        });

        var triangleCoords = [
          @forelse ($regional->geocodes as $geocode)
          {lat: {{$geocode->lat}}, lng: {{$geocode->lng}}},
          @empty
          {lat: {{env('CENTER_MAP_LAT')}}, lng: {{env('CENTER_MAP_LNG')}}}
          @endforelse
       ];

       // Construct the polygon.
        var bermudaTriangle = new google.maps.Polygon({
          paths: triangleCoords,
          strokeColor: '#FF0000',
          strokeOpacity: 0.8,
          strokeWeight: 2,
          fillColor: '#FF0000',
          fillOpacity: 0.35
        });
        bermudaTriangle.setMap(map);

        bermudaTriangle.addListener('click', function(event) {
          infoWindow.setPosition(event.latLng);
          infoWindow.open(map);
        });

      }
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAPS_KEY')}}&callback=initMap">
    //google.maps.event.addDomListener(window, 'load', initialize);
    </script>

  @endsection
