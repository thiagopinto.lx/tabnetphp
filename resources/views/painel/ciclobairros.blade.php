@extends('layouts.app')
  @section('css')
  <link href="{{ mix('/css/dataTables.bootstrap4.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ mix('/css/bootstrap-colorselector.css') }}" rel="stylesheet" type="text/css">
  @endsection

  @section('content')


  @isset($options['store'])
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#carregar-dbf">
      Carregar KML
    </button>

    <div class="modal fade" id="carregar-dbf" tabindex="-1" role="dialog" aria-labelledby="carregarModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="carregarModalLabel">Formulario de carregamento do DBF</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">

          <div id="divFormCarregar">
            <form class="addForm" action="{{$options['store']}}" method="post" enctype="multipart/form-data">
              @csrf
              <div class="form-group">
                <label for="exampleFormControlFile1">Selecione arquivo xml</label>
                <input type="file" class="form-control-file" id="fileDbf" name="fileKml" required>
              </div>
              <div class="loadDbf d-none">
                <div class="spinner-border" role="status">
                  <span class="sr-only">Loading...</span>
                </div>
              </div>

              <button type="submit" class="carregar btn btn-primary">Carregar</button>
            </form>
          </div>

          <div class="message alert">

          </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
        </div>
      </div>
    </div>
  </div>
  @endisset

  @isset($options['storebairrociclo'])
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#carregar-dbf">
      Adicionar Bairro ao Ciclo
    </button>

    <div class="modal fade" id="carregar-dbf" tabindex="-1" role="dialog" aria-labelledby="carregarModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="carregarModalLabel">Formulario de carregamento do DBF</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">

          <div id="divFormCarregar">
            <form class="addForm" action="{{$options['storebairrociclo']}}" method="post" enctype="multipart/form-data">
              @csrf
              <div class="form-group">
                <label>Bairro: </label>
                <input type="text" class="form-control typeahead" id="nomeBairro" placeholder="Bairro" name="nomeBairro">
                <input type="text" class="form-control-plaintext" id="idBairro" name="idBairro" required value="" readonly>
              </div>
              <div class="form-group">
                <label for="ipla">ipla: </label>
                <input type="text" id="ipla" class="form-control" placeholder="ipla" name="ipla">
              </div>
              <div class="loadDbf d-none">
                <div class="spinner-border" role="status">
                  <span class="sr-only">Loading...</span>
                </div>
              </div>

              <button type="submit" class="carregar btn btn-primary">Carregar</button>
            </form>
          </div>

          <div class="message alert">

          </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
        </div>
      </div>
    </div>
  </div>
  @endisset

  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">{{$options['descricao']}}</h6>
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th>Id</th>
              <th>Nome</th>
              <th>ipla</th>
              <th>Map</th>
              <th>Ação</th>
            </tr>
          </thead>
          <tfoot>
            <tr>
              <th>Id</th>
              <th>Nome</th>
              <th>ipla</th>
              <th>Map</th>
              <th>Ação</th>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>
  </div>
  @endsection

  @section('scripts')

    <!-- Page level plugins -->
    <script type="text/javascript">
      var xcsrftoken = $('meta[name="csrf-token"]').attr('content');
      var getindex = "{{ $options['getindex'] }}";
      var mylanguage = "{{ asset('/js/Portuguese-Brasil.json') }}";
      var token = "{{csrf_token()}}";
    </script>

    <script src="{{ mix('/js/ciclobairros.js') }}"></script>

    @isset($options['storebairrociclo'])
    <script src="{{ mix('/js/typeahead.min.js') }}"></script>

    <script type="text/javascript">

      jQuery(document).ready(function($) {
        // Set the Options for "Bloodhound" suggestion engine
        var engine = new Bloodhound({
            remote: {
                url: '{{ route('painel.bairros.autocomplete') }}?q=%QUERY%',
                wildcard: '%QUERY%'
            },
            datumTokenizer: Bloodhound.tokenizers.whitespace('q'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
        });

        $("#nomeBairro").typeahead({
            hint: true,
            highlight: true,
            minLength: 1,
        }, {
            name: 'name',
            display: 'name',
            source: engine,
            limit: 10,
            templates: {
              empty: [
                  '<div class="list-group search-results-dropdown"><div class="list-group-item">Não encontrado.</div></div>'
              ],
              header: [
                  '<div class="list-group search-results-dropdown">'
              ],
              suggestion: function (data) {
                  return '<a href="#" class="list-group-item">' + data.name + '</a>'
              }
            }
        }).bind('typeahead:select', function(event, data){
          $('#idBairro').val(data.id);
        });
      });


    </script>
    @endisset

  @endsection
