@extends('layouts.app')
  @section('css')
  <link href="{{ mix('/css/dataTables.bootstrap4.css') }}" rel="stylesheet" type="text/css">
  <style>

    .multiple-datasets .league-name {
      width: 80%;
      font-size: 1rem;
      margin: 5px 20px 5px 20px;
      padding: 3px 0;
      border-bottom: 1px solid #ccc;
      background-color: #fff;
    }

    .multiple-datasets .typeahead {
      width: 450px;
    }
  </style>
  @endsection

  @section('content')
  <div class="row">
      <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <div class="d-sm-flex align-items-right right-content-between mb-4 d-print-none">
          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#carregar-html">
            Carregar HTML
          </button>
        </div>
      </div>
      <div class="modal fade" id="carregar-html" tabindex="-1" role="dialog" aria-labelledby="carregarModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="carregarModalLabel">Formulario de carregamento do HTML</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
      
                <div id="divFormCarregar">
                  <form class="addForm" action="{{$options['store']}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                      <label for="exampleFormControlFile1">Selecione arquivo HTML</label>
                      <input type="file" class="form-control-file" id="fileHtml" name="fileHtml" required>
                    </div>
                    <div class="loadHtml d-none">
                      <div class="spinner-border" role="status">
                        <span class="sr-only">Loading...</span>
                      </div>
                    </div>
      
                    <button type="submit" class="carregar btn btn-primary">Carregar</button>
                  </form>
                </div>
      
                <div class="message">
      
                </div>
      
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
              </div>
            </div>
          </div>
        </div>
  </div>
  <div class="row">
      <!-- DataTales Example -->
    <div class="col-lg-12 mb-4">
        <div class="card shadow mb-4">
          <div class="card-header py-3">
            <div class="row">
              <div class="col-md-6">
                <h6 class="font-weight-bold text-primary">{{$options['descricao']}}</h6>
              </div>
              <div class="col-md-6 text-right">

              </div>
            </div>
            
          </div>
 
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" cellspacing="0">
                <thead>
                  <tr>
                    <th>Id</th>
                    <th>Nome Sies</th>
                    <th>Imuno</th>
                    <th>Atualização</th>
                    <th><i class="fas fa-list"></i></th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>Id</th>
                    <th>Nome Sies</th>
                    <th>Imuno</th>
                    <th>Atualização</th>
                    <th><i class="fas fa-list"></i></th>
                  </tr>
                </tfoot>
              </table>
            </div>
          </div>
        </div>
    </div>
  </div>

  @endsection
  @section('scripts')

    <!-- Page level plugins -->
    <script type="text/javascript">
      var xcsrftoken = $('meta[name="csrf-token"]').attr('content');
      var getindex = "{{$options['getindex']}}";
      var mylanguage = "{{ asset('/js/Portuguese-Brasil.json') }}";
      var token = "{{csrf_token()}}";
    </script>
    <script src="{{ mix('/js/load-sies.js') }}"></script>
    
  @endsection
