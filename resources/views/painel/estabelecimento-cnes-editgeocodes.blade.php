@extends('layouts.app')
  @section('css')
  <link href="{{ mix('/css/dataTables.bootstrap4.css') }}" rel="stylesheet" type="text/css">
  @endsection

  @section('content')

  <!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">{{$options['titulo']}} </h1>
  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <div class="row">
        <div class="col-md-6 form-inline">
          <div class="form-group">
            <ul id="paginate">

            </ul>
          </div>
          <div class="form-group">
            <nav aria-label="Page navigation">
              <select id="per_page" name="per_page" aria-controls="dataTable" class="custom-select custom-select form-control form-control" style="margin-top: -15px">
                <option value="5">5</option>
                <option value="10">10</option>
                <option value="25">25</option>
                <option value="50">50</option>
              </select>
            </nav>
          </div>
        </div>

        <div class="col-md-6 form-inline">
          <div class="form-row">
            <form class="form-inline">
              @csrf

                <div class="col">
                  <input type="text" disabled class="form-control" name="ID_GEO1" id="lat">
                </div>
                <div class="col">
                  <input type="text" disabled class="form-control" name="ID_GEO2" id="lng">
                </div>

            </form>
            <div class="col">
              <a id="updadegeocodes" class="btn btn-primary active" role="button">Salvar</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="card-body">
      <div id="map" style="height: 750px;">

      </div>
    </div>
  </div>


  @endsection
  @section('scripts')
    <script src="{{ mix('/js/bootstrap4-paginator.js') }}"></script>

    <!-- Page level plugins -->
    <script type="text/javascript">
    var xcsrftoken = $('meta[name="csrf-token"]').attr('content');
    var token = "{{csrf_token()}}";
    var get_data_geocodes = "{{route('painel.estabelecimentos-cnes.getdatageocodes')}}";
    var update_geocodes = "{{route('painel.estabelecimentos-cnes.updategeocodes')}}";
    var per_page = 5;
    var page = 1;
    var options = {currentPage: page, totalPages: 1};
    var markers = new Array();
    var infoWindows = new Array();
    var estabelecimentos = new Array();
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': xcsrftoken
      }
    });

    </script>
    <script>
      function createInfoWindow(estabelecimento){

        var divContent = document.createElement('div');
        var divSiteNotice = document.createElement('div');
        var divBodyContent = document.createElement('div');
        var h4SiteNotice = document.createElement('h4');
        divContent.className = 'content';
        divSiteNotice.className = 'siteNotice';
        divBodyContent.className = 'divBodyContent';
        var titulo = document.createTextNode('Estabelecimento: '+ estabelecimento.no_fantasia);
        h4SiteNotice.appendChild(titulo);
        divSiteNotice.appendChild(h4SiteNotice);
        var endereco = document.createTextNode(
                    'Estabelecimento: '+ estabelecimento.no_fantasia + ' , ' +
                    'Endereço: '+ estabelecimento.no_logradouro + ' ' +
                    estabelecimento.nu_endereco + ' ' +
                    'Bairro: '+ estabelecimento.no_bairro + ' ' +
                    'Complemento: '+ estabelecimento.no_complemento
                    );
        divBodyContent.appendChild(endereco);
        divContent.appendChild(divSiteNotice);
        divContent.appendChild(divBodyContent);

        return divContent;
      }

      function changePerPage(){
        per_page = $("#per_page").val();
        url = get_data_geocodes+'?page='+page+'&'+'per_page='+per_page;
        getDataGeocodes(url);
      }

      function updatePaginator(data){
        $('#paginate').bootstrapPaginator({
          currentPage: data.current_page, 
          totalPages: data.last_page,
          onPageClicked: function(e,originalEvent,type,page){
            per_page = $("#per_page").val();
            url = get_data_geocodes+'?page='+page+'&'+'per_page='+per_page;
            getDataGeocodes(url);
          }
        });
      }

      function updateMarkers(data){
        estabelecimentos = data.data;
        for (let i = 0; i < markers.length; i++) {
            markers[i].setMap(null);

        }
        for (let i = 0; i < estabelecimentos.length; i++) {

          markers[i] = new google.maps.Marker({
          position: {lat: Number(estabelecimentos[i].nu_latitude), lng: Number(estabelecimentos[i].nu_longitude)},
          map: map,
          title: 'Estabelecimento: '+ estabelecimentos[i].no_fantasia,
          draggable: true
          });

          infoWindows[i] = new google.maps.InfoWindow({
          content: createInfoWindow(estabelecimentos[i]).outerHTML
          });
          markers[i].addListener('click', function() {
              infoWindows[i].open(map, markers[i]);
          });

          markers[i].addListener('dragend', function(){
            estabelecimentos[i].nu_latitude = markers[i].getPosition().lat();
            estabelecimentos[i].nu_longitude = markers[i].getPosition().lng();
            document.getElementById('lat').value = markers[i].getPosition().lat();
            document.getElementById('lng').value = markers[i].getPosition().lng();
            console.log(estabelecimentos[i]);
          });

      }

      }


      var map;
      function getDataGeocodes(url){
      $.ajax(
        {
          url: url,
          type: "get",
          datatype: "json"
        }).done(function(data){
            let d = JSON.parse(data);
            updatePaginator(d);
            updateMarkers(d);

        }).fail(function(jqXHR, ajaxOptions, thrownError){
            alert('sem reposta do servidor');
        });

      }

      function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
        zoom: 14,
        center: {lat: {{env('CENTER_MAP_LAT')}}, lng: {{env('CENTER_MAP_LNG')}}}
        });
      }

      $(document).ready(function() {
        per_page = $("#per_page").val();
        url = get_data_geocodes+'?page='+page+'&'+'per_page='+per_page;
        getDataGeocodes(url);
        $('#per_page').change(changePerPage);

        $('#updadegeocodes').click(function(event){
        event.preventDefault();

        $.ajax({
            method: 'put',
            url: update_geocodes,
            data: {_token: token, estabelecimentos: estabelecimentos},
            dataType: "json"
        }).done(function( msg ) {
            per_page = $("#per_page").val();
            url = get_data_geocodes+'?page='+page+'&'+'per_page='+per_page;
            getDataGeocodes(url);
        }).fail(function(jqXHR, ajaxOptions, thrownError){
            alert('sem reposta do servidor');
        });
      });



      });
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAPS_KEY')}}&callback=initMap">
    //google.maps.event.addDomListener(window, 'load', initialize);
    </script>
  @endsection
