@extends('layouts.app')
  @section('css')
  <link href="{{ mix('/css/dataTables.bootstrap4.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ mix('/css/bootstrap-colorselector.css') }}" rel="stylesheet" type="text/css">
  @endsection

  @section('content')
  <a class="btn btn-primary" href="{{ route('register')}}" role="button">Novo Usuário</a>
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">{{$options['descricao']}}</h6>
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th>id</th>
              <th>Nome</th>
              <th>E-mail</th>
              <th>Delete</th>
              <th>Reset Senha</th>
            </tr>
          </thead>
          <tfoot>
            <tr>
              <th>id</th>
              <th>Nome</th>
              <th>E-mail</th>
              <th>Delete</th>
              <th>Reset Senha</th>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>
  </div>
  @endsection
  @section('scripts')

    <!-- Page level plugins -->
    <script type="text/javascript">
      var xcsrftoken = $('meta[name="csrf-token"]').attr('content');
      var getindex = "{{$options['getindex']}}";
      var mylanguage = "{{ asset('/js/Portuguese-Brasil.json') }}";
      var token = "{{csrf_token()}}";
    </script>

    <script src="{{ mix('/js/users.js') }}"></script>
  @endsection
