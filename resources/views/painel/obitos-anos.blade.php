@extends('layouts.app')
  @section('css')
  <link href="{{ mix('/css/dataTables.bootstrap4.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ mix('/css/bootstrap-colorselector.css') }}" rel="stylesheet" type="text/css">
  @endsection

  @section('content')
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#carregar-dbf">
      Carregar DBF
    </button>
    <div class="modal fade" id="carregar-dbf" tabindex="-1" role="dialog" aria-labelledby="carregarModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="carregarModalLabel">Formulario de carregamento do DBF</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">

          <div id="divFormCarregar">
            <div class="alert alert-warning" role="alert">
              Anos encerrados só precisão ser carregados uma vez.
            </div>
            <form class="addForm" action="{{$urls['store']}}" method="post" enctype="multipart/form-data">
              @csrf
              <div class="form-group">
                <label for="inputAno">Ano:</label>
                <input type="text" class="form-control" id="inputAno" name="inputAno" placeholder="Ano a ser carregado" required>
              </div>
              <div class="form-group">
                <select class="colorselector" name="color">
                @foreach ($cores as $key => $value)
                <option value="{{$value}}" data-color="{{$value}}">{{$key}}</option>
                @endforeach
                </select>
              </div>
              <div class="form-group">
                <label for="exampleFormControlFile1">Selecione arquivo DBF</label>
                <input type="file" class="form-control-file" id="fileDbf" name="fileDbf" required>
              </div>
              <div class="loadDbf d-none">
                <div class="spinner-border" role="status">
                  <span class="sr-only">Loading...</span>
                </div>
              </div>

              <button type="submit" class="carregar btn btn-primary">Carregar</button>
            </form>
          </div>

          <div class="message">

          </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
        </div>
      </div>
    </div>
  </div>

  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">{{$options['descricao']}}</h6>
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th>Ano</th>
              <th style="min-width: 100px;">Responsavel</th>
              <th>Atualização</th>
              <th class="d-none">Status</th>
              <th style="max-width: 30px;">Cor</th>
              <th style="max-width: 410px;">Ação</th>
              <th>Notificações</th>
            </tr>
          </thead>
          <tfoot>
            <tr>
              <th>Ano</th>
              <th>Responsavel</th>
              <th>Atualização</th>
              <th class="d-none">Status</th>
              <th>Cor</th>
              <th>Ação</th>
              <th>Notificações</th>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>
  </div>
  @endsection
  @section('scripts')

    <!-- Page level plugins -->
    <script type="text/javascript">
      var xcsrftoken = $('meta[name="csrf-token"]').attr('content');
      var getindex = "{{$urls['getindex']}}";
      var mylanguage = "{{ asset('/js/Portuguese-Brasil.json') }}";
      var token = "{{csrf_token()}}";
    </script>

    <script src="{{ mix('/js/agravos.js') }}"></script>
  @endsection
