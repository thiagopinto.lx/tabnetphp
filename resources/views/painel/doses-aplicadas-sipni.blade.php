@extends('layouts.app')
  @section('css')
  <link href="{{ mix('/css/dataTables.bootstrap4.css') }}" rel="stylesheet" type="text/css">
  @endsection

  @section('content')
  <div class="row">
      <!-- DataTales Example -->
    <div class="col-lg-12 mb-4">
        <div class="card shadow mb-4">
          <div class="card-header py-3">
            <div class="row">
              <div class="col-md-6">
                <h6 class="font-weight-bold text-primary">{{$options['descricao']}}</h6>
              </div>
              <div class="col-md-6 text-right">

              </div>
            </div>
            
          </div>
 
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" cellspacing="0">
                <thead>
                  <tr>
                    <th>Id</th>
                    <th>Vacinado</th>
                    <th>Imuno</th>
                    <th>idade</th>
                    <th>Data de Aplicação</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>Id</th>
                    <th>Vacinado</th>
                    <th>Imuno</th>
                    <th>idade</th>
                    <th>Data de Aplicação</th>
                  </tr>
                </tfoot>
              </table>
            </div>
          </div>
        </div>
    </div>
  </div>

  @endsection
  @section('scripts')

    <!-- Page level plugins -->
    <script type="text/javascript">
      var xcsrftoken = $('meta[name="csrf-token"]').attr('content');
      var getindex = "{{$options['getindex']}}";
      var mylanguage = "{{ asset('/js/Portuguese-Brasil.json') }}";
      var token = "{{csrf_token()}}";
    </script>
    <script src="{{ mix('/js/doses-aplicadas-sipni.js') }}"></script>
    
  @endsection
