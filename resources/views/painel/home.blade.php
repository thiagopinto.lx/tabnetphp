@extends('layouts.app')
  @section('css')
  @endsection

  @section('content')

          <div class="row">

            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                    <div class="h5 mb-0 font-weight-bold text-gray-800"> <a href="{{route('painel.dengue')}}">Dengue</a></div>
                    </div>
                    <div class="col-auto">
                      <a href="{{route('painel.dengue')}}">
                        <i class="fas fa-plus-square fa-2x text-gray-300"></i>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><a href="{{route('painel.chik')}}">Chikungunya</a></div>
                    </div>
                    <div class="col-auto">
                      <a href="{{route('painel.chik')}}">
                        <i class="fas fa-plus-square fa-2x text-gray-300"></i>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-xl-3 col-md-6 mb-4">
                <div class="card border-left-primary shadow h-100 py-2">
                    <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="h5 mb-0 font-weight-bold text-gray-800"><a href="{{route('painel.zika')}}">Zika</a></div>
                        </div>
                        <div class="col-auto">
                          <a href="{{route('painel.chik')}}">
                            <i class="fas fa-plus-square fa-2x text-gray-300"></i>
                          </a>
                        </div>
                    </div>
                    </div>
                </div>
                </div>

            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="h5 mb-0 font-weight-bold text-gray-800"><a href="{{route('painel.leishvisc')}}">Lashmaniose Visceral</a></div>
                    </div>
                    <div class="col-auto">
                        <a href="{{route('painel.leishvisc')}}">
                            <i class="fas fa-plus-square fa-2x text-gray-300"></i>
                        </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>

          <div class="row">

            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                    <div class="h5 mb-0 font-weight-bold text-gray-800"> <a href="{{route('painel.leishteg')}}">Leishmaniose Tegumentar</a></div>
                    </div>
                    <div class="col-auto">
                      <a href="{{route('painel.leishteg')}}">
                        <i class="fas fa-plus-square fa-2x text-gray-300"></i>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><a href="{{route('painel.meningite')}}">Meningite</a></div>
                    </div>
                    <div class="col-auto">
                      <a href="{{route('painel.meningite')}}">
                        <i class="fas fa-plus-square fa-2x text-gray-300"></i>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-xl-3 col-md-6 mb-4">
                <div class="card border-left-primary shadow h-100 py-2">
                    <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="h5 mb-0 font-weight-bold text-gray-800"><a href="{{route('painel.obito')}}">Obitos</a></div>
                        </div>
                        <div class="col-auto">
                          <a href="{{route('painel.obito')}}">
                            <i class="fas fa-plus-square fa-2x text-gray-300"></i>
                          </a>
                        </div>
                    </div>
                    </div>
                </div>
                </div>

            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="h5 mb-0 font-weight-bold text-gray-800"><a href="{{route('painel.ciclos')}}">Cilos LIRAa</a></div>
                    </div>
                    <div class="col-auto">
                        <a href="{{route('painel.ciclos')}}">
                            <i class="fas fa-plus-square fa-2x text-gray-300"></i>
                        </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>

          <div class="row">

            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="h5 mb-0 font-weight-bold text-gray-800"> <a href="{{route('painel.cids')}}">Codigo Internacional de Doencas</a></div>
                    </div>
                    <div class="col-auto">
                      <a href="{{route('painel.cids')}}">
                        <i class="fas fa-plus-square fa-2x text-gray-300"></i>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="h5 mb-0 font-weight-bold text-gray-800"> <a href="{{route('painel.unidadesnotificadoras')}}">Unidades Notificadoras SinanNet</a></div>
                    </div>
                    <div class="col-auto">
                      <a href="{{route('painel.unidadesnotificadoras')}}">
                        <i class="fas fa-plus-square fa-2x text-gray-300"></i>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="h5 mb-0 font-weight-bold text-gray-800"> <a href="{{route('painel.bairros')}}">Lista Bairros</a></div>
                    </div>
                    <div class="col-auto">
                      <a href="{{route('painel.bairros')}}">
                        <i class="fas fa-plus-square fa-2x text-gray-300"></i>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="h5 mb-0 font-weight-bold text-gray-800"> <a href="{{route('painel.cids')}}">Lista de Regionais</a></div>
                    </div>
                    <div class="col-auto">
                      <a href="{{route('painel.regionais')}}">
                        <i class="fas fa-plus-square fa-2x text-gray-300"></i>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>

          <div class="row">

            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="h5 mb-0 font-weight-bold text-gray-800"> <a href="{{route('painel.imunizacao')}}">Dados da Imunização</a></div>
                    </div>
                    <div class="col-auto">
                      <a href="{{route('painel.imunizacao')}}">
                        <i class="fas fa-plus-square fa-2x text-gray-300"></i>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>

  @endsection
  @section('scripts')
    <!-- Page level plugins -->
    <script type="text/javascript">

    </script>
  @endsection
