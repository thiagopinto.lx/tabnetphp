@extends('layouts.app')
  @section('css')
  @endsection

  @section('content')

          <div class="row">

            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                    <div class="h5 mb-0 font-weight-bold text-gray-800"> <a href="{{route('painel.estabelecimentos-sies')}}">Estabelecimento SIES</a></div>
                    </div>
                    <div class="col-auto">
                      <a href="{{route('painel.estabelecimentos-sies')}}">
                        <i class="fas fa-plus-square fa-2x text-gray-300"></i>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><a href="{{route('painel.imuno-sies')}}">Imunobiológicos SIES</a></div>
                    </div>
                    <div class="col-auto">
                      <a href="{{route('painel.imuno-sies')}}">
                        <i class="fas fa-plus-square fa-2x text-gray-300"></i>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-xl-3 col-md-6 mb-4">
                <div class="card border-left-primary shadow h-100 py-2">
                    <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="h5 mb-0 font-weight-bold text-gray-800"><a href="{{route('painel.load-sipni')}}">Doses Aplicadas SIPNI</a></div>
                        </div>
                        <div class="col-auto">
                          <a href="{{route('painel.load-sipni')}}">
                            <i class="fas fa-plus-square fa-2x text-gray-300"></i>
                          </a>
                        </div>
                    </div>
                    </div>
                </div>
                </div>

            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="h5 mb-0 font-weight-bold text-gray-800"><a href="{{route('painel.load-sies')}}">Doses Distribuidas SIES</a></div>
                    </div>
                    <div class="col-auto">
                        <a href="{{route('painel.load-sies')}}">
                            <i class="fas fa-plus-square fa-2x text-gray-300"></i>
                        </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>

  @endsection
  @section('scripts')
    <!-- Page level plugins -->
    <script type="text/javascript">

    </script>
  @endsection
