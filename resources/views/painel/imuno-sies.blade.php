@extends('layouts.app')
  @section('css')
  <link href="{{ mix('/css/dataTables.bootstrap4.css') }}" rel="stylesheet" type="text/css">
  <style>

    .multiple-datasets .league-name {
      width: 80%;
      font-size: 1rem;
      margin: 5px 20px 5px 20px;
      padding: 3px 0;
      border-bottom: 1px solid #ccc;
      background-color: #fff;
    }

    .multiple-datasets .typeahead {
      width: 450px;
    }
  </style>
  @endsection

  @section('content')

  <div class="row">
      <!-- DataTales Example -->
    <div class="col-lg-12 mb-4">
        <div class="card shadow mb-4">
 
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" cellspacing="0">
                <thead>
                  <tr>
                    <th>Id</th>
                    <th>Nome Sies</th>
                    <th>Atualização</th>
                    <th>Perda Técnica %</th>
                    <th>Nome Sipni</th>
                    <th><i class="far fa-edit"></i></th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>Id</th>
                    <th>Nome Sies</th>
                    <th>Atualização</th>
                    <th>Perda Técnica %</th>
                    <th>Nome Sipni</th>
                    <th><i class="far fa-edit"></i></th>
                  </tr>
                </tfoot>
              </table>
            </div>
          </div>
        </div>
    </div>
  </div>

  @endsection
  @section('scripts')

    <!-- Page level plugins -->
    <script type="text/javascript">
      var xcsrftoken = $('meta[name="csrf-token"]').attr('content');
      var getindex = "{{$options['getindex']}}";
      var mylanguage = "{{ asset('/js/Portuguese-Brasil.json') }}";
      var token = "{{csrf_token()}}";
      var nome_url = "{{route('painel.imuno-sipni.autocomplete-nome')}}";
      var sigla_url = "{{route('painel.imuno-sipni.autocomplete-sigla')}}";
      var relationship_url = "{{route('painel.imuno-sies')}}";
      var perca_tecnica_url = "{{route('painel.imuno-sies')}}";
    </script>
    <script src="{{ mix('/js/imuno-sies.js') }}"></script>
    
  @endsection
