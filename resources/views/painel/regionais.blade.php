@extends('layouts.app')
  @section('css')
  <link href="{{ mix('/css/dataTables.bootstrap4.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ mix('/css/bootstrap-colorselector.css') }}" rel="stylesheet" type="text/css">
  @endsection

  @section('content')
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#carregar-dbf">
      Carregar KML
    </button>

    <div class="modal fade" id="carregar-dbf" tabindex="-1" role="dialog" aria-labelledby="carregarModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="carregarModalLabel">Formulario de carregamento do xml</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">

          <div id="divFormCarregar">
            <form class="addForm" action="{{$options['store']}}" method="post" enctype="multipart/form-data">
              @csrf
              <div class="form-group">
                <label for="exampleFormControlFile1">Selecione arquivo DBF</label>
                <input type="file" class="form-control-file" id="fileDbf" name="fileKml" required>
              </div>
              <div class="loadDbf d-none">
                <div class="spinner-border" role="status">
                  <span class="sr-only">Loading...</span>
                </div>
              </div>

              <button type="submit" class="carregar btn btn-primary">Carregar</button>
            </form>
          </div>

          <div class="message">

          </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
        </div>
      </div>
    </div>
  </div>

  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">{{$options['descricao']}}</h6>
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th>Id</th>
              <th>Nome</th>
              <th>View</th>
            </tr>
          </thead>
          <tfoot>
            <tr>
              <th>Id</th>
              <th>Nome</th>
              <th>View</th>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>
  </div>
  @endsection
  @section('scripts')

    <!-- Page level plugins -->
    <script type="text/javascript">
      var xcsrftoken = $('meta[name="csrf-token"]').attr('content');
      var getindex = "{{$options['getindex']}}";
      var mylanguage = "{{ asset('/js/Portuguese-Brasil.json') }}";
      var token = "{{csrf_token()}}";
    </script>

    <script src="{{ mix('/js/bairros.js') }}"></script>
  @endsection
