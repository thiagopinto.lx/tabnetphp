@extends('layouts.app')
  @section('css')
  <link href="{{ mix('/css/dataTables.bootstrap4.css') }}" rel="stylesheet" type="text/css">
  @endsection

  @section('content')

  <!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Tabelas de {{$tabela}} </h1>
  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <div class="row">
        <div class="col-md-6">
          <h6 class="m-0 font-weight-bold text-primary">
              Lista tabelas {{$tabela}} {{$porcentagem}}% geo-referenciadas
          </h6>
        </div>
        <div class="col-md-6 text-right">
        <a class="btn btn-primary" href="{{route('painel.notificacao.editgeocodes', ['tabela' => $tabela])}}" role="button">Ajustar pontos</a>
          <a class="ali btn btn-primary" id="checked-all" href="#" role="button">Marcar Todos</a>
          <a class="ali btn btn-primary button-gps" href="#" role="button">
            <span class="spinner-border spinner-border-sm d-none" role="status" aria-hidden="true"></span>
            <span class="fas fa-globe-americas" role="status" aria-hidden="true"></span>
          </a>
        </div>
      </div>
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th>Notif</th>
              <th>Data</th>
              <th>Bairro</th>
              <th>Logradouro</th>
              <th>Complemento</th>
              <th>Referêcia</th>
              <th>Lat</th>
              <th>Lng</th>
              <th><i class="fas fa-globe-americas"></i></th>
              <th><i class="far fa-edit"></i></th>
            </tr>
          </thead>
          <tfoot>
            <tr>
                <th>Notif</th>
                <th>Data</th>
                <th>Bairro</th>
                <th>Logradouro</th>
                <th>Complemento</th>
                <th>Referêcia</th>
                <th>Lat</th>
                <th>Lng</th>
                <th><i class="fas fa-globe-americas"></i></th>
                <th><i class="far fa-edit"></i></th>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>
  </div>


  @endsection
  @section('scripts')

    <!-- Page level plugins -->
    <script type="text/javascript">
    var xcsrftoken = $('meta[name="csrf-token"]').attr('content');
    var getindex = "{{ route('painel.notificacao.getindex' , compact('tabela'))}}";
    var getgps = "{{ route('painel.notificacao.getgps', compact('tabela'))}}"
    var mylanguage = "{{ asset('/js/Portuguese-Brasil.json') }}";
    var token = "{{csrf_token()}}";
    </script>
    <script src="{{ mix('/js/notificacoes.js') }}"></script>

  @endsection
