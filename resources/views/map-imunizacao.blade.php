@extends('layouts.app')
@section('css')
<link href="{{ mix('/css/Chart.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ mix('/fonts/icofont/icofont.min.css') }}" rel="stylesheet" type="text/css">
@endsection
@section('content')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <div class="d-sm-flex align-items-right right-content-between mb-4 d-print-none">
        </div>
    </div>
    <div class="row">
        @include('components.bar-list-imunizacao')
        @isset($total)
            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-2 col-md-4 mb-2">
                <div class="card shadow h-100 py-2" style="border-left: 0.25rem solid {{$total->color}}">
                    <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-uppercase mb-1" style="color:{{$total->color}} ">{{$total->titulo}}</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{$total->media}}</div>
                        </div>
                        <div class="col-auto">
                        <i class="fas fa-stethoscope fa-2x text-gray-400"></i>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        @endisset
    </div>

    <div class="row">
        <div class="col-lg-12 mb-10">
            <!-- Maps -->
            <div class="card shadow mb-10">
                <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Comparativos de semanas por ano</h6>
                </div>
                <div class="card-body">
                    <div id="map" class="z-depth-1-half map-container" style="height: 750px"></div>
                    <hr>
                    Dados Sipin & Sies
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 mb-10">
            <div class="card shadow mb-10">
            </div>
        </div>
    </div>

    <div id="anos-dose">

    </div>


@endsection
@section('scripts')
<!-- Page level plugins -->
<script src="{{ mix('/js/markerclusterer.js') }}"></script>
<script src="{{ mix('/js/datatables.js') }}"></script>
<script type="text/javascript">
    var xcsrftoken = $('meta[name="csrf-token"]').attr('content');
    var token = "{{csrf_token()}}";
    var get_estabelecimentos = "{{$urls['estabelecimentos']}}";
    var get_estabelecimento = "{{$urls['estabelecimento']}}";
    var idImuno = {{$idImuno}};

    if(markers){
        delete markers;
        delete infowindows;
    }

    var markers = [];
    var infowindows = [];
    //var ano;
    var fields;

    function createInfoWindow(estabelecimento, ano){
        let stringInfoWindow = 
                    `<div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">${estabelecimento.nome}</h6>
                        </div>
                        <div class="card-body">
                            <p>Doses Aplicadas: ${estabelecimento.doses_aplicadas}</p>
                            <p>Doses Distribuidas: ${estabelecimento.doses_distribuidas}<p>
                            <p><a href="${get_estabelecimento}/${idImuno}/${ano}/${estabelecimento.id}">Detalhamento <i class="icofont-chart-line"></i></a></p>
                        </div>
                    </div>`;

        return new google.maps.InfoWindow({
                        content: stringInfoWindow
                    });
    }

    function createMarker(estabelecimento, color){
        let latLng = new google.maps.LatLng(estabelecimento.latitude, estabelecimento.longitude);
        return new google.maps.Marker({
                        position: latLng,
                        icon: {
                        path: google.maps.SymbolPath.CIRCLE,
                        fillColor: color,
                        fillOpacity: 0.9,
                        strokeWeight: 0,
                        scale: 20
                        },
                        label: {
                        text: String.fromCharCode("0xec24"),
                        fontFamily: 'IcoFont',
                        fontSize: '20px',
                        color: '#ffffff',
                        },
                        map: map,
                        labelContent: '<i class="icofont-ambulance-cross"></i>',
                        labelAnchor: new google.maps.Point(estabelecimento.latitude, estabelecimento.longitude)
                    });
    }



    function createTable(ano, estabelecimentos){

        let idTabela = `doses-aplicada-per-doses-distribuidas${ano}`;

        let trs = ''; 
        
        for (let i = 0; i < estabelecimentos.length; i++) {
          if(estabelecimentos[i].doses_distribuidas == null){
            estabelecimentos[i].doses_distribuidas = "";
          }
          trs += 
          `<tr>
              <td>${estabelecimentos[i].nome}</td>
              <td>${estabelecimentos[i].doses_aplicadas}</td>
              <td>${estabelecimentos[i].doses_distribuidas}</td>
              <td>${estabelecimentos[i].perda_tecnica_aceitavel}</td>
              <td>${estabelecimentos[i].perda_total}</td>
              <td><a href="${get_estabelecimento}/${idImuno}/${ano}/${estabelecimentos[i].id}">Detalhamento</a></td>
            </tr>`;
        }

                
        $('#anos-dose').append(
          `<div class="row">
            <div class="col-lg-12 mb-10">
              <div class="card">
                <div class="card-header">
                <h6 class="m-0 font-weight-bold text-primary">Doses Distribuidas por Doses Aplicadas ${ano}</h6>
                </div>
                <div class="card-body">
                  <table class="table table-striped compact table-bordered" id="${idTabela}" width="100%" cellspacing="0">
                    <thead>
                      <tr>
                      <th scope="col">Estabelecimento</th>
                      <th scope="col">Doses Aplicadas</th>
                      <th scope="col">Doses Distribuidas</th>
                      <th scope="col">Perda Técnica Aceitavel</th>
                      <th scope="col">Total Perda</th>
                      <th scope="col">Detalhes</th>
                      </tr>
                    </thead>
                    <tbody>
                    ${trs}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>`
        );
      return 
    }


    function createPoint(fields){
        for(let i=0; i<fields.length; i++) {
            let ano = fields[i].value;
            $.ajax({
            url : `${get_estabelecimentos}/${idImuno}/${ano}`,
            contentType: 'application/x-www-form-urlencoded',
            cache: false
            }).done(function(response){
                let data = JSON.parse(response);
                let estabelecimentos = data.estabelecimentos;
                createTable(ano, estabelecimentos);
                let color = data.color;
                for (var i = 0; i < estabelecimentos.length; i++) {
                    console.log(estabelecimentos[i]);

                    let infowindow = createInfoWindow(estabelecimentos[i], ano);

                    marker = createMarker(estabelecimentos[i], color);

                    marker.addListener('click', function() {
                        infowindow.open(map, this);
                    });
                    infowindows.push(infowindow);
                    markers.push(marker);
                    //marker.setMap( map );
                }

                //points.set(data.ano, null);
                //console.log(markerClusters);
            }).fail(function(response){
            console.log(response);
            }).always(function(response){
            });

        }
    }

    $(document).ready(function(){

        $('.anos').change(function(){

            for (var i = 0; i < markers.length; i++) {
                markers[i].setMap(null);
            }
            markers = [];

            oldFields = fields;

            fields = $( ".anos:checked" ).serializeArray();
            $('#anos-dose').empty();    
            createPoint(fields);
        });

    });

    function initMap() {
          map = new google.maps.Map(document.getElementById('map'), {
          zoom: 12,
          center: {lat: -5.082324, lng: -42.796397}
        });
        points = new Map();
        fields = $( ".anos:checked" ).serializeArray();
      }

</script>
<script async defer
src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAPS_KEY')}}&callback=initMap">
//google.maps.event.addDomListener(window, 'load', initialize);
</script>
@endsection
