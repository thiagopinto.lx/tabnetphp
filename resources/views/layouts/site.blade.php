<!doctype html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="{{ asset('images/fms-mini.png') }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Diretoria de Vigilância em Saúde">
    <meta name="author" content="Thiago Pinto Dias">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Diretoria de Vigilância em Saúde</title>


    <!-- Custom styles for this template -->
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:700,900" rel="stylesheet">
    <link href="{{ mix('/css/app.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ mix('/css/site.css') }}" rel="stylesheet" type="text/css">
  </head>

  <body>

    <div class="container-fluid">
      @include('layouts.header-site')

      @include('layouts.menu-site')

      @yield('content')

      @include('layouts.footer-site')

    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="{{ mix('/js/site.js') }}"></script>
    <script>
      Holder.addTheme('thumb', {
        bg: '#55595c',
        fg: '#eceeef',
        text: 'Thumbnail'
      });
    </script>
  </body>
</html>