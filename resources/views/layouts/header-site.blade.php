<header class="blog-header py-3">
  <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-blue-fms d-flex justify-content-between">
  <img src="{{ asset('images/fms_logo_mini.png') }}" class="img-fluid blog-header-logo" alt="Diretoria de Vigilância em Saúde">
  <form class="form-inline mt-2 mt-md-0">
    <input class="form-control mr-sm-2" type="text" placeholder="Busca" aria-label="Search">
    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Busca</button>
  </form>
  </nav>
</header>
<div class="container mt-5">
  <div class="row flex-nowrap justify-content-between align-items-center">
    <div class="col-12 text-center">
      <img src="{{ asset('images/logo-fms-dvs.png') }}" class="img-fluid blog-header-logo" alt="Diretoria de Vigilância em Saúde">
    </div>
  </div>
</div>
