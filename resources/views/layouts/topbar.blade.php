<!-- Topbar -->
<nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
  <!-- Sidebar Toggle (Topbar) -->
  <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
    <i class="fa fa-bars"></i>
  </button>
  @if(isset($options['titulo']))
  <a href="#" class="btn btn-light btn-icon-split">
    <span class="icon text-gray-600">
      <i class="fas fa-arrow-right"></i>
    </span>
    <span class="text mb-0 text-gray-800">{{$options['titulo']}}</span>
  </a>
  @elseif(isset($fullName))
  <a href="#" class="btn btn-light btn-icon-split">
    <span class="icon text-gray-600">
      <i class="fas fa-arrow-right"></i>
    </span>
    <span class="text mb-0 text-gray-800">{{$fullName}}</span>
  </a>
  @endif
</nav>

<!-- End of Topbar -->
