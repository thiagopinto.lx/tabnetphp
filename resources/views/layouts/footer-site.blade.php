<footer class="blog-footer jumbotron p-3 p-md-5">
  <div class="row">
      <div class="col-md-12">
          <h3><strong>Fundação Municipal de Saúde de Teresina - FMS</strong></h3>
      </div>
  </div>
  <div class="row">
      <div class="col-md-6">
          <h4>Contato</h4>
          <ul class="list-unstyled">
              <li><i class="fas fa-home"></i> Av. Miguel Rosa, 3860-3898 - Centro (Sul), Teresina - PI, 64018-560</li>
              <li><i class="fas fa-phone"></i> (+55) 86 xxxx-xxxx</li>
              <li><i class="fas fa-envelope"></i> <a href="" target="_blank">XXXXXXXXXXXXXXXX</a></li>
              <li><i class="fas fa-map-marker"></i> <a href="https://www.google.com/maps/place/SUS+-+Secretaria+Municipal+De+Saude/@-5.0942212,-42.801584,15z/data=!4m5!3m4!1s0x0:0x898c9eeaf1198303!8m2!3d-5.0942212!4d-42.801584" target="_blank">Encontre no Google Maps</a></li>
          </ul>
      </div>
      <div class="col-md-2">

      </div>
      <div class="col-md-4">
        <a target="_blank" href="http://www.teresina.pi.gov.br"><img alt="Prefeitura Municipal de Teresina" title="Prefeitura Municipal de Teresina" class="img-responsive" src="{{ asset('images/logoteresina.png')}}"></a>
      </div>
  </div>

</footer>