<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion toggled d-print-none" id="accordionSidebar">

  <!-- Sidebar - Brand -->
  <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{route('index')}}">
    <div class="sidebar-brand-icon rotate-n-15">
      <img src="{{ asset('images/fms_logo_mini.png') }}" class="img-fluid blog-header-logo" alt="Diretoria de Vigilância em Saúde">
    </div>
    <div class="sidebar-brand-text mx-3">TabNet <sup>php</sup></div>
  </a>

  <!-- Divider -->
  <hr class="sidebar-divider my-0">
  <!-- Nav Item - Dashboard -->
  <li class="nav-item">
    <a class="nav-link" href="{{route('arbovirose')}}">
      <i class="fas fa-fw fa-tachometer-alt"></i>
      <span>Arboviroses</span></a>
  </li>
  <!-- Divider -->
  <hr class="sidebar-divider">
  <!-- Nav Item - Dashboard -->
  <li class="nav-item">
    <a class="nav-link" href="{{route('dengue.index')}}">
      <i class="fas fa-fw fa-tachometer-alt"></i>
      <span>Dengue</span></a>
  </li>
  <!-- Divider -->
  <hr class="sidebar-divider">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item">
        <a class="nav-link" href="{{route('chik.index')}}">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Chikungunya</span></a>
      </li>
      <!-- Divider -->
      <hr class="sidebar-divider">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item">
        <a class="nav-link" href="{{route('zika.index')}}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Zika</span></a>
        </li>
    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item">
        <a class="nav-link" href="{{route('leishvisc.index')}}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Lashmaniose Visceral</span></a>
        </li>
    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item">
        <a class="nav-link" href="{{route('leishteg.index')}}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Leishmaniose Tegumentar Americana</span></a>
        </li>
    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item">
        <a class="nav-link" href="{{route('meningite.index')}}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Meniginte</span></a>
        </li>
    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item">
        <a class="nav-link" href="{{route('obitoinfantil.index')}}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Obitos Infantis</span></a>
        </li>
    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item">
        <a class="nav-link" href="{{route('obitoperinatal.index')}}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Obitos Perinatal</span></a>
        </li>
    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item">
        <a class="nav-link" href="{{route('imunizacao.index')}}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Imunização</span></a>
        </li>
    <!-- Divider -->
    <hr class="sidebar-divider">

  <!-- Sidebar Toggler (Sidebar) -->
  <div class="text-center d-none d-md-inline">
    <button class="rounded-circle border-0" id="sidebarToggle"></button>
  </div>

    <!-- Nav Item - Dashboard -->
    <li class="nav-item">
        <a class="nav-link" href="{{route('painel.home')}}">
        <i class="fas fa-tools"></i>
        <span>Painel</span></a>
    </li>
    <!-- Divider -->
    <hr class="sidebar-divider">


</ul>
<!-- End of Sidebar -->
