@extends('layouts.app')
@section('css')
<style>
#legend {
    position: relative;
    width: 100%;
    height: 30px;
    margin-top: 10px;
}

#legendGradient {
    width: 100%;
    height: 15px;
    border: 1px solid black;
}

</style>


@endsection
@section('content')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <div class="d-sm-flex align-items-right right-content-between mb-4">
            <a href="{{$urls['detalhamentos']}}" style="margin-right: 10px;" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
                <i class="fas fa-info-circle fa-sm text-white-50"></i> Detalhamento
            </a>
            <a href="{{$urls['clustersmaps']}}" style="margin-right: 10px;" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
                <i class="fas fa-globe-americas fa-sm text-white-50"></i>Mapas
            </a>
            <a href="{{$urls['getsemepdem']}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
                <i class="far fa-calendar-alt fa-sm text-white-50"></i> Semanas Epidemiológicas
            </a>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 mb-10">
            <!-- Maps -->
            <div class="card shadow mb-10">
                <div class="card-header py-3">
                <form>
                  <div class="form-row">
                    @isset($ciclos)
                      <div class="col">
                        <fieldset class="form-group">
                          <legend>Ciclos avaliação do LIRAa</legend>
                        @forelse ($ciclos as $ciclo)
                          <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="ciclo" id="ciclo{{$ciclo->nu_ciclo}}" value="{{$ciclo->id}}">
                            <label class="form-check-label" for="ciclo{{$ciclo->nu_ciclo}}">Ciclo {{$ciclo->nu_ciclo}}</label>
                          </div>
                        @empty
                          <p>Não tem ciclos cadastrados</p>
                        @endforelse
                        </fieldset>
                      </div>
                    @endisset
                    <div class="col">
                      <fieldset class="form-group row">
                        <legend>Período de avaliação</legend>
                        <div class="form-group mx-sm-3 mb-2">
                          <label for="dataInicio">Data inicio: </label>
                          <input type="date" class="form-control" name="dataInicio" id="dataInicio" min="{{$options['inicio']}}" max="{{$options['fim']}}" value="{{$options['inicio']}}" required>
                        </div>
                        <div class="form-group mx-sm-3 mb-2">
                          <label for="dataFim">Data Fim:</label>
                          <input type="date" class="form-control" name="dataFim" id="dataFim" min="{{$options['inicio']}}" max="{{$options['fim']}}" value="{{$options['fim']}}" required>
                        </div>
                      </fieldset>
                    </div>
                  </div>
                </form>

                </div>
                <div class="card-body">
                    <div id="map" class="z-depth-1-half map-container" style="height: 750px"></div>
                    <hr>
                    <div id="legend" class="d-flex">
                        <span>0</span>
                        <div id="legendGradient"></div>
                        <span>10</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')

<script type="text/javascript">
      var xcsrftoken = $('meta[name="csrf-token"]').attr('content');
      var token = "{{csrf_token()}}";
      var get_data_heatmap = "{{$urls['getdataheatmap']}}";
      var map, heatmap;
      var heatmapData = new Array();


      function initMap() {

            map = new google.maps.Map(document.getElementById('map'), {
            zoom: 12,
            center: {lat: -5.082324, lng: -42.796397}
          });

          getPoints();

      }

      $(document).ready(function(){
        $("input[name='dataInicio']").change(function(event) {
          heatmapData = [];
          heatmap.setData(heatmapData);
          getPoints();
        });
        $("input[name='dataFim']").change(function(event) {
          heatmapData = [];
          heatmap.setData(heatmapData);
          getPoints();
        });
      });
      @isset($ciclos)
        $(document).ready(function(){
          var get_data_ciclo = "{{$urls['getdataciclo']}}";
          var polygonBairro = new Array();
          var infoWindow = new Array();

          $("input[name='ciclo']").change(function(event) {
            let ciclo = $(this).val();

            $.ajax({
            url : get_data_ciclo+'?ciclo='+ciclo,
            contentType: 'application/x-www-form-urlencoded',
            cache: false
            }).done(function(response){
              let ciclo = JSON.parse(response);
              bairros = ciclo.bairros;
            }).fail(function(response){
            console.log(response);
            }).always(function(response){
              polygonBairro.forEach(function(bairro){
              bairro.setMap(null);
            });

              polygonBairro = new Array();

              bairros.forEach(function(bairro){

                let divContent = document.createElement('div');
                let h4Title = document.createElement('h4');
                divContent.className = 'content';
                let titulo = document.createTextNode(`Bairro: ${bairro.name}`);
                h4Title.appendChild(titulo);
                divContent.appendChild(h4Title);

                infoWindow[bairro.id] = new google.maps.InfoWindow({
                content: divContent.outerHTML
                });

                limites = new Array();
                limites[bairro.id] = new Array();

                bairro.geocodes.forEach(function(geocode){
                  limites[bairro.id].push({lat: Number(geocode.lat), lng: Number(geocode.lng)});
                });

                if(bairro.ipla <= 0.9){
                  risco = '#008000';
                }else if (bairro.ipla > 0.9 && bairro.ipla <= 3.9) {
                  risco = '#FFFF00';
                }else {
                  risco = '#FF0000';
                }
                   polygonBairro[bairro.id] = new google.maps.Polygon({
                   paths: limites[bairro.id],
                   strokeColor: risco,
                   strokeOpacity: 0.8,
                   strokeWeight: 2,
                   fillColor: risco,
                   fillOpacity: 0.35
                 });

                 polygonBairro[bairro.id].setMap(map);

                 polygonBairro[bairro.id].addListener('click', function(event) {
                   infoWindow[bairro.id].setPosition(event.latLng);
                   infoWindow[bairro.id].open(map);
                 });
              });

            });
          });
        });
      @endisset


      // Heatmap data: 500 Points
      function getPoints() {

        let inicio = $("input[name='dataInicio']").val();
        let fim = $("input[name='dataFim']").val();

        $.ajax({
        url : get_data_heatmap+'?inicio='+inicio+'&fim='+fim,
        contentType: 'application/x-www-form-urlencoded',
        cache: false
        }).done(function(response){
            var dataHeatMap = JSON.parse(response);
            for (var i = 0; i < dataHeatMap.length; i++) {
              heatmapData.push(new google.maps.LatLng(dataHeatMap[i].ID_GEO1, dataHeatMap[i].ID_GEO2));
            };
        }).fail(function(response){
        console.log(response);
        }).always(function(response){
          heatmap = new google.maps.visualization.HeatmapLayer({
            data: heatmapData,
            map: map
          });
          //heatmap.set('radius', 5);
          //heatmap.set('opacity', 0.5);
          heatmap.set('maxIntensity', 10);
          setGradient();
          setLegendGradient();
          return true;
        });
      }

      function setGradient() {
        gradient = [
          'rgba(0, 255, 255, 0)',
          'rgba(153, 204, 153, 1)',
          'rgba(153, 255, 153, 1)',
          'rgba(153, 255, 102, 1)',
          'rgba(255, 255, 153, 1)',
          'rgba(255, 255, 102, 1)',
          'rgba(255, 255, 51, 1)',
          'rgba(255, 255, 0, 1)',
          'rgba(255, 204, 51, 1)',
          'rgba(255, 153, 0, 1)',
          'rgba(255, 153, 102, 1)',
          'rgba(255, 102, 25, 1)',
          'rgba(255, 51, 0, 1)',
          'rgba(153, 0, 0, 1)',
          'rgba(100, 0, 0, 1)'
        ]
        heatmap.set('gradient', gradient);
      }

      function setLegendGradient() {
          var gradientCss = '(left';
          for (var i = 0; i < gradient.length; ++i) {
              gradientCss += ', ' + gradient[i];
          }
          gradientCss += ')';

          $('#legendGradient').css('background', '-webkit-linear-gradient' + gradientCss);
          $('#legendGradient').css('background', '-moz-linear-gradient' + gradientCss);
          $('#legendGradient').css('background', '-o-linear-gradient' + gradientCss);
          $('#legendGradient').css('background', 'linear-gradient' + gradientCss);
      }

      function setLegendLabels() {
      google.maps.event.addListenerOnce(map, 'tilesloaded', function() {
          var maxIntensity = heatmap['gm_bindings_']['data'][158]['kd']['D'];
          var legendWidth = $('#legendGradient').outerWidth();

          for (var i = 0; i <= maxIntensity; ++i) {
              var offset = i * legendWidth / maxIntensity;
              if (i > 0 && i < maxIntensity) {
                  offset -= 0.5;
              } else if (i == maxIntensity) {
                  offset -= 1;
              }

              $('#legend').append($('<div>').css({
                  'position': 'absolute',
                  'left': offset + 'px',
                  'top': '15px',
                  'width': '1px',
                  'height': '3px',
                  'background': 'black'
              }));
              $('#legend').append($('<div>').css({
                  'position': 'absolute',
                  'left': (offset - 5) + 'px',
                  'top': '18px',
                  'width': '10px',
                  'text-align': 'center',
                  'font-size': '0.8em',
              }).html(i));
          }
      });
  }



</script>

<script async defer
    src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAPS_KEY')}}&libraries=visualization&callback=initMap">
    //google.maps.event.addDomListener(window, 'load', initialize);
</script>


@endsection
