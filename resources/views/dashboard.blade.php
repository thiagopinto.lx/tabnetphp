@extends('layouts.app')
@section('css')
<link href="{{ mix('/css/Chart.min.css') }}" rel="stylesheet" type="text/css">
@endsection
@section('content')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <div class="d-sm-flex align-items-right right-content-between mb-4 d-print-none">
            <a href="{{$urls['detalhamentos']}}" style="margin-right: 10px;" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
                <i class="fas fa-info-circle fa-sm text-white-50"></i> Detalhamento
            </a>
            <a href="{{$urls['clustersmaps']}}" style="margin-right: 10px;" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
                <i class="fas fa-globe-americas fa-sm text-white-50"></i>Mapas
            </a>
            <a href="{{$urls['getsemepdem']}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
                <i class="far fa-calendar-alt fa-sm text-white-50"></i> Semanas Epidemiológicas
            </a>
        </div>
    </div>
    <div class="row">
        @include('components.bar-list')
        @isset($total)
            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-2 col-md-4 mb-2">
                <div class="card shadow h-100 py-2" style="border-left: 0.25rem solid {{$total->color}}">
                    <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-uppercase mb-1" style="color:{{$total->color}} ">{{$total->titulo}}</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{$total->media}}</div>
                        </div>
                        <div class="col-auto">
                        <i class="fas fa-stethoscope fa-2x text-gray-400"></i>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        @endisset
    </div>
    <div class="row">
        <!-- Content Column -->
        <div class="col-lg-6 mb-4">
          <!-- Line Chart -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Comparativos de meses por ano</h6>
            </div>
            <div class="card-body">
                <canvas id="myLineChart"></canvas>
                <hr>
                Dados SinanOnline
            </div>
          </div>
        </div>

        <!-- Content Column -->
        <div class="col-lg-6 mb-4">
            <!-- Line Chart -->
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Comparativos por ano</h6>
                </div>
                <div class="card-body">
                    <canvas id="myBarChart"></canvas>
                    <hr>
                    Dados SinanOnline
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 mb-12">
            <!-- Line Chart -->
            <div class="card shadow mb-12">
                <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Comparativos de semanas por ano</h6>
                </div>
                <div class="card-body">
                    <canvas id="myLineChartBySem"></canvas>
                    <hr>
                    Dados SinanOnline
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
<!-- Page level plugins -->
<script src="{{ mix('/js/Chart.min.js') }}"></script>
<script type="text/javascript">
    var xcsrftoken = $('meta[name="csrf-token"]').attr('content');
    var token = "{{csrf_token()}}";
    var get_data_bar = "{{$urls['getdatabar']}}";
    var get_data_line = "{{$urls['getdataline']}}";
    var get_data_line_by_sem = "{{$urls['getdatalinebysem']}}";


    $(document).ready(function(){

        var ctx = document.getElementById('myBarChart');
        var chartBar = new Chart(ctx, {
            type: 'bar',
            data: {},
            options: {
                responsive: true,
                scales: {
                    xAxes: [{
                        gridLines: {
                        display: true,
                        drawBorder: true
                        },
                        ticks: {
                        maxTicksLimit: 6
                        },
                        maxBarThickness: 25,
                    }],
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });

        var ctx = document.getElementById('myLineChart').getContext('2d');
        var chartLine = new Chart(ctx, {
            type: 'line',
            data: {},
            options: {

            }
        });

        var ctx = document.getElementById('myLineChartBySem').getContext('2d');
        var chartLineSem = new Chart(ctx, {
            type: 'line',
            data: {},
            options: {
                elements: {
                    point: {
                        pointStyle: 'circle'
                    }
				}
            }
        });

        function updateChart(fields){
            $.ajax({
            url : get_data_bar+'?'+decodeURIComponent( $.param( fields )) ,
            contentType: 'application/x-www-form-urlencoded',
            cache: false
            }).done(function(response){
                var dataBar = JSON.parse(response);
                chartBar.data = dataBar;
                chartBar.update();
            }).fail(function(response){
            console.log(response);
            }).always(function(response){
            });

            $.ajax({
            url : get_data_line+'?'+decodeURIComponent( $.param( fields )) ,
            contentType: 'application/x-www-form-urlencoded',
            cache: false
            }).done(function(response){
                var dataLine = JSON.parse(response);
                chartLine.data = dataLine;
                chartLine.update();
            }).fail(function(response){
            console.log(response);
            }).always(function(response){
            });

            $.ajax({
            url : get_data_line_by_sem+'?'+decodeURIComponent( $.param( fields )) ,
            contentType: 'application/x-www-form-urlencoded',
            cache: false
            }).done(function(response){
                var dataLine = JSON.parse(response);
                chartLineSem.data = dataLine;
                chartLineSem.update();
            }).fail(function(response){
            console.log(response);
            }).always(function(response){
            });
        }


        var fields = $( ".anos:checked" ).serializeArray();
        updateChart(fields);


        $('.anos').change(function(){
            var fields = $( ".anos:checked" ).serializeArray();
            updateChart(fields);
            console.log(fields);

        });

    });

</script>
@endsection
