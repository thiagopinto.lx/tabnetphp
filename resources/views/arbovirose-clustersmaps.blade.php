@extends('layouts.app')
@section('css')
<link href="{{ mix('/css/Chart.min.css') }}" rel="stylesheet" type="text/css">
@endsection
@section('content')
    <div class="row">
        @include('components.bar-list-btn')
        @isset($total)
        <!-- Earnings (Monthly) Card Example -->
        <div class="col-xl-2 col-md-6 mb-4">
            <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                <div class="row no-gutters">
                    <div class="btn-group-vertical">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fas fa-stethoscope"></i>
                                    </div>
                                </div>
                                <input type="text" class="form-control" aria-label="Input text com checkbox" value="Dengue" style="padding: 1px;" disabled>
                            </div>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fas fa-stethoscope"></i>
                                    </div>
                                </div>
                                <input type="text" class="form-control" aria-label="Input text com checkbox" value="Chik" style="padding: 1px;" disabled>
                            </div>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fas fa-stethoscope"></i>
                                    </div>
                                </div>
                                <input type="text" class="form-control" aria-label="Input text com checkbox" value="Zika" style="padding: 1px;" disabled>
                            </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
        @endisset
    </div>
    <div class="row">
        <div class="col-lg-12 mb-10">
            <!-- Maps -->
            <div class="card shadow mb-10">
                <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Mapa comparativo das ocrrências Dengue, Chikungunya, Zika</h6>
                </div>
                <div class="card-body">
                    <div id="map" class="z-depth-1-half map-container" style="height: 750px"></div>
                    <hr>
                    Dados SinanOnline
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
<!-- Page level plugins -->
<script src="{{ mix('/js/markerclusterer.js') }}"></script>
<script type="text/javascript">
    var xcsrftoken = $('meta[name="csrf-token"]').attr('content');
    var token = "{{csrf_token()}}";
    var get_geocodes_dengue = "{{$route['dengue']}}";
    var get_geocodes_chik = "{{$route['chik']}}";
    var get_geocodes_zika = "{{$route['zika']}}";


    var markerClustersDengues = new Map();
    var markerClustersChiks = new Map();
    var markerClustersZikas = new Map();
        var ano;
        function createCluster(fields, get_geocodes, agravo){
            for(var i=0; i<fields.length; i++) {
                ano = fields[i].value;
                $.ajax({
                url : get_geocodes+'?ano='+ano,
                contentType: 'application/x-www-form-urlencoded',
                cache: false
                }).done(function(response){
                    var data = JSON.parse(response);
                    //console.log(JSON.parse(response));
                    var markers = [];
                    for (let j = 0; j < data.geocodes.length; j++) {
                        var latLng = new google.maps.LatLng(
                                            data.geocodes[j].ID_GEO1,
                                            data.geocodes[j].ID_GEO2
                                            );
                        var marker = new google.maps.Marker({
                                            position: latLng,
                                            title: agravo+' '+data.geocodes[j].NU_ANO.toString()
                                        });
                        markers.push(marker);
                    }

                    markerCluster = new MarkerClusterer(map, markers,{
                    styles: [{
                                height: 35,
                                width: 35,
                                anchor: [10, 0],
                                textColor: '#ffffff',
                                backgroundColor: data.color,
                                textSize: 10
                            }, {
                                height: 45,
                                width: 45,
                                anchor: [15, 0],
                                textColor: '#ffffff',
                                backgroundColor: data.color,
                                textSize: 11
                            }, {
                                height: 55,
                                width: 55,
                                anchor: [20, 0],
                                textColor: '#ffffff',
                                backgroundColor: data.color,
                                textSize: 12
                            }]
                    });
                    if(agravo == 'dengue'){
                        markerClustersDengues.set(data.ano, markerCluster);
                    }
                    if(agravo == 'chik'){
                        markerClustersChiks.set(data.ano, markerCluster);
                    }
                    if(agravo == 'zika'){
                        markerClustersZikas.set(data.ano, markerCluster);
                    }
                    //console.log(markerClusters);
                }).fail(function(response){
                console.log(response);
                }).always(function(response){
                });

            }
        }

    $(document).ready(function(){

        $('.anos').change(function(){

            for (var [key, value] of markerClustersDengues) {
                 value.clearMarkers();
            }
            for (var [key, value] of markerClustersChiks) {
                 value.clearMarkers();
            }
            for (var [key, value] of markerClustersZikas) {
                 value.clearMarkers();
            }
            var dengues = $( ".dengues:checked" ).serializeArray();
            var chik = $( ".chiks:checked" ).serializeArray();
            var zika = $( ".zikas:checked" ).serializeArray();

            createCluster(dengues, get_geocodes_dengue, 'dengue');
            createCluster(chik, get_geocodes_chik, 'chik');
            createCluster(zika, get_geocodes_zika, 'zika');
        });

    });

    function initMap() {
          map = new google.maps.Map(document.getElementById('map'), {
          zoom: 12,
          center: {lat: -5.082324, lng: -42.796397}
        });
            var dengues = $( ".dengues:checked" ).serializeArray();
            var chik = $( ".chiks:checked" ).serializeArray();
            var zika = $( ".zikas:checked" ).serializeArray();

            createCluster(dengues, get_geocodes_dengue);
            createCluster(chik, get_geocodes_chik);
            createCluster(zika, get_geocodes_zika);

    }

</script>
<script async defer
src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAPS_KEY')}}&callback=initMap">
//google.maps.event.addDomListener(window, 'load', initialize);
</script>
@endsection
