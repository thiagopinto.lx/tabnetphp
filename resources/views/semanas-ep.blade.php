@extends('layouts.app')
@section('css')
<link href="{{ mix('/css/dataTables.bootstrap4.css') }}" rel="stylesheet" type="text/css">

@endsection
@section('content')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <div class="d-sm-flex align-items-right right-content-between mb-4">
            <a href="{{$urls['detalhamentos']}}" style="margin-right: 10px;" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
                <i class="fas fa-info-circle fa-sm text-white-50"></i> Detalhamento
            </a>
            <a href="{{$urls['clustersmaps']}}" style="margin-right: 10px;" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
                <i class="fas fa-globe-americas fa-sm text-white-50"></i>Mapas
            </a>
            <a href="{{$urls['getsemepdem']}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
                <i class="far fa-calendar-alt fa-sm text-white-50"></i> Semanas Epidemiológicas
            </a>
        </div>
    </div>
    <div class="row">
        @include('components.bar-list')
        @isset($total)
            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-2 col-md-4 mb-2">
                <div class="card shadow h-100 py-2" style="border-left: 0.25rem solid {{$total->color}}">
                    <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-uppercase mb-1" style="color:{{$total->color}} ">{{$total->titulo}}</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{$total->media}}</div>
                        </div>
                        <div class="col-auto">
                        <i class="fas fa-stethoscope fa-2x text-gray-300"></i>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        @endisset
    </div>

    <div class="row">
        <div class="col-lg-12 mb-4">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Notificações por Semanas Epidemiológicas {{$fullName}}</h6>
                </div>
                <div class="card-body">
                    <table class="table-striped compact table-bordered" id="dataTableSemanas" width="100%" cellspacing="0">
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
<!-- Page level plugins -->
<script type="text/javascript">
    var xcsrftoken = $('meta[name="csrf-token"]').attr('content');
    var token = "{{csrf_token()}}";
    var get_data_semanas = "{{$urls['getdatasemepdem']}}";

    var tableSemanas;

    function createTable(fields){
        var columnsName = [];
        columnsName.push({ title: "Semana" });

        for (i = 0; i < fields.length; i++) {
            columnsName.push({ title: fields[i].value });
        }

        tableSemanas = $('#dataTableSemanas').DataTable( {
            serverSide: true,
            processing: true,
            ajax:{
                url: get_data_semanas+'?'+decodeURIComponent( $.param( fields )),
                dataType: "json"
            },
            info: false,
            paging: false,
            searching: false,
            ordering: false,
            columns: columnsName
        } );
    }

    function updateTable(fields){
        $('#dataTableSemanas').DataTable().destroy();
        $('#dataTableSemanas').empty();
        createTable(fields);
    }


    $(document).ready(function(){

        $(document).ready(function() {
            var fields = $( ".anos:checked" ).serializeArray();
            createTable(fields);


            $('.anos').change(function(){
                var fields = $( ".anos:checked" ).serializeArray();
                updateTable(fields);
            });
        } );





    });

</script>
<script src="{{ mix('/js/datatables.js') }}"></script>
@endsection
