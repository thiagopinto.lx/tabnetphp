@isset($newLoadDatas)
    @for ($i = 0; $i < 5; $i++)

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-2 col-md-6 mb-4">
                <div class="card border-left-primary shadow h-100 py-2">
                    <div class="card-body">
                    <div class="row no-gutters">
                        <div class="btn-group-vertical" style="width: 100%;">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                  <div class="input-group-text" style="background-color:{{$newLoadDatas['dengue'][$i]->color}}; padding: 0.50rem 0.45rem;">
                                    <input type="checkbox" class="anos dengues" name="anos[]"  autocomplete="off" value="{{$newLoadDatas['dengue'][$i]->ano}}">
                                  </div>
                                </div>
                                <input type="text" class="form-control" aria-label="Input text com checkbox" value="Dengue {{$newLoadDatas['dengue'][$i]->ano}}" style="padding: 1px;" disabled>
                              </div>

                              <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                  <div class="input-group-text" style="background-color:{{$newLoadDatas['chik'][$i]->color}}; padding: 0.50rem 0.45rem;">
                                    <input type="checkbox" class="anos chiks" name="anos[]"  autocomplete="off" value="{{$newLoadDatas['chik'][$i]->ano}}">
                                  </div>
                                </div>
                                <input type="text" class="form-control" aria-label="Input text com checkbox" value="Chik {{$newLoadDatas['chik'][$i]->ano}}" style="padding: 1px;" disabled>
                              </div>

                              <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                  <div class="input-group-text" style="background-color:{{$newLoadDatas['zika'][$i]->color}}; padding: 0.50rem 0.45rem;">
                                    <input type="checkbox" class="anos zikas" name="anos[]"  autocomplete="off" value="{{$newLoadDatas['zika'][$i]->ano}}">
                                  </div>
                                </div>
                                <input type="text" class="form-control" aria-label="Input text com checkbox" value="Zika {{$newLoadDatas['zika'][$i]->ano}}" style="padding: 1px;" disabled>
                              </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
    @endfor
@endisset




