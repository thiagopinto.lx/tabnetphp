@extends('layouts.app')
@section('css')
<link href="{{ mix('/css/Chart.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ mix('/fonts/icofont/icofont.min.css') }}" rel="stylesheet" type="text/css">
@endsection
@section('content')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <div class="d-sm-flex align-items-right right-content-between mb-4 d-print-none">
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 mb-10">
            <!-- Chart -->
            <div class="card shadow mb-10">
                <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Comparativos de meses</h6>
                </div>
                <div class="card-body">
                    <canvas id="myChart" ></canvas>
                    Dados Sipin & Sies
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 mb-10">
            <div class="card shadow mb-10">
            </div>
        </div>
    </div>


@endsection
@section('scripts')
<!-- Page level plugins -->
<script src="{{ mix('/js/Chart.min.js') }}"></script>

<script type="text/javascript">
let estabelecimentos = {!! $data->estabelecimentos !!};
let dataColor = "{!! $data->color !!}";
let doses_aplicadas = [];
let doses_distribuidas = [];
let color = Chart.helpers.color;

for (let i = 0; i < estabelecimentos.length; i++) {
    doses_aplicadas.push(estabelecimentos[i].doses_aplicadas_mes);
    doses_distribuidas.push(estabelecimentos[i].doses_distribuidas_mes);  
}


var barChartData = {
			labels: [
                        'Janeiro',
                        'Fevereiro',
                        'Março',
                        'Abril',
                        'Maio',
                        'Junho',
                        'Julho',
                        'Agosto',
                        'Setembro',
                        'Outubro',
                        'Novembro',
                        'Desembro'
                    ]
,
			datasets: [{
				label: 'Doses Aplicadas',
				backgroundColor: dataColor,
				borderColor: color(dataColor).alpha(0.5).rgbString(),
				borderWidth: 1,
				data: doses_aplicadas
			}, {
				label: 'Doses Distribuidas',
				backgroundColor: color(dataColor).alpha(0.5).rgbString(),
				borderColor: dataColor,
				borderWidth: 1,
				data: doses_distribuidas
			}]

		};






var ctx = document.getElementById('myChart').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: barChartData,
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});
</script>
@endsection
