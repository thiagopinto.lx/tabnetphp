@extends('layouts.app')
@section('css')
<link href="{{ mix('/css/dataTables.bootstrap4.css') }}" rel="stylesheet" type="text/css">

@endsection
@section('content')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <div class="d-sm-flex align-items-right right-content-between mb-4">
            <a href="{{$urls['detalhamentos']}}" style="margin-right: 10px;" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
                <i class="fas fa-info-circle fa-sm text-white-50"></i> Detalhamento
            </a>
            <a href="{{$urls['clustersmaps']}}" style="margin-right: 10px;" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
                <i class="fas fa-globe-americas fa-sm text-white-50"></i>Mapas
            </a>
            <a href="{{$urls['getsemepdem']}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
                <i class="far fa-calendar-alt fa-sm text-white-50"></i> Semanas Epidemiológicas
            </a>
        </div>
    </div>
    <div class="row">
        @include('components.bar-list')
        @isset($total)
            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-2 col-md-4 mb-2">
                <div class="card shadow h-100 py-2" style="border-left: 0.25rem solid {{$total->color}}">
                    <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-uppercase mb-1" style="color:{{$total->color}} ">{{$total->titulo}}</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{$total->media}}</div>
                        </div>
                        <div class="col-auto">
                        <i class="fas fa-stethoscope fa-2x text-gray-300"></i>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        @endisset
    </div>

    <div class="row">
        <div class="col-lg-6 mb-4">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Classificação notificações de {{$fullName}}</h6>
                </div>
                <div class="card-body">
                    <table class="table-striped compact table-bordered" id="dataTableClassif" width="100%" cellspacing="0">
                    </table>
                </div>
            </div>
        </div>
        <div class="col-lg-6 mb-4">
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Evolução notificações de {{$fullName}}</h6>
                    </div>
                    <div class="card-body">
                        <table class="table-striped compact table-bordered" id="dataTableEvolucao" width="100%" cellspacing="0">
                        </table>
                    </div>
                </div>
            </div>
    </div>
    <div class="row">
        <div class="col-lg-6 mb-4">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Classificação por faixa etária RIPSA</h6>
                </div>
                <div class="card-body">
                    <table class="table-striped compact table-bordered" id="dataTableRipsa" width="100%" cellspacing="0">
                    </table>
                </div>
            </div>
        </div>
        <div class="col-lg-6 mb-4">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Notificações de {{$fullName}} por sexo</h6>
                </div>
                <div class="card-body">
                    <table class="table-striped compact table-bordered" id="dataTableSexo" width="100%" cellspacing="0">
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 mb-4">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary"> Contato Com Caso Suspeito de {{$fullName}} </h6>
                </div>
                <div class="card-body">
                    <table class="table-striped compact table-bordered" id="dataTableContatoAgravo" width="100%" cellspacing="0">
                    </table>
                </div>
            </div>
        </div>
        <div class="col-lg-6 mb-4">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">
                        Sinais e Sintomas (Petéquias/ Sufusões Hemorrágicas) {{$fullName}}
                    </h6>
                </div>
                <div class="card-body">
                    <table class="table-striped compact table-bordered" id="dataTabelaSintomasHemorragico" width="100%" cellspacing="0">
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 mb-4">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary"> Classificação tipo de {{$fullName}} </h6>
                </div>
                <div class="card-body">
                    <table class="table-striped compact table-bordered" id="dataTabelaTipo" width="100%" cellspacing="0">
                    </table>
                </div>
            </div>
        </div>
        <div class="col-lg-6 mb-4">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary"> Criterio de comfirmação de {{$fullName}} </h6>
                </div>
                <div class="card-body">
                    <table class="table-striped compact table-bordered" id="dataTabelaCriterioComfirmacao" width="100%" cellspacing="0">
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 mb-4">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary"> Notificações de {{$fullName}} por estabelecimento de Saúde</h6>
                </div>
                <div class="card-body">
                    <table class="table-striped compact table-bordered" id="dataTabelaNotificacaoUnidade" width="100%" cellspacing="0">
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row" id="incidencia-bairros">

    </div>
@endsection
@section('scripts')
<!-- Page level plugins -->
<script src="{{ mix('/js/Chart.min.js') }}"></script>
<script type="text/javascript">
    var xcsrftoken = $('meta[name="csrf-token"]').attr('content');
    var token = "{{csrf_token()}}";
    var get_data_classificacao = "{{$urls['getdataclassificacao']}}";
    var get_data_evolucao = "{{$urls['getdataevolucao']}}";
    var get_data_ripsa = "{{$urls['getdataripsa']}}";
    var get_data_sexo = "{{$urls['getdatasexo']}}";
    var get_data_contato_agravo = "{{$urls['getdatacontatoagravo']}}";
    var get_data_sintomas_hemorragico = "{{$urls['getdatasintomashemorragico']}}";
    var get_data_tipo = "{{$urls['getdatatipo']}}";
    var get_data_criterio_confirmacao = "{{$urls['getdatacriterioconfirmacao']}}";
    var get_data_notificacao_unidade = "{{$urls['getdatanotificacaounidade']}}";
    var get_data_bairro = "{{$urls['getdatabairro']}}";
    var fields;


    var tableClassif;
    var tableEvolucao;
    var tableRipsa;
    var tableSexo;
    var tableContatoAgravo;
    var tabelaSintomasHemorragico
    var tabelaTipo
    var tabelaCriterioComfirmacao;
    var dataTabelaNotificacaoUnidade;
    var tableBairro;


    function createDataTable(idDiv, ano){
        let anos = [ano];
        tableBairro = $("#"+idDiv).DataTable( {
        serverSide: true,
        processing: true,
        ajax:{
            url: get_data_bairro+'?'+decodeURIComponent( $.param( anos )),
            dataType: "json"
        },
        info: false,
        paging: false,
        searching: false,
        ordering: false,
        columns: [{title: ano.value}, {title: "Qnt"}]
        } );

    }

    function createTable(fields){
        var columnsName = [];
        var columnsNameAnos = [];
        columnsName.push({ title: "Name" });

        for (i = 0; i < fields.length; i++) {
            columnsName.push({ title: fields[i].value });
            columnsNameAnos.push({ title: fields[i].value });
            columnsNameAnos.push({ title: "Quant" });
        }

        tableClassif = $('#dataTableClassif').DataTable( {
            serverSide: true,
            processing: true,
            ajax:{
                url: get_data_classificacao+'?'+decodeURIComponent( $.param( fields )),
                dataType: "json"
            },
            info: false,
            paging: false,
            searching: false,
            ordering: false,
            columns: columnsName
        } );

        tableEvolucao = $('#dataTableEvolucao').DataTable( {
            serverSide: true,
            processing: true,
            ajax:{
                url: get_data_evolucao+'?'+decodeURIComponent( $.param( fields )),
                dataType: "json"
            },
            info: false,
            paging: false,
            searching: false,
            ordering: false,
            columns: columnsName
        } );

        tableRipsa = $('#dataTableRipsa').DataTable( {
            serverSide: true,
            processing: true,
            ajax:{
                url: get_data_ripsa+'?'+decodeURIComponent( $.param( fields )),
                dataType: "json"
            },
            info: false,
            paging: false,
            searching: false,
            ordering: false,
            columns: columnsName
        } );

        tableSexo = $('#dataTableSexo').DataTable( {
            serverSide: true,
            processing: true,
            ajax:{
                url: get_data_sexo+'?'+decodeURIComponent( $.param( fields )),
                dataType: "json"
            },
            info: false,
            paging: false,
            searching: false,
            ordering: false,
            columns: columnsName
        } );

        tableContatoAgravo = $('#dataTableContatoAgravo').DataTable( {
            serverSide: true,
            processing: true,
            ajax:{
                url: get_data_contato_agravo+'?'+decodeURIComponent( $.param( fields )),
                dataType: "json"
            },
            info: false,
            paging: false,
            searching: false,
            ordering: false,
            columns: columnsName
        } );

        tabelaSintomasHemorragico = $('#dataTabelaSintomasHemorragico').DataTable( {
            serverSide: true,
            processing: true,
            ajax:{
                url: get_data_sintomas_hemorragico+'?'+decodeURIComponent( $.param( fields )),
                dataType: "json"
            },
            info: false,
            paging: false,
            searching: false,
            ordering: false,
            columns: columnsName
        } );

        tabelaTipo = $('#dataTabelaTipo').DataTable( {
            serverSide: true,
            processing: true,
            ajax:{
                url: get_data_tipo+'?'+decodeURIComponent( $.param( fields )),
                dataType: "json"
            },
            info: false,
            paging: false,
            searching: false,
            ordering: false,
            columns: columnsName
        } );

        tabelaCriterioComfirmacao = $('#dataTabelaCriterioComfirmacao').DataTable( {
            serverSide: true,
            processing: true,
            ajax:{
                url: get_data_criterio_confirmacao+'?'+decodeURIComponent( $.param( fields )),
                dataType: "json"
            },
            info: false,
            paging: false,
            searching: false,
            ordering: false,
            columns: columnsName
        } );

        dataTabelaNotificacaoUnidade = $('#dataTabelaNotificacaoUnidade').DataTable( {
            serverSide: true,
            processing: true,
            ajax:{
                url: get_data_notificacao_unidade+'?'+decodeURIComponent( $.param( fields )),
                dataType: "json"
            },
            info: false,
            paging: false,
            searching: false,
            ordering: false,
            columns: columnsName
        } );

        /*
        tableBairro = $('#dataTableBairro').DataTable( {
            serverSide: true,
            processing: true,
            ajax:{
                url: get_data_bairro+'?'+decodeURIComponent( $.param( fields )),
                dataType: "json"
            },
            info: false,
            paging: false,
            searching: false,
            ordering: false,
            columns: columnsNameAnos
        } );
        */

        for (i = 0; i < fields.length; i++) {
            $('#incidencia-bairros').append(
              `<div class="col-lg-3 mb-4">
                 <div class="card shadow mb-4">
                   <div class="card-header py-3">
                     <h6 class="m-0 font-weight-bold text-primary">Incidência por bairro</h6>
                   </div>
                   <div class="card-body">
                     <table class="table-striped compact table-bordered" id="bairros${fields[i].value}" width="100%" cellspacing="0">
                     </table>
                   </div>
                </div>
              </div>`
            );
            createDataTable("bairros"+fields[i].value, fields[i]);
        }



    }

    function updateTable(fields, oldFields){
        $('#dataTableClassif').DataTable().destroy();
        $('#dataTableClassif').empty();
        $('#dataTableEvolucao').DataTable().destroy();
        $('#dataTableEvolucao').empty();
        $('#dataTableRipsa').DataTable().destroy();
        $('#dataTableRipsa').empty();
        $('#dataTableSexo').DataTable().destroy();
        $('#dataTableSexo').empty();
        $('#dataTableContatoAgravo').DataTable().destroy();
        $('#dataTableContatoAgravo').empty();
        $('#dataTabelaSintomasHemorragico').DataTable().destroy();
        $('#dataTabelaSintomasHemorragico').empty();
        $('#dataTabelaTipo').DataTable().destroy();
        $('#dataTabelaTipo').empty();
        $('#dataTabelaCriterioComfirmacao').DataTable().destroy();
        $('#dataTabelaCriterioComfirmacao').empty();
        $('#dataTabelaNotificacaoUnidade').DataTable().destroy();
        $('#dataTabelaNotificacaoUnidade').empty();

        for (i = 0; i < oldFields.length; i++) {
            $("#bairros"+oldFields[i].value).DataTable().destroy();
            $("#bairros"+oldFields[i].value).empty();
        }
        $('#incidencia-bairros').empty();

        createTable(fields);
    }


    $(document).ready(function(){
        fields = $( ".anos:checked" ).serializeArray();
        createTable(fields);


        $('.anos').change(function(){
            oldFields = fields;
            fields = $( ".anos:checked" ).serializeArray();
            updateTable(fields, oldFields);
        });
    });

</script>
<script src="{{ mix('/js/datatables.js') }}"></script>
@endsection
