window.$ = window.jQuery = require('jquery');
require('datatables.net');
require('datatables.net-bs4');
require('typeahead.js');
var Bloodhound = require('typeahead.js/dist/bloodhound');

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': xcsrftoken
    }
});

var no_fantasia = new Bloodhound({
  remote: {
      url: `${no_fantasia_url}?q=%QUERY%`,
      wildcard: '%QUERY%'
  },
  datumTokenizer: Bloodhound.tokenizers.whitespace('q'),
  queryTokenizer: Bloodhound.tokenizers.whitespace,
});
var no_razao_social = new Bloodhound({
    remote: {
        url: `${no_razao_social_url}?q=%QUERY%`,
        wildcard: '%QUERY%'
    },
    datumTokenizer: Bloodhound.tokenizers.whitespace('q'),
    queryTokenizer: Bloodhound.tokenizers.whitespace,
});

function initTypeahead(){
    $('.typeahead').typeahead({
      hint: true,
      highlight: true,
      minLength: 1,
  }, {
      name: 'no_fantasia',
      display: 'no_fantasia',
      source: no_fantasia,
      limit: 10,
      templates: {
        empty: [
            '<div class="list-group search-results-dropdown"><div class="list-group-item">Não encontrado.</div></div>'
        ],
        header: [
            '<h3 class="league-name text-danger">Nome Fantasia</h3><div class="list-group search-results-dropdown">'
        ],
        suggestion: function (data) {
            return '<a href="" onclick="return false;" class="list-group-item">' + data.no_fantasia + '</a>'
        }
      }
  }, {
      name: 'no_razao_social',
      display: 'no_razao_social',
      source: no_razao_social,
      limit: 10,
      templates: {
        empty: [
            '<div class="list-group search-results-dropdown"><div class="list-group-item">Não encontrado.</div></div>'
        ],
        header: [
            '<h3 class="league-name text-danger">Razão Social</h3><div class="list-group search-results-dropdown">'
        ],
        suggestion: function (data) {
            return '<a href="" onclick="return false;" class="list-group-item">' + data.no_razao_social + '</a>'
        }
      }
  }).bind('typeahead:select', function(event, data){
    let idSies = $(event.target).parent().next().val();
    let idCnes = data.id;
    $.ajax({
      url : `${relationship_url}/relationship/${idSies}/${idCnes}`,
      type: "GET"
    }).done(function(response){
          console.log(response);
          if (typeof(callback) !="undefined"){
            callback();
          }
    }).fail(function(response){
          console.log(response);
    }).always(function(response){

    });
    //$('#idBairro').val(data.id);
    //alert(data.id);
  });
}


// Call the dataTables jQuery plugin
$(document).ready(function() {

  function postForm(seletor, callback){
    $(seletor).submit(function(event){
        event.preventDefault();
        button = $(event.target).find(".carregar");
        load = $(event.target).find(".loadHtml");
        button.addClass("d-none");
        load.addClass("d-flex justify-content-center");
        load.removeClass("d-none");
        var post_url = $(event.target ).attr("action"); //get form action url
        var request_method = $(event.target ).attr("method"); //get form GET/POST method
        var form_data = new FormData(event.target ); //Encode form elements for submission

          $.ajax({
            url : post_url,
            type: request_method,
            data : form_data,
            contentType: false,
            processData: false,
            cache: false
          }).done(function(response){
                console.log(response);
                if (typeof(callback) !="undefined"){
                  callback();
                }
          }).fail(function(response){
                console.log(response);
          }).always(function(response){
            load.removeClass("d-flex justify-content-center");
            load.addClass("d-none");
            button.removeClass("d-none");
            $(".message").html(response);
          });
    });
  }

  var table = $('#dataTable').DataTable({
      serverSide: true,
      processing: true,
      ajax:{
             "url": getindex,
             "dataType": "json",
             "type": "POST",
             "data":{ _token: token}
           },
      columns: [
          {data: 'id', name: 'id'},
          {data: 'no_fantasia', name: 'no_fantasia'},
          {data: 'updated_at', name: 'updated_at'},
          {data: 'cnes', name: 'cnes', searchable: false, orderable: false},
          {data: 'edit', name: 'edit', searchable: false, orderable: false}
      ],
      "language": {
          "url": mylanguage
      },      
      "drawCallback": function(settings) {
        initTypeahead();
      }

  });

  postForm('.addForm', function(){
    table.ajax.reload(null, false);
  });

});
