window.$ = window.jQuery = require('jquery');
require('datatables.net');
require('datatables.net-bs4');
require('bootstrap-colorselector');

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': xcsrftoken
    }
});

// Call the dataTables jQuery plugin
$(document).ready(function() {

  function formColor(seletor){
    $(seletor).colorselector({
      callback: function (value, color, title) {
    }
  });

    $(seletor).change(function(event){
      var form = $(this).parent();
      var post_url = $(form).attr("action"); //get form action url
      var request_method = $(form).attr("method"); //get form GET/POST method
      var form_data = $(form).serialize(); //Encode form elements for submission
      $.ajax({
        url : post_url,
        type: request_method,
        contentType: 'application/x-www-form-urlencoded',
        data : form_data,
        cache: false
      }).done(function(response){
            console.log(response);
      }).fail(function(response){
            console.log(response);
      }).always(function(response){
        $(".message").html(response);
      });

    });
  }

  function postForm(seletor, callback){
    $(seletor).submit(function(event){
        event.preventDefault();
        button = $(event.target).find(".carregar");
        load = $(event.target).find(".loadDbf");
        button.addClass("d-none");
        load.addClass("d-flex justify-content-center");
        load.removeClass("d-none");
        var post_url = $(event.target ).attr("action"); //get form action url
        var request_method = $(event.target ).attr("method"); //get form GET/POST method
        var form_data = new FormData(event.target ); //Encode form elements for submission

          $.ajax({
            url : post_url,
            type: request_method,
            data : form_data,
            contentType: false,
            processData: false,
            cache: false
          }).done(function(response){
                console.log(response);
                if (typeof(callback) !="undefined"){
                  callback();
                }
          }).fail(function(response){
                console.log(response);
          }).always(function(response){
            load.removeClass("d-flex justify-content-center");
            load.addClass("d-none");
            button.removeClass("d-none");
            $(".message").html(response);
          });
    });
  }

  var table = $('#dataTable').DataTable({
      serverSide: true,
      processing: true,
      ajax:{
             "url": getindex,
             "dataType": "json",
             "type": "POST",
             "data":{ _token: token}
           },
      columns: [
          {data: 'ano', name: 'ano'},
          {data: 'user_id', name: 'user_id'},
          {data: 'updated_at', name: 'updated_at'},
          {data: 'status', name: 'status'},
          {data: 'color', name: 'color'},
          {data: 'action', name: 'action', orderable: false, searchable: false},
          {data: 'link', name: 'link', orderable: false, searchable: false}

      ],
      "language": {
          "url": mylanguage
      }

  }).on( 'draw.dt', function(){
    table.column(3).visible( false );
    formColor('.colorselector-table');

    postForm('.updateForm', function(){
        table.ajax.reload(null, false);
        table.column(3).visible( false );
        formColor('.colorselector-table');
    });
  });

  postForm('.addForm', function(){
    table.ajax.reload(null, false);
    table.column(3).visible( false );
    formColor('.colorselector');
  });

  $('.colorselector').colorselector();
});
