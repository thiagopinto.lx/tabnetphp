window.$ = window.jQuery = require('jquery');
require('datatables.net');
require('datatables.net-bs4');
require('bootstrap-colorselector');

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': xcsrftoken
    }
});

// Call the dataTables jQuery plugin
$(document).ready(function() {

  var table = $('#dataTable').DataTable({
      serverSide: true,
      processing: true,
      ajax:{
             "url": getindex,
             "dataType": "json",
             "type": "POST",
             "data":{ _token: token}
           },
      columns: [
          {data: 'NU_NOTIFIC', name: 'NU_NOTIFIC'},
          {data: 'DT_NOTIFIC', name: 'DT_NOTIFIC'},
          {data: 'NM_BAIRRO', name: 'NM_BAIRRO'},
          {data: 'NM_LOGRADO', name: 'NM_LOGRADO'},
          {data: 'NM_COMPLEM', name: 'NM_COMPLEM'},
          {data: 'NM_REFEREN', name: 'NM_REFEREN'},
          {data: 'ID_GEO1', name: 'ID_GEO1'},
          {data: 'ID_GEO2', name: 'ID_GEO2'},
          {data: 'checkbox', name: 'checkbox', searchable: false, orderable: false},
          {data: 'edit', name: 'edit', searchable: false, orderable: false}

      ],
      "language": {
          "url": mylanguage
      }

  });

  $('.button-gps').click(function() {
    spinner = $('.button-gps').find('span.spinner-border');
    fas = $('.button-gps').find('span.fas');
    spinner.removeClass('d-none');
    fas.addClass('d-none');
    button =
    $.ajax({
            url : getgps,
            type: "POST",
            dataType: "json",
            data : $("input[type=checkbox][name='checkboxs[]']:checked").serializeArray(),
            cache: false
          }).done(function(response){
                console.log(response);
          }).fail(function(response){
                console.log(response);
          }).always(function(response){
            spinner.addClass('d-none');
            fas.removeClass('d-none');
            table.ajax.reload(null, false);
          });
  });

  $('#checked-all').click(function() {
    $("input[type=checkbox][name='checkboxs[]']:checked").attr('checked', false);
    $("input[type=checkbox][name='checkboxs[]']").attr('checked', true);
    return false;
  });

});
