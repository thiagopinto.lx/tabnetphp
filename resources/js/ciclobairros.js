window.$ = window.jQuery = require('jquery');
require('datatables.net');
require('datatables.net-bs4');

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': xcsrftoken
    }
});

// Call the dataTables jQuery plugin
$(document).ready(function() {

  function postForm(seletor, callback){
    $(seletor).submit(function(event){
        event.preventDefault();
        button = $(event.target).find(".carregar");
        load = $(event.target).find(".loadDbf");
        button.addClass("d-none");
        load.addClass("d-flex justify-content-center");
        load.removeClass("d-none");
        var post_url = $(event.target ).attr("action"); //get form action url
        var request_method = $(event.target ).attr("method"); //get form GET/POST method
        var form_data = new FormData(event.target ); //Encode form elements for submission

          $.ajax({
            url : post_url,
            type: request_method,
            data : form_data,
            contentType: false,
            processData: false,
            cache: false
          }).done(function(response){
                console.log(response);
          }).fail(function(response){
                console.log(response);
          }).always(function(response){
            load.removeClass("d-flex justify-content-center");
            load.addClass("d-none");
            button.removeClass("d-none");
            $(".message").html(response);
            if (typeof(callback) !="undefined"){
              callback();
            }
          });
    });
  }


  function deleteBairro(seletor, callback){
    $(seletor).submit(function(event){
        event.preventDefault();
        var post_url = $(event.target ).attr("action"); //get form action url
        var request_method = $(event.target ).attr("method"); //get form GET/POST method
        var form_data = new FormData(event.target ); //Encode form elements for submission

          $.ajax({
            url : post_url,
            type: request_method,
            data : form_data,
            contentType: false,
            processData: false,
            cache: false
          }).done(function(response){
              console.log(response);
          }).fail(function(response){
              console.log(response);
          }).always(function(response){
            if (typeof(callback) !="undefined"){
              callback();
            }
          });
    });
  }

  var table = $('#dataTable').DataTable({
      serverSide: true,
      processing: true,
      ajax:{
             "url": getindex,
             "dataType": "json",
             "type": "POST",
             "data":{ _token: token}
           },
      columns: [
          {data: 'id', name: 'id'},
          {data: 'name', name: 'name'},
          {data: 'ipla', name: 'ipla', orderable: false, searchable: false},
          {data: 'view', name: 'view', orderable: false, searchable: false},
          {data: 'delete', name: 'delete', orderable: false, searchable: false}
      ],
      "language": {
          "url": mylanguage
      },
      "drawCallback": function(settings) {
        deleteBairro('.deleteBairro', function(){
          table.ajax.reload(null, false);
        });
      }

  });

  postForm('.addForm', function(){
    table.ajax.reload(null, false);
    $('#nomeBairro').val('');
    $('#idBairro').val('');
    $('#ipla').val('');
  });

});
